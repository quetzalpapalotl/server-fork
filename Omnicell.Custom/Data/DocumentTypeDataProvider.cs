﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text.RegularExpressions;
using System.Web;

using Omnicell.Custom.Components;

namespace Omnicell.Custom.Data
{
    public class DocumentTypeDataProvider : SqlDataProvider<DocumentTypeDataProvider, DocumentType>
    {
        public int Add(DocumentType type)
        {
            string procName = ProcNameWithOwner("omnicell_DocumentType_Add");
            List<SqlParameter> parameters = new List<SqlParameter>()
            {
                new SqlParameter{ParameterName = "@Value", SqlDbType = SqlDbType.NVarChar, Size = 50, Value = type.Value}
            };
            return Add(procName, parameters);
        }
        public void Update(DocumentType type)
        {
            string procName = ProcNameWithOwner("omnicell_DocumentType_Update");
            List<SqlParameter> parameters = new List<SqlParameter>()
            {
                new SqlParameter{ParameterName = "@Id", SqlDbType = SqlDbType.Int, Value = type.Id},
                new SqlParameter{ParameterName = "@Value", SqlDbType = SqlDbType.NVarChar, Size = 50, Value = type.Value}
            };
            Edit(procName, parameters);
        }
        public DocumentType Get(int id)
        {
            string procName = ProcNameWithOwner("omnicell_DocumentType_Get");
            List<SqlParameter> parameters = new List<SqlParameter>()
            {
                new SqlParameter{ParameterName = "@Id", SqlDbType = SqlDbType.Int, Value = id}
            };
            return Get(procName, parameters);
        }
        public List<DocumentType> List()
        {
            string procName = ProcNameWithOwner("omnicell_DocumentType_List");
            List<SqlParameter> parameters = new List<SqlParameter>();
            return GetList(procName, parameters);
        }
        public void Delete(int id)
        {
            string procName = ProcNameWithOwner("omnicell_DocumentType_Delete");
            List<SqlParameter> parameters = new List<SqlParameter>()
            {
                new SqlParameter{ParameterName = "@Id", SqlDbType = SqlDbType.Int, Value = id}
            };
            Delete(procName, parameters);
        }
    }
}
