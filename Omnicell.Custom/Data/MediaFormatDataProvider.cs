﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text.RegularExpressions;
using System.Web;

using Omnicell.Custom.Components;

namespace Omnicell.Custom.Data
{
    public class MediaFormatDataProvider : SqlDataProvider<MediaFormatDataProvider, MediaFormat>
    {
        public int Add(MediaFormat format)
        {
            string procName = ProcNameWithOwner("omnicell_MediaFormat_Add");
            List<SqlParameter> parameters = new List<SqlParameter>()
            {
                new SqlParameter{ParameterName = "@Value", SqlDbType = SqlDbType.NVarChar, Size = 50, Value = format.Value}
            };
            return Add(procName, parameters);
        }
        public void Update(MediaFormat format)
        {
            string procName = ProcNameWithOwner("omnicell_MediaFormat_Update");
            List<SqlParameter> parameters = new List<SqlParameter>()
            {
                new SqlParameter{ParameterName = "@Id", SqlDbType = SqlDbType.Int, Value = format.Id},
                new SqlParameter{ParameterName = "@Value", SqlDbType = SqlDbType.NVarChar, Size = 50, Value = format.Value}
            };
            ExecuteNonQuery(procName, parameters);
        }
        public MediaFormat Get(int id)
        {
            string procName = ProcNameWithOwner("omnicell_MediaFormat_Get");
            List<SqlParameter> parameters = new List<SqlParameter>()
            {
                new SqlParameter{ParameterName = "@Id", SqlDbType = SqlDbType.Int, Value = id}
            };
            return Get(procName, parameters);
        }
        public List<MediaFormat> List()
        {
            string procName = ProcNameWithOwner("omnicell_MediaFormat_List");
            List<SqlParameter> parameters = new List<SqlParameter>();
            return GetList(procName, parameters);
        }
        public void Delete(int id)
        {
            string procName = ProcNameWithOwner("omnicell_MediaFormat_Delete");
            List<SqlParameter> parameters = new List<SqlParameter>()
            {
                new SqlParameter{ParameterName = "@Id", SqlDbType = SqlDbType.Int, Value = id}
            };
            Delete(procName, parameters);
        }
    }
}
