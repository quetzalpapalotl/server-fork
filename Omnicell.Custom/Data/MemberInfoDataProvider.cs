﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text.RegularExpressions;
using System.Web;

using Omnicell.Custom.Components;

namespace Omnicell.Custom.Data
{
    public class MemberInfoDataProvider : SqlDataProvider<MemberInfoDataProvider, MemberType>
    {
        public string ListUserProducts(int userId)
        {
            string procName = ProcNameWithOwner("omnicell_UserGetProducts_Get");
            List<SqlParameter> parameters = new List<SqlParameter>()
            {
                new SqlParameter{ParameterName = "@UserId", SqlDbType = SqlDbType.Int, Value = userId}
            };

            return System.Convert.ToString(ExecuteScalar(procName, parameters));
        }
    }
}
