﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Telligent.Common;
using Telligent.Evolution.Components;
using Telligent.Evolution.Extensibility.Api.Version1;
using V1Entities = Telligent.Evolution.Extensibility.Api.Entities.Version1;
using Telligent.Evolution.Extensibility.Version1;
using Telligent.Evolution.Extensibility.UI.Version1;
using Telligent.Evolution.MediaGalleries.Components;
using Telligent.Evolution.Components.Search;

using Omnicell.Custom;
using Omnicell.Custom.Components;
using Omnicell.Custom.Search;
using Omnicell.Data.Model;
using System.IO;
using System.Net;

namespace Omnicell.Custom
{
    public class OmnicellInternationalCustomerPlugin : IScriptedContentFragmentExtension
    {
        public object Extension
        {
            get { return new InternationalCustomer(); }
        }

        public string ExtensionName
        {
            get { return "omnicell_v1_internationalcustomer"; }
        }

        public string Description
        {
            get { return "Enables scripted content fragments to use international customer functionality." ; }
        }

        public void Initialize()
        {
            
        }

        public string Name
        {
            get { return "Omnicell International Customer Plugin (omnicell_v1_internationalcustomer)"; }
        }
    }

    public class InternationalCustomer
    {
        OmnicellEntities db = new OmnicellEntities();

        public bool UploadInternationalCustomers()
        {
            bool success = true;
            string fileLocation = @"C:\inetpub\sites\myomnicell.com\prod\4282014_Prod\4282014_Prod\20140422\custom\data\Accounts.txt";
           // string fileLocation = url;
            int counter = 0;
           
            List<Omnicell_InternationalCustomer> intList = new List<Omnicell_InternationalCustomer>();

            try 
            {
                
                using (StreamReader r = new StreamReader(fileLocation, Encoding.Default, true))
                {
                    
                    while (!r.EndOfStream)
                    {
                        Omnicell_InternationalCustomer customer = new Omnicell_InternationalCustomer();
                        var line = r.ReadLine();
                        var values = line.Split(';');

                        string parseString = values[0];
                        string[] sArray = parseString.Split('\t');

                        if (counter > 0)
                        {
                            customer.CSN = sArray[0];
                            customer.Name = sArray[1];
                            customer.Street = sArray[2];
                            customer.City = sArray[3];
                            customer.Region = sArray[4];
                            customer.Country = sArray[5];
                            customer.Phone = sArray[6];
                            customer.IsActive = true;
                            customer.Updated = DateTime.Now;

                            intList.Add(customer);

                        }

                        counter++;

                    }
                }

                foreach (var cust in intList)
                {
                    db.Omnicell_InternationalCustomer.AddObject(cust);
                    db.SaveChanges();
                }

                success = true;

            }
            catch(Exception ex) 
            {
                 new CSException(CSExceptionType.UnknownError, string.Format("Error loading file to database."), ex).Log();
            }


            return success;
        }

        public bool UploadInternationalCustomers(string url)
        {
            bool success = true;
           // string fileLocation = @"C:\inetpub\sites\myomnicell.com\prod\4282014_Prod\4282014_Prod\20140422\custom\data\Accounts.txt";
           string fileLocation = url;
            int counter = 0;

            List<Omnicell_InternationalCustomer> intList = new List<Omnicell_InternationalCustomer>();

            try
            {
                HttpWebRequest httpRequest = (HttpWebRequest)WebRequest.Create(fileLocation);
                HttpWebResponse response = (HttpWebResponse)httpRequest.GetResponse();
                using (StreamReader r = new StreamReader(response.GetResponseStream(), Encoding.Default, true))
                {

                    while (!r.EndOfStream)
                    {
                        Omnicell_InternationalCustomer customer = new Omnicell_InternationalCustomer();
                        var line = r.ReadLine();
                        var values = line.Split(';');

                        string parseString = values[0];
                        string[] sArray = parseString.Split('\t');

                        if (counter > 0)
                        {
                            customer.CSN = sArray[0];
                            customer.Name = sArray[1];
                            customer.Street = sArray[2];
                            customer.City = sArray[3];
                            customer.Region = sArray[4];
                            customer.Country = sArray[5];
                            customer.Phone = sArray[6];
                            customer.IsActive = true;
                            customer.Updated = DateTime.Now;

                            intList.Add(customer);

                        }

                        counter++;

                    }
                }

                foreach (var cust in intList)
                {
                    db.Omnicell_InternationalCustomer.AddObject(cust);
                    db.SaveChanges();
                }

                success = true;

            }
            catch (Exception ex)
            {
                new CSException(CSExceptionType.UnknownError, string.Format("Error loading file to database."), ex).Log();
            }


            return success;
        }

        public bool IsInternationalCSN(string csn)
        {
            if(db.Omnicell_InternationalCustomer.Any(x => csn.Equals(x.CSN)))
            {
                return true;
            }

            return false;
        }

        public bool IsActiveCSN(string csn)
        {
            if(db.Omnicell_InternationalCustomer.Any(x => x.IsActive && csn.Equals(x.CSN))) 
            {
                return true;
            }

            return false;
        }

        public bool IsInActiveCSN(string csn)
        {
            if (db.Omnicell_InternationalCustomer.Any(x => !x.IsActive && csn.Equals(x.CSN)))
            {
                return true;
            }

            return false;
        }

        public Omnicell_InternationalCustomer GetCustomerById(int id)
        {
            Omnicell_InternationalCustomer customer = null;
           
            try
            {
                customer = db.Omnicell_InternationalCustomer.FirstOrDefault(x => x.Id == id);
            }
            catch (Exception ex)
            {
                new CSException(CSExceptionType.UnknownError, string.Format("Error retrieving International Customer from the database. ID: {0}", id), ex).Log();
            }

            return customer;
        }

        public V1Entities.PagedList<Omnicell_InternationalCustomer> GetCustomerByCSN(string csn)
        {
            List<Omnicell_InternationalCustomer> interCustomers = null;
            int total = 0;
            int pageIndex = 0;
            int pageSize = 5000;

            try
            {
                interCustomers = db.Omnicell_InternationalCustomer.Where(x => x.CSN.Equals(csn)).ToList();
                total = interCustomers.Count;
                interCustomers = interCustomers.Skip((pageIndex) * pageSize).Take(pageSize).ToList();
            }
            catch (Exception ex)
            {
                new CSException(CSExceptionType.UnknownError, string.Format("Error retrieving International Customer from the database. CSN: {0}", csn), ex).Log();
            }

            return new V1Entities.PagedList<Omnicell_InternationalCustomer>(interCustomers, pageSize, pageIndex, total);
        }

        public V1Entities.PagedList<Omnicell_InternationalCustomer> InternationalCustomerList(int pageSize, int pageIndex)
        {
            List<Omnicell_InternationalCustomer> interCustomers = null;
            int total = 0;
            try
            {
                interCustomers = db.Omnicell_InternationalCustomer.ToList();
                total = interCustomers.Count;
                interCustomers = interCustomers.Skip((pageIndex) * pageSize).Take(pageSize).ToList();
            }
            catch (Exception ex)
            {
                new CSException(CSExceptionType.UnknownError, string.Format("Error retrieving International Customers from the database."), ex).Log();
            }
             
            return new V1Entities.PagedList<Omnicell_InternationalCustomer>(interCustomers, pageSize, pageIndex, total);
        }


        public bool CreateInternationalCustomer(string csn, string name, string street, string city, string region, string country, string phone)
        {
            bool created = true;

            try
            {
                if (db.Omnicell_InternationalCustomer.Any(x => csn.Equals(x.CSN)))
                {
                    created = false;
                }
                else
                {
                     Omnicell_InternationalCustomer customer = new Omnicell_InternationalCustomer
                                    {
                                        CSN = csn,
                                        Name = name,
                                        Street = street,
                                        City = city,
                                        Region = region,
                                        Country = country,
                                        Phone = phone,
                                        IsActive = true,
                                        Updated = DateTime.Now

                                    };

                                    db.Omnicell_InternationalCustomer.AddObject(customer);
                                    db.SaveChanges();
                                    created = true;
                }
               

            }
            catch (Exception ex)
            {
              
                new CSException(CSExceptionType.UnknownError, string.Format("Error creating International Customer {0}.", csn), ex).Log();
            }

            return created;
        }

        public bool UpdateInternationalCustomer(int Id, string csn, string name, string street, string city, string region, string country, string phone, bool isActive)
        {
            bool updated = false;

            try
            {
                var customer = db.Omnicell_InternationalCustomer.FirstOrDefault(x => x.Id == Id);

                if (customer != null)
                {
                    customer.CSN = csn;
                    customer.Name = name;
                    customer.Street = street;
                    customer.City = city;
                    customer.Region = region;
                    customer.Country = country;
                    customer.Phone = phone;
                    customer.IsActive = isActive;
                    customer.Updated = DateTime.Now;

                    db.SaveChanges();
                }

                updated = true;
            }
            catch (Exception ex)
            {
                new CSException(CSExceptionType.UnknownError, string.Format("Error updating International Customer {0}.", csn), ex).Log();
            }

            return updated;
        }
    }
}
