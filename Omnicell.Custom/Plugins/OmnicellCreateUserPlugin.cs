﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
//using Microsoft.SharePoint.Client;
using Omnicell.Custom.Components;
using Omnicell.Data.Model;
using Telligent.Evolution.Components;
using Telligent.Evolution.Extensibility.Api.Version1;
using Telligent.Evolution.Extensibility.UI.Version1;
using Telligent.Evolution.Extensibility.Urls.Version1;
using System.Web;
using WidgetExtensions.Extensions;
using Telligent.Evolution.Extensibility.Version1;
using Telligent.Evolution.Extensibility.Api.Entities.Version1;

namespace Omnicell.Custom.Plugins
{
    public class OmnicellCreateUserPlugin : IScriptedContentFragmentExtension
    {
        public string Description
        {
            get { return "Enables scripted content fragments to utilize functionality for users"; }
        }

        public void Initialize()
        {
            
        }

        public string Name
        {
            get { return "Omnicell User Plugin omnicell_v1_user"; }
        }

        public object Extension
        {
            get { return new OmnicellUserExtension(); }
        }

        public string ExtensionName
        {
            get { return "omnicell_v1_user"; }
        }
    }

    public class OmnicellUserExtension
    {
        OmnicellEntities entities = new OmnicellEntities();
        OmnicellAuthentication wExtensions = new OmnicellAuthentication();
        OmnicellUtilitiesExtension utilities = new OmnicellUtilitiesExtension();
        OmnicellTrainingUserPluginExtension tUser = new OmnicellTrainingUserPluginExtension();
        InternationalCustomer iCustomer = new InternationalCustomer();

        public OmnicellUserExtension()
        {
        }

        public string CreateUser(string userName, string password, string email, string ipAddress, string portalGroupId, 
            [
                Documentation(Name = "CSN", Type = typeof(string), Description = "Client CSN Number prefix _ProfileFields_"),
                Documentation(Name = "FirstName", Type = typeof(string), Description = "First Name of the User prefix _ProfileFields_"),
                Documentation(Name = "LastName", Type = typeof(string), Description = "Last Name of the User prefix _ProfileFields_"),
                Documentation(Name = "Department", Type = typeof(string), Description = "The user's department prefix _ProfileFields_"),
                Documentation(Name = "Title", Type = typeof(string), Description = "The user's job title prefix _ProfileFields_"),
                Documentation(Name = "Office Phone", Type = typeof(string), Description = "The user's work phone number prefix _ProfileFields_"),
                Documentation(Name = "Facility", Type = typeof(string), Description = "The user's work facility prefix _ProfileFields_"),
                Documentation(Name = "Products", Type = typeof(string), Description = "A comma seperated list of selected products"),
                Documentation(Name = "TimeZone", Type = typeof(Double), Description = "The user's current time zome"),
                Documentation(Name = "AllowPartnerSitestoContact", Type = typeof(bool), Description = "Allow partner sites to contact true/false"),
                Documentation(Name = "AllowSitetoContact", Type = typeof(bool), Description = "Allow this site to contact true/false"),
                Documentation(Name = "Zip", Type = typeof(string), Description = "User's zip code")

            ]
            IDictionary options)
        {
            string returnUrl = string.Empty;
            string zip = options.GetValue("_ProfileFields_Zip", string.Empty);
            string csn = options.GetValue("_ProfileFields_CSN", string.Empty);
            string title = options.GetValue("_ProfileFields_Title", string.Empty);
            string firstName = options.GetValue("_ProfileFields_FirstName", string.Empty);
            string lastName = options.GetValue("_ProfileFields_LastName", string.Empty);
            string department = options.GetValue("_ProfileFields_Department", string.Empty);
            string facility = options.GetValue("_ProfileFields_Facility", string.Empty);
            string phone = options.GetValue("_ProfileFields_Office Phone", string.Empty);
            string products = options.GetValue("Products", string.Empty);
            bool partnerContact = options.GetValue<bool>("AllowPartnerSitesToContact", false);
            bool siteContact = options.GetValue<bool>("AllowSiteToContact", false);
            Double timeZone = options.GetValue<Double>("TimeZone", 0);

            UsersCreateOptions userOptions = new UsersCreateOptions();

            if (timeZone > 0)
                userOptions.TimeZone = timeZone;

            userOptions.AllowSiteToContact = siteContact;
            userOptions.AllowSitePartnersToContact = partnerContact;

            List<ProfileField> userAttributes = new List<ProfileField>();

            if (!string.IsNullOrEmpty(csn))
            {
                ProfileField aCsn = new ProfileField { Label = "CSN", Value = csn };
                userAttributes.Add(aCsn);
            }

            if (!string.IsNullOrEmpty(title))
            {
                ProfileField aTitle = new ProfileField { Label = "Title", Value = title };
                userAttributes.Add(aTitle);
            }

            if (!string.IsNullOrEmpty(firstName))
            {
                ProfileField aFirstName = new ProfileField { Label = "FirstName", Value = firstName };
                userAttributes.Add(aFirstName);
            }

            if (!string.IsNullOrEmpty(lastName))
            {
                ProfileField aLastName = new ProfileField { Label = "LastName", Value = lastName };
                userAttributes.Add(aLastName);
            }

            if (!string.IsNullOrEmpty(facility))
            {
                ProfileField aFacility = new ProfileField { Label = "Facility", Value = facility };
                userAttributes.Add(aFacility);
            }

            if (!string.IsNullOrEmpty(department))
            {
                ProfileField aDepartment = new ProfileField { Label = "Department", Value = department };
                userAttributes.Add(aDepartment);
            }

            if (!string.IsNullOrEmpty(phone))
            {
                ProfileField aPhone = new ProfileField { Label = "Office Phone", Value = phone };
                userAttributes.Add(aPhone);
            }

            userOptions.ProfileFields = userAttributes;
            userOptions.DisplayName = firstName + " " + lastName;

            var user = PublicApi.Users.Create(userName, password, email, userOptions);

            if (user != null)
            {
                int userId = Convert.ToInt32(user.Id);
                int portalId = Convert.ToInt32(portalGroupId);
                SetProductGroups(products, userId);
                returnUrl = SetRole(userId, email, ipAddress, csn, firstName, lastName, zip, department, portalId);
            }
            

            return returnUrl;
        }

        private string SetRole(int userId, string email, string ipAddress, string csn, string firstName, string lastName, string zip, string department, int portalGroupId)
        {
            string returnUrl = "/";
            string failReason = string.Empty;
            string roleName = string.Empty;
            string groupMembershipType = "PendingMember";
            string message = "Unkown";
            GroupUserMembersCreateOptions options = new GroupUserMembersCreateOptions();

            if (wExtensions.HasCustomerOverride(email))
            {
                roleName = "Customer";
                tUser.AddMembertoRole(userId, roleName);
            }
            else if (wExtensions.HasEmployeeOverride(email))
            {
                roleName = "Employee";
                tUser.AddMembertoRole(userId, roleName);
            }
            else if (!wExtensions.IsAuthorizedIpAddress(ipAddress))
            {
                failReason = "Unauthorized IP Address";
                wExtensions.AddFailedAccountAttempt(csn, email, failReason, firstName, lastName, zip, ipAddress);
                returnUrl = "p/unauthorizedaccount";
              
            }
            else if (wExtensions.IsBannedDomainName(email))
            {
                failReason = "Restricted Email Domain";
                wExtensions.AddFailedAccountAttempt(csn, email, failReason, firstName, lastName, zip, ipAddress);
                returnUrl = "p/unauthorizedaccount";
            }
            else if (department == "Omnicell Employee")
            {
                if (wExtensions.IsActiveEmployee(email))
                {
                    roleName = "Employee";
                    tUser.AddMembertoRole(userId, roleName);    
                }
                else
                {
                    roleName = "Employee Inactive";
                    tUser.AddMembertoRole(userId, roleName);
                    options.Message = message;
                    Telligent.Evolution.Components.User accessUser = CSContext.Current.User;
                    Telligent.Evolution.Components.User adminUser = Telligent.Evolution.Users.GetUser("admin");

                    CSContext.Current.User = adminUser;

                    options.GroupMembershipType = groupMembershipType;
                    var groupUser = PublicApi.GroupUserMembers.Create(portalGroupId, userId, options);
                    returnUrl = "p/unauthorizedaccount";

                    CSContext.Current.User = accessUser;
                }
            }
            else if (department == "International Partner")
            {
                if (wExtensions.IsActiveCSN(csn) && utilities.InternationalDomain(email))
                {
                    roleName = "International Partner";
                    tUser.AddMembertoRole(userId, roleName);
                }
                else
                {
                    roleName = "International Partners Unverified";
                    tUser.AddMembertoRole(userId, roleName);
                    options.Message = "Inactive CSN";

                    Telligent.Evolution.Components.User accessUser = CSContext.Current.User;
                    Telligent.Evolution.Components.User adminUser = Telligent.Evolution.Users.GetUser("admin");

                    CSContext.Current.User = adminUser;

                    options.GroupMembershipType = groupMembershipType;
                    var groupUser = PublicApi.GroupUserMembers.Create(portalGroupId, userId, options);
                    returnUrl = "p/unverifiedinternationalpartners";

                    CSContext.Current.User = accessUser;
                }
            }
            else if (iCustomer.IsInternationalCSN(csn))
            {
                if (iCustomer.IsActiveCSN(csn))
                {
                    roleName = "International Customer";
                    tUser.AddMembertoRole(userId, roleName);
                }
                else
                {
                    roleName = "International Customer Unverified";
                    tUser.AddMembertoRole(userId, roleName);
                    options.Message = "InactiveCSN";

                    Telligent.Evolution.Components.User accessUser = CSContext.Current.User;
                    Telligent.Evolution.Components.User adminUser = Telligent.Evolution.Users.GetUser("admin");

                    CSContext.Current.User = adminUser;

                    options.GroupMembershipType = groupMembershipType;
                    var groupUser = PublicApi.GroupUserMembers.Create(portalGroupId, userId, options);
                    returnUrl = "p/unverifiedinternationalcustomers";

                    CSContext.Current.User = accessUser;
                }
            }
            else if (!wExtensions.IsActiveCSN(csn) && !wExtensions.IsImportedFromSalesForce(csn))
            {
                failReason = "Inactive CSN";
                roleName = "Customer Unverified";
                wExtensions.AddFailedAccountAttempt(csn, email, failReason, firstName, lastName, zip, ipAddress);
                tUser.AddMembertoRole(userId, roleName);
                options.Message = "Inactive CSN";

                Telligent.Evolution.Components.User accessUser = CSContext.Current.User;
                Telligent.Evolution.Components.User adminUser = Telligent.Evolution.Users.GetUser("admin");

                CSContext.Current.User = adminUser;

                options.GroupMembershipType = groupMembershipType;
                var groupUser = PublicApi.GroupUserMembers.Create(portalGroupId, userId, options);
                returnUrl = "p/unverifiedcustomers";

                CSContext.Current.User = accessUser;
            }
            else
            {
                roleName = "Customer";
                tUser.AddMembertoRole(userId, roleName);
            }
           
            return returnUrl;
        }

        public void SetProductGroups(string productGroups, int userId)
        {
            Telligent.Evolution.Components.User accessUser = CSContext.Current.User;
            Telligent.Evolution.Components.User adminUser = Telligent.Evolution.Users.GetUser("admin");

            CSContext.Current.User = adminUser;

            string[] productIds = productGroups.Split(',');
            string groupMembershipType = "Member";
            GroupUserMembersCreateOptions options = new GroupUserMembersCreateOptions();

            foreach (string id in productIds)
            {
                int groupId = Convert.ToInt32(id);
                options.GroupMembershipType = groupMembershipType;
                var groupUser = PublicApi.GroupUserMembers.Create(groupId, userId, options);
            }

            CSContext.Current.User = accessUser;
        }
    }
}
