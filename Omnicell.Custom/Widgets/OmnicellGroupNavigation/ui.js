(function($)
{
	if (typeof $.telligent === 'undefined') { $.telligent = {}; }
	if (typeof $.telligent.evolution === 'undefined') { $.telligent.evolution = {}; }
	if (typeof $.telligent.evolution.widgets === 'undefined') { $.telligent.evolution.widgets = {}; }

	var populate = function(context)
	{
		if (context.populated)
		{
			context.popup.glowPopUpPanel('show', context.newpost);
			return;
		}
			
		$.telligent.evolution.get({
				url: context.newPostUrl,
				success: function(response)
				{
					var jq = $(response);
					var links = jq.find('.navigation-item a');
					if (links.length == 0)
					{
						context.newPost.hide();
					}
					else if (links.length == 1)
					{
						try {
							window.location.href = links.get(0).href;							
						} catch(e) {}
					}
					else
					{					
						context.popup.glowPopUpPanel('html', jq);
						context.popup.glowPopUpPanel('show', context.newPost);
						context.populated = true;
					}
				},
				defaultErrorMessage: context.error
			});
	};

	$.telligent.evolution.widgets.groupApplicationNavigation = {
		register: function(context) {
			context.populated = false;

			context.popup = $('<div></div>').glowPopUpPanel({
				cssClass: 'menu group-application-navigation-content',
				position: 'down',
				zIndex: 1000,
				hideOnDocumentClick: true
			})
				.bind('glowPopUpPanelIsShown', function() { context.newPost.addClass('selected'); })
				.bind('glowPopUpPanelHidden', function() { context.newPost.removeClass('selected'); })
				.glowPopUpPanel('html', '');

			context.newPost.click(function()
			{
			    context.populated = false;
				if (context.popup.glowPopUpPanel('isShown'))
				{
					context.popup.glowPopUpPanel('hide');
				}
				else
				{
					populate(context);
				}
				
				return false;
			});
		}
	};
}(jQuery));
