(function ($) {
  if (typeof $.widgets === 'undefined') {
      $.widgets = {};
  }
  /*
   $('.search-csn').bind('click', function(e, data) {
              e.preventDefault();
              
             var csn =  $('.csn-search-text').val();
             
             if(csn.length > 0) {
                 var qs = window.location.pathname + '?csn=' + csn;
                 window.location = qs;
             }
             
          }); 
     */     
  var deleteCSN = function(context, list) {
      $.telligent.evolution.post({
          url: context.deleteCsn,
          data: { ids: list },
          success: function(response) {
              if(response.result == "True") {
                 alert("CSN(s) removed successfully");
                 window.location = window.location.pathname;
              }
             else {
                 alert("One or more CSNs were not removed.");
                
              }
              
          },
          error: function() {
              alert("An error occured csns were not removed successfully");
              
          }
          
      });
      
      return false;
  };
  
  var addCSN = function(context, csn) {
      $.telligent.evolution.post({
          url: context.addCsn,
          data: { id: csn },
          success: function(response) {
            if(response.result == "True") 
            {
                alert("CSN added successfully");
                window.location = window.location.pathname;
            }
            else {
                alert("Error: CSN does not exist in database.");
               
            }
          },
          error: function(jqXHR) {
              alert(jqXHR.status + ' ' + jqXHR.responseText);
          }
      });
      return false;
  }; 
  
   var api = {
      register: function(context) {
          context.wrapperId = $(context.wrapperId);
          
          $('.removeCsn').bind('click', function(e, data) {
              e.preventDefault();
              var list = "";
              $('.remove-checkbox').each(function() {
                  var isChecked = (this.checked ? "1" : "0");
                  var csn = $(this).val();
                  if(isChecked == "1") {
                      list += (list == "" ? csn : "," + csn);
                  }
              });
              
              if(list.length > 0) {
                  deleteCSN(context, list);
              }
          });
          
          $('.addCsn').bind('click', function (e, data) {
              e.preventDefault();
              var csn = $.trim($('.csn-text').val());
              
              if(csn.length > 0) {
                  addCSN(context, csn);
              }
              
          });
          
          $('.search-csn').bind('click', function(e, data) {
              e.preventDefault();
              
             var csn =  $('.csn-search-text').val();
             
             if(csn.length > 0) {
                 var qs = window.location.pathname + '?csn=' + csn;
                 window.location = qs;
             }
             
          });
          
       /*   $('.select-all').bind('click', function(e, data) {
              e.preventDefault();
                if($('.select-all').text() == "Select") {
                    $('.remove-checkbox').attr('checked', true);
                    $('.select-all a').text("De-select");
                }
                else {
                    $('.remove-checkbox').attr('checked', false);
                    $('.select-all a').text("Select");
                }
              
              
          }); */
          
      }
  };
  
  if (typeof $.widgets == 'undefined') { $.widgets = {}; }
  $.widgets.manageCSNWidget = api;
  
}(jQuery));