(function($)
{
	if (typeof $.telligent === 'undefined') { $.telligent = {}; }
	if (typeof $.telligent.evolution === 'undefined') { $.telligent.evolution = {}; }
	if (typeof $.telligent.evolution.widgets === 'undefined') { $.telligent.evolution.widgets = {}; }

	var _preview = function(context)
	{
		$('#' + context.previewPaneId).html('<div class="message loading loading__message">' + context.previewLoadingText + '</div>');

		$.telligent.evolution.post({
			url: context.previewUrl,
			data: _populateData(context),
			success: function(response)
			{
				$('#' + context.previewPaneId).hide().html(response).fadeIn('fast');
			},
			defaultErrorMessage: context.previewErrorText,
			error: function(xhr, desc, ex)
			{
				$('#' + context.previewPaneId).html('<div class="message error error__message">' + desc + '</div>');
			}
		});
	}, 
	_save = function(context)
	{
		$.telligent.evolution.post({
			url: context.saveUrl,
			data: _populateData(context),
			success: function(response)
			{
				if (response.redirectUrl) {
					window.location = response.redirectUrl;
				}
				else if (response.errors) {
					alert(context.saveErrorText + '\n\n' + response.errors[0].error);

					$('#' + context.wrapperId + ' a.save-post').parent().removeClass('processing');
					$('#' + context.wrapperId + ' a.save-post').removeClass('disabled');	
				}
			},
			defaultErrorMessage: context.saveErrorText,
			error: function(xhr, desc, ex)
			{
				$.telligent.evolution.notifications.show(desc,{type:'error'});
				$('#' + context.wrapperId + ' a.save-post').parent().removeClass('processing');
				$('#' + context.wrapperId + ' a.save-post').removeClass('disabled');
			}
		});
	},
	_populateData = function(context)
	{
		var data = {
			Body: context.getBodyContent(),
			ForumId: context.forumId
		};
			
		if (context.threadTypeQA.length > 0 && context.threadTypeQA.is(':checked'))
		{
			data.IsQuestion = 1;
		}
		else if (context.threadTypeDiscussion.length > 0 && context.threadTypeDiscussion.is(':checked'))
		{
			data.IsQuestion = 0;
		}
		else
		{
			data.IsQuestion = -1;
		}
					
		if ($('#' + context.subjectId).length > 0)
		{
			data.Subject =  $('#' + context.subjectId).val();
		}
		
		if ($('#' + context.editNotesId).length > 0)
		{
			data.EditNotes = $('#' + context.editNotesId).val();
		}
		
		if (context.tagBox.length > 0)
		{
			var inTags = context.tagBox.val().split(/[,;]/g);
			var tags = [];
			for (var i = 0; i < inTags.length; i++)
			{
				var tag = $.trim(inTags[i]);
				if (tag) {
					tags[tags.length] = tag;
				}
			}
			
			data.Tags = tags.join(',');
		}
		
		if (context.subscribe.length > 0)
		{
			data.Subscribe = context.subscribe.is(':checked') ? 1 : 0;
		}
		else
		{
			data.Subscribe = -1;
		}
		
		if (context.feature.length > 0)
		{
			data.Featured = context.feature.is(':checked') ? 1 : 0;
			if (context.featuredImageUrl.length > 0)
			{
				data.FeaturedImageUrl = context.featuredImageUrl.val();
			}
		}
		else
		{
			data.Featured = -1;
		}
		
		if (context.file)
		{
			data.AttachmentChanged = 1;
			if (context.file.isRemote)
			{
				data.AttachmentName = context.file.fileName;
				data.AttachmentUrl = context.file.url;
				data.AttachmentIsRemote = '1';
			}
			else
			{
				data.AttachmentName = context.file.fileName;
				data.AttachmentContextId = context.file.contextId;
				data.AttachmentIsRemote = '0';
			}	
		}
		else if (context.removeAttachment)
		{
			data.RemoveAttachment = 1;
		}
		
		if (context.lock.length > 0)
		{
			data.IsLocked = context.lock.is(':checked') ? 1 : 0;
		}
		else
		{
			data.IsLocked = -1;
		}
		
		if (context.suggestAnswer.length > 0)
		{
			data.IsSuggestedAnswer = context.suggestAnswer.is(':checked') ? 1 : 0;
		}
		else
		{
			data.IsSuggestedAnswer = -1;
		}
		
		if (context.anonymous.length > 0)
		{
			data.IsAnonymous = context.anonymous.is(':checked') ? 1 : 0;
		}
		else
		{
			data.IsAnonymous = -1;
		}
		
		if (context.sticky.length > 0 && context.sticky.val() != '0')
		{
			data.StickyDate = $.telligent.evolution.formatDate(new Date((new Date()).getTime() + (parseInt(context.sticky.val(), 10) * 1000 * 60 * 60 * 24)));
			data.HasStickyDate = 1;
		}
		else
		{
			data.HasStickyDate = 0;
		}
		
		if (context.replyId > 0)
		{
			data.ReplyId = context.replyId;
		}
		
		if (context.threadId > 0)
		{
			data.ThreadId = context.threadId;
		}
		
		if (context.replyToThreadId > 0)
		{
			data.ReplyToThreadId = context.replyToThreadId;
		}
		
		if (context.replyToReplyId > 0)
		{
			data.ReplyToReplyId = context.replyToReplyId;
		}
		
		if (context.includePoll.length > 0)
		{
			if (context.includePoll.is(':checked'))
			{
				data.HasPoll = 1;
				data.PollTitle = context.pollTitle.val();
				data.PollDescription = context.getPollDescription();
				
				var expireDays = parseInt(context.pollExpireAfterDays.val(), 10);
				if (!isNaN(expireDays))
				{
					data.PollExpireDays = expireDays;
				}	
				
				var answers = [];
				var index = 0;
				context.pollAnswers.find('.field-item-input').each(function()
				{
					var answerId = $(this).attr('data-pollitemid');
					var answerText = $(this).find('input').val();
					
					answers[answers.length] = 'AnswerIndex=' + index;
					
					if (answerId != 'NEW')
					{
						answers[answers.length] = 'Answer' + index + 'Id=' + encodeURIComponent(answerId);
					}
					
					answers[answers.length] = 'Answer' + index + '=' + encodeURIComponent(answerText);
					
					index++;
				});
				
				data.PollAnswers = answers.join('&');
			}
			else
			{
				data.HasPoll = 0;
			}
		}
		else
		{
			data.HasPoll = -1;
		}
		
		return data;
	}
	_attachHandlers = function(context)
	{
		context.attachmentUpdate.click(function()
		{
			$.glowModal(context.uploadFileUrl, { width: 500, height: 300, onClose: function(file) 
				{
					if (file)
					{
						context.attachmentFileName.text(file.fileName);
						context.fileUploaded = true;
						context.file = file;
						context.attachmentRemove.show();
						context.attachmentUpdate.text(context.attachmentUpdateText);
					}
				}
			});
				
			return false;
		});
		
		context.attachmentRemove.click(function()
		{
			context.fileUploaded = false;
			context.file = null;
			context.attachmentFileName.text('');
			context.attachmentRemove.hide();
			context.attachmentUpdate.text(context.attachmentAddText);
			context.removeAttachment = true;
			
			return false;	
		});
		
		if (!context.attachmentFileName.text())
		{
			context.attachmentRemove.hide();
		}
		
		context.featuredImageUrl.evolutionUserFileTextBox({
			removeText: context.featureRemoveText,
			selectText: context.featureSelectUploadText,
			noFileText: context.featureNoFileSelectedText,
			initialPreviewHtml: context.featuredImagePreview
		});

		context.feature.click(function() {
			if($(this).is(':checked')) {
				context.featuredImage.show();
			} else {
				context.featuredImage.hide();
			}
		});

		$.telligent.evolution.navigationConfirmation.enable();
		var saveButton = $('#' + context.wrapperId + ' a.save-post');
		$.telligent.evolution.navigationConfirmation.enable();
		$.telligent.evolution.navigationConfirmation.register(saveButton);
		
		if (context.quoteButton.length > 0)
		{
			context.quoteButton.click(function()
			{
				context.saveBodyBookmark();
				var content = $.telligent.glow.utility.getSelectedHtmlInElement(context.quoteText.get(0),true,false,context.quoteInvalidSelectionText);
				if (content)
				{
					context.insertBodyContent('[quote user="' + context.replyToAuthor + '"]' + content + '[/quote]');
				}				
				
				return false;
			});
		}
		
		if(context.includePoll.length > 0)
		{
			context.includePoll.click(function()
			{
				if($(this).is(':checked'))
				{
					$('#' + context.wrapperId + ' .poll-toggled-option').show();
				}
				else
				{
					$('#' + context.wrapperId + ' .poll-toggled-option').hide();
				}				
			});
			
			context.pollNewAnswer.keydown(function(e) { 
				if (e.keyCode == 13)
				{
					context.pollAddAnswer.click();
					return false;
				}
			});
			
			context.pollAddAnswer.click(function()
			{
				var answer = $.trim(context.pollNewAnswer.val());
				if (answer != '')
				{
					var newAnswer = $('<span class="field-item-input" data-pollitemid="NEW"><input type="text" size="70" value="" /><a href="#" class="move-up"><span></span>&nbsp;</a><a href="#" class="move-down"><span></span>&nbsp;</a><a href="#" class="delete-question"><span></span>&nbsp;</a></span>');
					
					newAnswer.find('input').val(answer);
					
					if (context.pollAnswers.has('.field-item-input').length == 0)
					{
						context.pollAnswers.html(newAnswer);
					}
					else
					{
						context.pollAnswers.find('.field-item-input:last').after(newAnswer);
					}
					
					context.pollNewAnswer.val('');
				}
				
				return false;
			});
			
			$('.move-up', context.pollAnswers).live('click', function()
			{
				var item = $(this).parent();
				if (item.prev().length > 0)
				{
					item.prev().before(item);
				}
				return false;
			});
			
			$('.move-down', context.pollAnswers).live('click', function()
			{
				var item = $(this).parent();
				if (item.next().length > 0)
				{
					item.next().after(item);
				}
				return false;
			});
			
			$('.delete-question', context.pollAnswers).live('click', function()
			{
				if (window.confirm(context.pollAnswerDeleteConfirmation))
				{
					var item = $(this).parent();			
					item.slideUp('fast', function() { item.detach(); });
				}
				
				return false;
			});
		}
	},
	_addValidation = function(context)
	{
		var saveButton = $('#' + context.wrapperId + ' a.save-post');

		saveButton.evolutionValidation({
			validateOnLoad: context.mediaId <= 0 ? false : null,
			onValidated: function(isValid, buttonClicked, c) {
				if (isValid) {
					saveButton.removeClass('disabled');
				} else {
					saveButton.addClass('disabled');
					if (c)
					{
						var tabbedPane = context.tabs.glowTabbedPanes('getByIndex', c.tab);
						if (tabbedPane) {
							context.tabs.glowTabbedPanes('selected', tabbedPane);
						}
					}
				}
			},
			onSuccessfulClick: function(e) {
				if (!saveButton.parent().hasClass('processing'))
				{
					saveButton.parent().addClass('processing');
					saveButton.addClass('disabled');
					_save(context);
				}
				
				return false;
			}
		});

		if ($('#' + context.subjectId).length > 0)
		{
			saveButton.evolutionValidation('addField', '#' + context.subjectId,
				{
					required: true,
					messages: { required: context.subjectRequiredText }
				},
				'#' + context.wrapperId + ' .field-item.post-subject .field-item-validation',
				{ tab: 0 }
			);
		}
		
		context.attachBodyChangeScript(saveButton.evolutionValidation('addCustomValidation', 'body', function() 
			{ 
				var body = context.getBodyContent();
				var beginningMatches = body.match(/\[quote((?:\s*)user=(?:\"|&quot;|&#34;).*?(?:\"|&quot;|&#34;))?\]/g); 
        		var endingMatches = body.match(/\[\/quote(?:\s*)\]/g); 
        		if ((beginningMatches == null) != (endingMatches == null))
            		return false;
            
	    		return body.length > 0 && ((beginningMatches == null && endingMatches == null) || beginningMatches.length == endingMatches.length);
			}, 
			context.bodyRequiredText, '#' + context.wrapperId + ' .field-item.post-body .field-item-validation', { tab: 0 }
		));		
		
		if (context.editNotesRequired && $('#' + context.editNotesId).length > 0)
		{
			saveButton.evolutionValidation('addField', '#' + context.editNotesId,
				{
					required: true,
					messages: { required: context.editNotesRequiredText }
				},
				'#' + context.wrapperId + ' .field-item.edit-notes .field-item-validation',
				{ tab: 0 }
			);
		}
		
		if(context.includePoll.length > 0)
		{
			var f = saveButton.evolutionValidation('addCustomValidation', 'pollQuestion', function()
				{
					return (!context.includePoll.is(':checked') || $.trim(context.pollTitle.val()) != '');
				},
				'*', '#' + context.wrapperId + ' .field-item.poll-title .field-item-validation',
				{ tab: 2 }
			);
			
			context.pollTitle.keydown(function(){f();}).change(function(){f();});
		}
	};
		
	$.telligent.evolution.widgets.createEditPost = {
		register: function(context) {
			var tabs = [];
			tabs[tabs.length] = [context.composePaneId, context.composePaneText, null];
			tabs[tabs.length] = [context.optionsPaneId, context.optionsPaneText, null];
			if ($('#' + context.pollPaneId).length > 0)
			{
				tabs[tabs.length] = [context.pollPaneId, context.pollPaneText, null];
			}
			tabs[tabs.length] = [context.previewPaneId, context.previewPaneText, function() { _preview(context); }];
			
			context.tabs.glowTabbedPanes({
				cssClass:'tab-pane',
				tabSetCssClass:'tab-set with-panes',
				tabCssClasses:['tab'],
				tabSelectedCssClasses:['tab selected'],
				tabHoverCssClasses:['tab hover'],
				enableResizing:false,
				tabs: tabs
			});

			context.tagBox.evolutionTagTextBox({allTags:context.tags});
			context.selectTags.click(function() 
			{
				context.tagBox.evolutionTagTextBox('openTagSelector');
				return false;
			});
			if (context.tags.length == 0)
			{
				context.selectTags.hide();
			}
			
			_attachHandlers(context);
			_addValidation(context);
		}
	};
})(jQuery);
