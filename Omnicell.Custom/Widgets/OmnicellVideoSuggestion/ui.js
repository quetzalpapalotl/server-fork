jQuery(document).ready(function() {
    
   if (typeof $.defakto === 'undefined') { $.defakto = {}; }
   if (typeof $.defakto.emailSubmission === 'undefined') { $.defakto.emailSubmission = {}; }
   
        $('#videoLink').click(function(e) {
            e.preventDefault();
            $("#emailbody").val("");
            $("#name").val("");
            $("#email").val("");
            $('select#groupSelect').prop('selectedIndex', 0);
            $('.texterror').css("display", "none");
            $('.fnameError').css("display", "none");
            $('.addressError').css("display", "none");
            
            $("#emailForm").modal({
                overlayCss: {
                 backgroundColor: '#000',
                 cursor: 'wait'
               },
               containerCss: {      
                 backgroundColor: '#fff',
                 width: 500,
                 height: 600,
                 border: '3px solid #ccc'
               }
            });
        });
     
        var submitForm = function(context) {
            var saveurl = context.saveurl;
            var body = $('#emailbody').val();
            var body = body.trim();
            var product = $('#groupSelect').val();
            var data = {
                EmailBody: body,
                Name: $('#name').val(),
                Email: $('#email').val(),
                Product: $("#groupSelect").val()
            };
            $.telligent.evolution.post({
                url: saveurl,
                data: data,
                success: function(response) {
                    $.modal.close();
                    alert("Thank you for your submission");
                },
                 defaultErrorMessage : "Error",
                  error : function (xhr, desc, ex) {
                      $.telligent.evolution.notifications.show(desc+ex, {
                  		type : 'error'
                  	});
                      $.modal.close();
                }
            });
            
        };
        
      _attachHandlers = function(context) {
          
            
          $('#submitemail').click(function(e) {
               e.preventDefault();
               var errorCount = 0;
               
               var body = $('#emailbody').val();
               var name = $('#name').val();
               var email = $('#email').val();
               body = body.trim();
               name = name.trim();
               email = email.trim();
               
               $('.texterror').hide();
               $('.fnameError').hide();
               $('.addressError').hide();
               
               if(body.length == 0) {
                   $('.texterror').show();
                   errorCount++;
              }
              if(name.length == 0) {
                  $('.fnameError').show();
                  errorCount++;
              }
              if(email.length == 0) {
                  $('.addressError').show();
                  errorCount++;
              }else {
                  var validFormat = /^\w(\.?\w)*@\w(\.?[-\w])*\.([a-z]{3}(\.[a-z]{2})?|[a-z]{2}(\.[a-z]{2})?)$/i;
                  var eValid = validFormat.test(email);
                  if(!eValid) {
                      errorCount++;
                      $('.addressError').show();
                  } 
              }  
              if(errorCount == 0) {
                 submitForm(context);
              }
              
          });
         
     };
     

     $.defakto.emailSubmission = {
        register : function (context) {
          
          _attachHandlers(context);
       
    	}
    };
});