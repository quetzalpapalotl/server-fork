(function($)
{
	if (typeof $.telligent === 'undefined')
	        $.telligent = {};

	if (typeof $.telligent.evolution === 'undefined')
	    $.telligent.evolution = {};

	if (typeof $.telligent.evolution.widgets === 'undefined')
	    $.telligent.evolution.widgets = {};

	var _requestFriendship = function(accessingUserId, userId, message)
	{
		var data = {
			AccessingUserId: accessingUserId,
			RequesteeId: userId,
			RequestMessage: message
		};

		$.telligent.evolution.post({
				url: $.telligent.evolution.site.getBaseUrl() + 'api.ashx/v2/users/{AccessingUserId}/friends.json',
				data: data,
				success: function(response)
				{
					_reloadPage();
				}
			});
	},
	_deleteFriendship = function(accessingUserId, userId)
	{
		var data = {
			AccessingUserId: accessingUserId,
			RequesteeId: userId
		};

		$.telligent.evolution.del({
				url: $.telligent.evolution.site.getBaseUrl() + 'api.ashx/v2/users/{AccessingUserId}/friends/{RequesteeId}.json',
				data: data,
				success: function(response)
				{
					_reloadPage();
				}
			});
	}
	_reloadPage = function()
	{
		try
		{
			window.location = window.location;
		} catch (e) { };
	}

	$.telligent.evolution.widgets.userBanner =
	{
		register: function(context)
		{
			if (context.inEditMode)
			{
				$('#' + context.editAvatar).click(function() {
					$.glowModal(context.editAvatarUrl, {
						width:560,
						height:300,
						onClose: function() { 
							// load the updated avatar
							$.telligent.evolution.get({
								url: context.avatarUrl,
								data: { w_userId: context.userId },
								success: function(response) {
									var avatar = $(context.wrapperId + ' .user-avatar img');
									var newAvatar = $(response.avatar);
									avatar.after(newAvatar).remove();
								}
							});
						}
					});

					return false;
				});

				$.telligent.evolution.navigationConfirmation.enable();
				$.telligent.evolution.navigationConfirmation.register($('#' + context.cancel + ', #' + context.save));

				$('#' + context.save).click(function() {
						$.telligent.evolution.editableGroup.save('User', {
							success: function() {
								window.location = context.viewUrl;
							}
						});

						return false;
				});
			}
			else
			{
				$('#' + context.friendRequest).click(function() {
					if (context.friendRequestUrl)
					{
						$.glowModal(context.friendRequestUrl, {
							width: 670,
							height: 390
						});
					}
					else
					{
						_requestFriendship(context.accessingUserId, context.userId, context.friendRequestMessage);
					}

					return false;
				});

				$('#' + context.friendDelete).click(function() {
					if (!context.friendDeleteConfirmation || window.confirm(context.friendDeleteConfirmation))
					{
						_deleteFriendship(context.accessingUserId, context.userId);
					}
					return false;
				});

				$('#' + context.friendCancel).click(function() {
					if (!context.friendCancelConfirmation || window.confirm(context.friendCancelConfirmation))
					{
						_deleteFriendship(context.accessingUserId, context.userId);
					}
					return false;
				});
			}
		}
	};
})(jQuery);
