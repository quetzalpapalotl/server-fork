
// format strings like in C#
String.prototype.format = String.prototype.f = function() {
    var s = this,
        i = arguments.length;

    while (i--) {
        s = s.replace(new RegExp('\\{' + i + '\\}', 'gm'), arguments[i]);
    }
    return s;
};

// close modal
function closeModal(e) {
    $.modal.close();  
    return false;
}

(function($){    
    
    $('.image').each(function() {
        var $parent = $(this).parent();
        
        if(!$parent.next('br').length) {
            var image = $parent.children('.image');
            image.addClass('inlineImage');
            image.removeClass('image');
        }
        
    });
    
    $('.stepresult').find('.inlineImage').each(function() {
            $(this).removeClass('inlineImage');
            $(this).addClass('image');
    });
    
     $('.ExistingPageLink').each(function() {
        var $parent = $(this).parent();
        
        if($parent.attr('class') != undefined) {
            var $class = $parent.attr('class');
            var $href = $(this).attr('href');
            $href = $href + '#' + $class;
            
            $(this).attr('href', $href);
              
        }
          
    });
    
    $('.tableImg').each(function() {
        var iWidth = $(this).attr('width') + "px";
        var iHeight = $(this).attr('height') + "px";
        
        $(this).attr('width', iWidth);
        $(this).attr('height', iHeight);
        
    });
    
    $('.videoPopup').click(function(e) {
        e.preventDefault();
        $.modal.close();
        var href = $(this).attr('href');
         var obj = '<object style="padding-top: 120px; padding-left: 150px;" data="{0}" width="640" height="460"><param name="src" value="{1}"/><param name="movie" value="{2}"/></object>'.format(href, href, href);
       // obj.data(href);
       $.modal(obj, {
        overlayCss: {
            backgroundColor: '#000',
            cursor: 'wait'
          },
          containerCss: {      
            backgroundColor: '#fff',
            width: 800,
            height: 500,
            border: '3px solid #ccc'
          }
        });
    });
   
    // namespace client code to avoid collisions
    $.defakto = $.defakto || {};
    $.defakto.ditawiki = {
        register: function(options) {
            
        }
    }
    
})(jQuery);




