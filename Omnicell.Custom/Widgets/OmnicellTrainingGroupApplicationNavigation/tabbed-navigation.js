var windowState = "large";

(function($)
{
    if (typeof $.telligent === 'undefined') { $.telligent = {}; }
	if (typeof $.telligent.evolution === 'undefined') { $.telligent.evolution = {}; }
	if (typeof $.telligent.evolution.widgets === 'undefined') { $.telligent.evolution.widgets = {}; }
	
    var sw = document.body.clientWidth;
    if(sw < 735) {
        smMenu();
    }
    else {
        lgMenu();
    }
    
    $(window).resize(function () {
        var sw = document.body.clientWidth;
        if(sw < 735 && windowState == 'large') {
            smMenu();
        }
        if(sw > 734 && windowState == 'small') {
            lgMenu();
        }
    });
    
    	$.telligent.evolution.widgets.groupApplicationNavigation = {
		register: function(context) {
		    
		}
	};   
    
}(jQuery));

function smMenu() {
    $('.training-navigation').each(function () {
        
       // change list to select
        var select = $('<select />');
        select.addClass("navigation-dropdown");
        
        //populate select with links from the list
        $(this).find('a').each(function() {
            if($(this).hasClass('add-post')) {
                $('.training-nav-footer').append($(this));
            }
            // go through eack link and create an option in the select for each one
            var option = $("<option />");
            
            //populate the option with data from the links
            option.attr('value', $(this).attr('href')).html($(this).html());
            option.attr('title', $(this).text());
            
            //add the option to the select element
            if($(this).attr('href') != "#") {
                select.append(option);
            }
        });
        
        // when an option is selected, navigate to the selected page
        select.change(function () {
            window.location = $(this).find("option:selected").val();
        });
        
        // target the ul and replace it with the generated select element
        $(this).replaceWith(select);
        setSelected();
        $('.navigation-dropdown').prev('.navigation-list-header').text("Training Navigation");
        
    });
    
    // set the current window state to small
    windowState = "small";
    
}

function lgMenu() {
    // target the select menu
    $('.navigation-dropdown').each(function() {
        // create unordered list
        var ul = $("<ul />");
        ul.addClass("navigation-list");
        ul.addClass("training-navigation");
        
        // go through the select and cycle through each option
        $(this).find('option').each(function() {
            //for each option create an li and an anchor
            var li = $('<li />');
            li.addClass('navigation-item');
            var anchor = $('<a />');
            anchor.addClass('internal-link');
            anchor.addClass('view-post');
            
            //populate the anchor attributes from the option
            anchor.attr('href', $(this).attr('value')).html($(this).html());
            anchor.text($(this).attr('title'));
            
            //add the li and anchors to the ul
            ul.append(li);
            li.append(anchor);
            
        });
        
        // add the new post button back to the list
      
            var li = $('<li />');
            li.addClass('navigation-item');
            li.addClass('add-post');
            li.append($('a.add-post'));
            ul.append(li);
            
            
        
        // replace the select with the generated ul
        $(this).replaceWith(ul);
    });
    
    // set the window state
    windowState = "large";
    
    // set the current tab
    setCurrent();
    $('.training-navigation').prev('.navigation-list-header').text("");
}

// set the current tab
function setCurrent() {
    var title = document.documentURI;
    title = $.trim(title).toLowerCase();
    
    var listItems = $(".training-navigation li");
    listItems.each(function(idx, li) {
        var anchorText = "http://omnicelldev.defaktolabs.com" + $(li).find('a:first').attr('href');
        anchorText = $.trim(anchorText).toLowerCase();
        if(anchorText === title) {
            $(li).addClass('selected');
            return false;
        }
    });
}

function setSelected() {
    var title = document.documentURI;
    title = $.trim(title).toLowerCase();
    
    $('.navigation-dropdown option').each(function (idx, opt) {
        var url = "http://omnicelldev.defaktolabs.com" + $(this).attr('value');
        if(url === title) {
            $('.navigation-dropdown').val($(this).val());
            return false;
        }
        
    });
} 