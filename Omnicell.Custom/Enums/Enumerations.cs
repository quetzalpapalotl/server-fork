﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Omnicell.Custom
{
    public enum TrainingCourseType
    {
        Classroom_Training = 1,
        Onsite_Training = 2,
        Course_With_Lab = 3,
        Course_Without_Lab = 4
    }

}
