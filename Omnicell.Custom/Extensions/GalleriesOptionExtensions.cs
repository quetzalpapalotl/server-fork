﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Telligent.Evolution.Extensibility.Api.Version1;
using Telligent.Evolution.Extensibility.Api.Entities.Version1;

using Omnicell.Custom.Components;

namespace Omnicell.Custom
{
    public static class GalleriesOptionExtensions
    {
        public static void MediaFormatFilters(this GalleriesUpdateOptions o, List<MediaFormat> formatFilters)
        {
            if (o.ExtendedAttributes == null)
                o.ExtendedAttributes = new List<ExtendedAttribute>();

            o.ExtendedAttributes.Add(new ExtendedAttribute { Key = Constants.GALLERY_MEDIA_FORMAT_FILTER, Value = formatFilters.ToDelimitedString() });
        }

        public static void CourseTypeFilters(this GalleriesUpdateOptions o, List<CourseType> formatFilters)
        {
            if (o.ExtendedAttributes == null)
                o.ExtendedAttributes = new List<ExtendedAttribute>();

            o.ExtendedAttributes.Add(new ExtendedAttribute { Key = Constants.GALLERY_COURSE_TYPE_FILTER, Value = formatFilters.ToDelimitedString() });
        }

        public static void GalleryType(this GalleriesUpdateOptions o, int galleryType)
        {
            if (o.ExtendedAttributes == null)
                o.ExtendedAttributes = new List<ExtendedAttribute>();
            o.ExtendedAttributes.Add(new ExtendedAttribute { Key = Constants.GALLERY_GALLERY_TYPE, Value = galleryType.ToString() });
        }
        public static void ShowCourseInfo(this GalleriesUpdateOptions o, bool showCourseInfo)
        {
            if (o.ExtendedAttributes == null)
                o.ExtendedAttributes = new List<ExtendedAttribute>();
            o.ExtendedAttributes.Add(new ExtendedAttribute { Key = Constants.GALLERY_SHOW_COURSE_INFO, Value = showCourseInfo.ToString() });
        }
        public static void CourseDateType(this GalleriesUpdateOptions o, int dateType)
        {
            if (o.ExtendedAttributes == null)
                o.ExtendedAttributes = new List<ExtendedAttribute>();
            o.ExtendedAttributes.Add(new ExtendedAttribute { Key = Constants.GALLERY_COURSE_DATE_TYPE, Value = dateType.ToString() });
        }

    }
}
