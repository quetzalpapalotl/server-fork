﻿using System;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web;
using HiQPdf;
using Microsoft.SharePoint.Client;
using Omnicell.Data.Model;
using Telligent.Evolution.Components;
using Telligent.Evolution.Components.Search;
using Telligent.Evolution.Extensibility.Api.Version1;
using Telligent.Evolution.Extensibility.Storage.Version1;
using V1Entities = Telligent.Evolution.Extensibility.Api.Entities.Version1;


namespace Omnicell.Custom.Components
{
    public static class WikiPageExtensions
    {

        public static byte[] ImageToByteArray(string filePath)
        {

            using (MemoryStream ms = new MemoryStream())
            {
                // Convert Image to byte[]
                System.Drawing.Image cImage = System.Drawing.Image.FromFile(filePath);
                ImageFormat format = cImage.RawFormat;
                cImage.Save(ms, format);
                byte[] imageBytes = ms.ToArray();

                // Convert byte[] to Base64 String
                string imageString = Convert.ToBase64String(imageBytes);

                return imageBytes;

            }
        }

        public static byte[] CentralFileImageToByteArray(string url, string apiKey)
        {
            ICentralizedFile cfs = null;
            byte[] imageBytes = null;
            Telligent.Evolution.Components.User admUser = Telligent.Evolution.Users.GetUser("admin");

            RunAsUser(() => cfs = CentralizedFileStorage.GetCentralizedFileByUrl(url), new ContextService().GetExecutionContext(), admUser);

            if (cfs != null)
            {
                imageBytes = cfs.OpenReadStream().ToByteArray();
            }

            return imageBytes;
        }

        public static byte[] StreamToByteArray(MemoryStream stream)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                stream.CopyTo(ms);
                return ms.ToArray();
            }

        }

        public static string ImageBase64(string filePath)
        {

            using (MemoryStream ms = new MemoryStream())
            {
                // Convert Image to byte[]
                System.Drawing.Image cImage = System.Drawing.Image.FromFile(filePath);
                ImageFormat format = cImage.RawFormat;
                cImage.Save(ms, format);
                byte[] imageBytes = ms.ToArray();

                // Convert byte[] to Base64 String
                string imageString = Convert.ToBase64String(imageBytes);

                return imageString;

            }
        }

        public static int WikiPageExists(int wikiId, string contentId)
        {
            int fileId = -1;
            int pageSize = 5000;

            try
            {
                string tag = contentId;
                //string tag = "Cerner";
                Guid docGuid;
                bool? includeDisabled = true;

                WikiPagesListOptions pOptions = new WikiPagesListOptions();

                pOptions.PageSize = pageSize;
                pOptions.IncludeDisabledPages = includeDisabled;
                pOptions.QueryType = "AllByTags";
                pOptions.Tags = tag;
                var children = PublicApi.WikiPages.List(wikiId, pOptions).ToList();

                if (Guid.TryParse(contentId, out docGuid))
                {

                    foreach (var child in children)
                    {

                        string docId = child.Tags.FirstOrDefault(s => s.StartsWith("DocGUID:"));
                        docId = docId.Replace("DocGUID:", "");
                        Guid cGuid;

                        if (Guid.TryParse(docId, out cGuid))
                        {
                            if (docGuid.CompareTo(cGuid) == 0)
                            {
                                fileId = (int)child.Id;
                                break;
                            }
                        }


                    }
                }
                else
                {
                    foreach (var child in children)
                    {
                        string docId = child.Tags.FirstOrDefault(s => s.StartsWith("DocGUID:"));
                       
                        //docId = docId.Replace("DocGUID:", "");

                        if (docId.ToLower().Equals(contentId.ToLower()))
                        {
                            fileId = (int)child.Id;
                            break;
                        }
                    }
                }


            }
            catch (Exception ex)
            {
                new CSException(CSExceptionType.UnknownError, string.Format("Error retrieving wiki page with Id: {0}, and contentId: {1}", wikiId, contentId), ex).Log();
            }


            return fileId;
        }

        public static int WikiExists(int groupId, string key)
        {
            int fileId = -1;
            try
            {
                // get list of pages in the wiki and see if this one exists
                List<FieldFilter> fieldFilters = new List<FieldFilter>
                {
                    new FieldFilter { Id = groupId, FieldName = SearchFields.GroupID, FieldValue = groupId.ToString() }
                };


                SearchResultsListOptions searchOptions = new SearchResultsListOptions
                {
                    PageIndex = 0,
                    PageSize = 5000,
                    FieldFilters = fieldFilters
                };

                V1Entities.SearchResults results = PublicApi.Search.List(searchOptions);
                if (results.Count > 0)
                {
                    fileId = Convert.ToInt32(results[0].ContentId);
                }

            }
            catch (Exception ex)
            {
                new CSException(CSExceptionType.UnknownError, string.Format("Error retrieving wiki  with  groupId: {0}, and key: {1}", groupId, key), ex).Log();
            }


            return fileId;
        }

        public static void SaveMetaData(OmnicellWikiMetadata meta)
        {
            try
            {

                using (var db = new OmnicellEntities())
                {
                    Omnicell.Data.Model.Omnicell_WikiMetaData dbEntity = null;

                    if (!string.IsNullOrEmpty(meta.WikiId.ToString()))
                    {
                        dbEntity = db.Omnicell_WikiMetaData2.FirstOrDefault(x => x.WikiId == meta.WikiId);

                        if (dbEntity != null)
                        {


                            db.Omnicell_WikiMetaData2.DeleteObject(dbEntity);

                        }

                        dbEntity = new Omnicell_WikiMetaData
                        {
                            WikiId = meta.WikiId,
                            Product = meta.Products,
                            Interfaces = meta.Interfaces,
                            GroupId = meta.GroupId,
                            DocumentType = meta.DocumentType,
                            ContentId = meta.DocumentId,
                            Features = meta.Features,
                            Audiences = meta.Audiences,
                            ProductVersion = meta.ProductVersion,
                            RevNum = meta.RevNum,
                            PartName = meta.PartName
                        };

                        db.Omnicell_WikiMetaData2.AddObject(dbEntity);
                        db.SaveChanges();
                    }

                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public static void SavePageMetaData(OmnicellWikiPageMetadata meta)
        {
            try
            {
                using (var db = new OmnicellEntities())
                {
                    Omnicell.Data.Model.WikiPageMetaData dbEntity = null;

                    if (!string.IsNullOrEmpty(meta.WikiPageId.ToString()))
                    {
                        dbEntity = db.WikiPageMetaDatas.FirstOrDefault(x => x.WikiPageId == meta.WikiPageId);

                        if (dbEntity != null)
                        {
                            db.WikiPageMetaDatas.DeleteObject(dbEntity);
                        }

                        dbEntity = new WikiPageMetaData
                        {
                            ContentId = meta.contentId,
                            Product = meta.Products,
                            ProductVersion = meta.ProductVersion,
                            Audience = meta.Audiences,
                            Interfaces = meta.Interfaces,
                            Features = meta.Features,
                            DocumentType = meta.DocumentType,
                            GroupId = meta.GroupId,
                            WikiId = meta.WikiId,
                            ParentPageId = meta.ParentPageId,
                            OrderNumber = meta.OrderNumber,
                            PageKey = meta.PageKey,
                            WikiPageId = meta.WikiPageId,
                            ShortDesc = meta.ShortDesc,
                            RevNum = meta.RevNum,
                            PartName = meta.PartName,
                            FileName = meta.FileName
                        };

                        db.WikiPageMetaDatas.AddObject(dbEntity);
                        db.SaveChanges();
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public static void CreateHiQPdf(string cssString, string body, string topicTitle, string copy, string imgUrl, string footUrl, string copyYear)
        {
            HtmlToPdf htmlConverter = new HtmlToPdf();
            htmlConverter.Document.PageSize = PdfPageSize.Letter;
            htmlConverter.Document.PageOrientation = PdfPageOrientation.Portrait;
            PdfMargins margins = new PdfMargins(45f, 40f, 20f, 20f);
            htmlConverter.Document.Margins = margins;
            htmlConverter.Document.FitPageWidth = true;

            htmlConverter.SerialNumber = "BExtVVRg-YkhtZnZl-dn08KjQk-NSQ3JD0z-NSQ3NSo1-Nio9PT09";
            SetHQHeader(htmlConverter.Document);
            SetHQFooter(htmlConverter.Document, copy, copyYear, footUrl);
            
            byte[] pdfBuffer = null;

            pdfBuffer = htmlConverter.ConvertHtmlToMemory(body, "");

            string title = topicTitle.Replace(" ", String.Empty).Replace("&amp;", "&").Replace("&#160;", "tm");
            //HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.ContentType = "application/pdf";
            //HttpContext.Current.Response.AddHeader("ContentType", "application/pdf");
            HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename=" + title + ".pdf");
            HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);

            // write the PDF buffer to HTTP response
            HttpContext.Current.Response.BinaryWrite(pdfBuffer);
           
            HttpContext.Current.Response.Flush();
            HttpContext.Current.Response.End();
           
        }

        public static void SetHQHeader(PdfDocumentControl htmlConverter)
        {
            // enable header display
            htmlConverter.Header.Enabled = true;

            htmlConverter.Header.Height = 50f;

            float pdfPageWidth = htmlConverter.PageOrientation == PdfPageOrientation.Portrait ?
                                        htmlConverter.PageSize.Width : htmlConverter.PageSize.Height;

            float headerWidth = pdfPageWidth - htmlConverter.Margins.Left - htmlConverter.Margins.Right;
            float headerHeight = htmlConverter.Header.Height;
            string header = string.Format(@"<span style=""font-family: Arial; font-size: 50pt;"">Printed from <strong>myOmnicell.com</strong> | </span>");
            PdfHtml headerHtml = new PdfHtml(320, 5, header, null);
            headerHtml.FitDestHeight = false;
            htmlConverter.Header.Layout(headerHtml);

            // add page numbering
            System.Drawing.Font pageNumberingFont = new System.Drawing.Font(new System.Drawing.FontFamily("Arial"),
                                        11, System.Drawing.GraphicsUnit.Point);
            PdfText pageNumberingText = new PdfText(490, 8, " {CrtPage}", pageNumberingFont);
            //pageNumberingText.HorizontalAlign = PdfTextHAlign.Center;
            pageNumberingText.EmbedSystemFont = true;
            htmlConverter.Header.Layout(pageNumberingText);

        }

        public static void SetHQFooter(PdfDocumentControl htmlConverter, string copy, string copyYear, string footUrl)
        {

            // enable footer display
            htmlConverter.Footer.Enabled = true;

            htmlConverter.Footer.Height = 70f;


            float pdfPageWidth = htmlConverter.PageOrientation == PdfPageOrientation.Portrait ?
                                        htmlConverter.PageSize.Width : htmlConverter.PageSize.Height;

            float footerWidth = pdfPageWidth - htmlConverter.Margins.Left - htmlConverter.Margins.Right;
            //float footerWidth = pdfPageWidth;
            float footerHeight = htmlConverter.Footer.Height;

            //layout HTML in footer
            string footer = string.Format(@"<div style=""border-top: 1pt solid #222e36; font-family: Arial""><p style=""font-size: 18pt;"">{0}</p><p style=""font-size: 14pt;"">{1} Alteration of this material is prohibited without express written permission of the copyright holder. Periodically, check <a href=""https://myomnicell.com"">myomnicell.com</a> for updates to this material.</p></div>", copy, copyYear);
            PdfHtml footerHtml = new PdfHtml(5,5, footer, null);
            
            //footerHtml.FitDestHeight = true;
            htmlConverter.Footer.Layout(footerHtml);

            // image processing
            Telligent.Evolution.Components.User admUser = Telligent.Evolution.Users.GetUser("admin");


            ICentralizedFile cfs = null;
            RunAsUser(() => cfs = CentralizedFileStorage.GetCentralizedFileByUrl(footUrl), new ContextService().GetExecutionContext(), admUser);

            if (cfs != null)
            {
                System.Drawing.Image img = null;
                RunAsUser(() => img = System.Drawing.Image.FromStream(cfs.OpenReadStream()), new ContextService().GetExecutionContext(), admUser);

                 // create footer image
                string footerImage = copy;
                HiQPdf.PdfImage logo = new HiQPdf.PdfImage(footerWidth - 80 - 5, 18, 70, img);
                logo.PreserveAspectRatio = true;
                htmlConverter.Footer.Layout(logo);

            }

           


        }


        private static object _runAsUserLock = new object();
        public static void RunAsUser(Action a, Telligent.Evolution.Components.IExecutionContext context, Telligent.Evolution.Components.User u)
        {
            lock (_runAsUserLock)
            {
                var originalUser = context.User;
                try
                {
                    context.User = u;
                    a();
                }
                finally
                {
                    context.User = originalUser;
                }
            }
        }
    }
}
