﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;

using Omnicell.Custom.Components;

namespace Omnicell.Custom
{
    public static class DictionaryExtensions
    {
        public static string ToDelimitedString(this Dictionary<int, string> dict)
        {
            return String.Join(";", dict.OrderBy(_kv => _kv.Key).Zip(dict, (kv, sec) => String.Join("=", kv.Key, kv.Value)));
        }
        public static Dictionary<int, string> FromDelimitedString(this IDictionary<int, string> dict, string delimitedValues)
        {
            Dictionary<int, string> newDict = null;
            if (!string.IsNullOrEmpty(delimitedValues))
            {
                newDict = Regex.Matches(delimitedValues, @"\s*(.*?)\s*=\s*(.*?)\s*(;|$)")
                    .OfType<Match>()
                    .ToDictionary(m => int.Parse(m.Groups[1].Value), m => m.Groups[2].Value);
            }
            else
            {
                newDict = new Dictionary<int, string>();
            }
            return newDict;
        }

        public static T GetValue<T>(this IDictionary dict, object key)
        {
            return dict.GetValue<T>(key, default(T));
        }
        public static T GetValue<T>(this IDictionary dict, object key, T defaultValue)
        {
            T returnValue = defaultValue;
            if (dict[key] != null)
            {
                returnValue = (T)Convert.ChangeType(dict[key], typeof(T));
            }
            return returnValue;
        }
        public static string GetValue(this IDictionary dict, string key)
        {
            string returnValue = string.Empty;
            if (dict[key] != null)
                returnValue = dict[key].ToString();

            return returnValue;
        }
        public static bool FieldExists(this IDictionary dict, string key)
        {
            bool exists = (dict[key] != null);
            return exists;
        }

        public static string ToDelimitedString(this IEnumerable<LookupType> list)
        {
            return String.Join(";", list.OrderBy(_kv => _kv.Id).Zip(list, (kv, sec) => String.Join("=", kv.Id, kv.Value)));
        }
        public static IEnumerable<T> FromDelimitedString<T>(this IEnumerable<T> list, string delimitedValues)
            where T: LookupType, new()
        {
            List<T> newList = null;
            if (!string.IsNullOrEmpty(delimitedValues))
            {
                newList = 
                    Regex.Matches(delimitedValues, @"\s*(.*?)\s*=\s*(.*?)\s*(;|$)")
                        .OfType<Match>()
                        .Select(m => new T { Id = int.Parse(m.Groups[1].Value), Value = (m.Groups[2].Value) })
                        .ToList();
            } else {
                newList = new List<T>();
            }
            return newList;
        }

        public static IEnumerable<T> FromDelimitedString<T>(this IEnumerable<T> list, string delimitedValues, string delimiter)
            where T : LookupType, new()
        {
            List<T> newList = null;
            if (!string.IsNullOrEmpty(delimitedValues))
                newList = delimitedValues.ToString().Split(delimiter.ToCharArray()).Select(m => new T { Id = 0, Value = m }).ToList();
            else
                newList = new List<T>();

            return newList;
        }


    }
}
