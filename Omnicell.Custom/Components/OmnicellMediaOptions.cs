﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Omnicell.Custom.Components
{
    public class OmnicellMediaOptions
    {
        public int PostId { get; set; }
        public int DocumentTypeId { get; set; }
        public string AudienceTypeIdList { get; set; }
        public string ProductIdList { get; set; }
        public DateTime PublishDate { get; set; }
        public DateTime ScheduledDate { get; set; }
        public string Versions { get; set; }
        public string FeatureList { get; set; }
        public string Interfaces { get; set; }
    }
}
