﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Telligent.Caching;
using Telligent.Common;
using Telligent.Evolution.Components;
using Telligent.Evolution.MediaGalleries.Components;

using Omnicell.Data.Model;

namespace Omnicell.Custom.Components
{
    public interface ITrainingService
    {
        Dictionary<int, string> ListFormatTypes();
        Dictionary<int, string> ListInstructorCourseTypes();
        Dictionary<int, string> ListSubscriberScheduledCourseTypes();
        string GetCourseTypeName(int courseTypeId);
    }
    public class TrainingService : ITrainingService
    {
        private readonly ICacheService _cache = null;

        public TrainingService()
        {
            _cache = Services.Get<ICacheService>();
        }

        #region Format Types

        public Dictionary<int, string> ListFormatTypes()
        {
            Dictionary<int, string> filters = GetFormatTypesFromCache();
            if (filters == null)
            {
                var db = new OmnicellEntities();
                filters = db.MediaFormats.ToDictionary(i=> i.Id, i=> i.Value);

                if(filters!= null && filters.Count > 0)
                {
                    PushFormatTypesToCache(filters);
                }
            }
            return filters;
        }

        #endregion Format Types

        #region Instructor Course Type

        public Dictionary<int, string> ListInstructorCourseTypes()
        {
            Dictionary<int, string> typeList = GetInstructorCourseTypeListFromCache();
            if (typeList == null)
            {
                typeList = new Dictionary<int,string>();
                foreach(var item in Enum.GetValues(typeof(InstructorCourseType)))
                {
                    typeList.Add((int)Enum.Parse(typeof(InstructorCourseType), item.ToString()), Enum.GetName(typeof(InstructorCourseType),item).Replace("_"," "));
                }

                PushInstructorCourseTypeListToCache(typeList);
            }
            return typeList;
        }

        #endregion Instructor Course Type

        #region SubscriberScheduled Course Type

        public Dictionary<int, string> ListSubscriberScheduledCourseTypes()
        {
            Dictionary<int, string> typeList = GetSubscriberScheduledCourseTypeListFromCache();
            if (typeList == null)
            {
                typeList = new Dictionary<int, string>();
                foreach (var item in Enum.GetValues(typeof(SubscriberScheduledCourseType)))
                {
                    typeList.Add((int)Enum.Parse(typeof(SubscriberScheduledCourseType), item.ToString()), Enum.GetName(typeof(SubscriberScheduledCourseType), item).Replace("_", " "));
                }

                PushSubscriberScheduledCourseTypeListToCache(typeList);
            }
            return typeList;
        }

        #endregion SubscriberScheduled Course Type

        public string GetCourseTypeName(int courseTypeId)
        {
            string name = string.Empty;
            if (ListInstructorCourseTypes().Keys.Contains(courseTypeId))
                name = ListInstructorCourseTypes()[courseTypeId];
            else if (ListSubscriberScheduledCourseTypes().Keys.Contains(courseTypeId))
                name = ListSubscriberScheduledCourseTypes()[courseTypeId];

            return name;
        }

        #region Caching

        #region Format Types

        private string GetFormatTypesKey(string key)
        {
            return string.Format("PK_FORMAT_FILTERS::{0}", key);
        }
        private void PushFormatTypesToCache(Dictionary<int, string> filters)
        {
            PushFormatTypesToCache("ALL-FORMATS", filters);
        }
        private void PushFormatTypesToCache(string key, Dictionary<int, string> filters)
        {
            _cache.Put(GetFormatTypesKey(key), filters, CacheScope.All);
        }
        private Dictionary<int, string> GetFormatTypesFromCache()
        {
            return GetFormatTypesFromCache("ALL-FORMATS");
        }
        private Dictionary<int, string> GetFormatTypesFromCache(string key)
        {
            return _cache.Get(GetFormatTypesKey(key), CacheScope.All) as Dictionary<int, string>;
        }
        private void RemoveFormatTypesFromCache()
        {
            RemoveFormatTypesFromCache("ALL-FORMATS");
        }
        private void RemoveFormatTypesFromCache(string key)
        {
            _cache.Remove(GetFormatTypesKey(key), CacheScope.All);
        }

        #endregion Format Types

        #region Instructor Course Type

        public string GetInstructorCourseTypeListKey(string key)
        {
            return string.Format("PK_INSTRUCTOR_COURSE_TYPE_LIST::{0}", key);
        }
        public void PushInstructorCourseTypeListToCache(Dictionary<int, string> types)
        {
            PushInstructorCourseTypeListToCache("ALL-TYPES", types);
        }
        public void PushInstructorCourseTypeListToCache(string key, Dictionary<int, string> types)
        {
            TimeSpan expire = new TimeSpan(2, 0, 0);
            _cache.Put(GetInstructorCourseTypeListKey(key), types, CacheScope.All, expire);
        }
        public Dictionary<int, string> GetInstructorCourseTypeListFromCache()
        {
            return GetInstructorCourseTypeListFromCache("ALL-TYPES");
        }
        public Dictionary<int, string> GetInstructorCourseTypeListFromCache(string key)
        {
            return _cache.Get(GetInstructorCourseTypeListKey(key), CacheScope.All) as Dictionary<int, string>;
        }
        public void RemoveInstructorCourseTypeListFromCache()
        {
            RemoveInstructorCourseTypeListFromCache("ALL-TYPES");
        }
        public void RemoveInstructorCourseTypeListFromCache(string key)
        {
            _cache.Remove(GetInstructorCourseTypeListKey(key), CacheScope.All);
        }

        #endregion Instructor Course Type

        #region SubscriberScheduled Course Type

        public string GetSubscriberScheduledCourseTypeListKey(string key)
        {
            return string.Format("PK_SUB_SCHED_COURSE_TYPE_LIST::{0}", key);
        }
        public void PushSubscriberScheduledCourseTypeListToCache(Dictionary<int, string> types)
        {
            PushSubscriberScheduledCourseTypeListToCache("ALL-TYPES", types);
        }
        public void PushSubscriberScheduledCourseTypeListToCache(string key, Dictionary<int, string> types)
        {
            TimeSpan expire = new TimeSpan(2, 0, 0);
            _cache.Put(GetSubscriberScheduledCourseTypeListKey(key), types, CacheScope.All, expire);
        }
        public Dictionary<int, string> GetSubscriberScheduledCourseTypeListFromCache()
        {
            return GetSubscriberScheduledCourseTypeListFromCache("ALL-TYPES");
        }
        public Dictionary<int, string> GetSubscriberScheduledCourseTypeListFromCache(string key)
        {
            return _cache.Get(GetSubscriberScheduledCourseTypeListKey(key), CacheScope.All) as Dictionary<int, string>;
        }
        public void RemoveSubscriberScheduledCourseTypeListFromCache()
        {
            RemoveSubscriberScheduledCourseTypeListFromCache("ALL-TYPES");
        }
        public void RemoveSubscriberScheduledCourseTypeListFromCache(string key)
        {
            _cache.Remove(GetSubscriberScheduledCourseTypeListKey(key), CacheScope.All);
        }

        #endregion SubscriberScheduled Course Type
        
        #endregion Caching

    }
}
