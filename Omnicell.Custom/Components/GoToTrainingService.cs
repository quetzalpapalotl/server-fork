﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;

using Telligent.Caching;
using Telligent.Common;
using Telligent.Evolution.Components;
using Telligent.Evolution.Extensibility.Version1;

using GTT = GoToTraining.Api.Components;

namespace Omnicell.Custom.Components
{
    public interface IGoToTrainingService
    {
        GTT.Training GetTraining(string trainingKey);
        List<GTT.Session> GetSessions(DateTime startDate, DateTime endDate);
        List<GTT.Session> GetSessions(string trainingKey);
        List<GTT.Attendee> GetAttendees(string sessionKey);
        List<GTT.Registrant> GetRegistrants(string trainingKey);
    }
    public class GoToTrainingService 
    {
        private readonly ICacheService _cache = null;
        private readonly GoToTrainingPlugin _gotoTraining = null;
        private readonly GoToTraining.Api.Context _gttContext = null;

        public GoToTrainingService()
        {
            _cache = Services.Get<ICacheService>();
            _gotoTraining = Telligent.Evolution.Extensibility.Version1.PluginManager.Get<GoToTrainingPlugin>().FirstOrDefault();
            _gttContext = GoToTraining.Api.Context.Instance(_gotoTraining.AccessToken);
        }

        public GTT.Training GetTraining(string trainingKey)
        {
            GTT.Training training = GetTrainingFromCache(trainingKey);
            if(training == null) {
                training = _gttContext.GetTraining(_gotoTraining.OrganizerKey, trainingKey);

                if(training != null)
                    PutTrainingInCache(training);
            }
            return training;
        }
        public List<GTT.Session> GetSessions(DateTime startDate, DateTime endDate)
        {
            List<GTT.Session> sessions = GetSessionListFromCache(startDate, endDate);
            if (sessions == null)
            {
                sessions = _gttContext.GetSessions(_gotoTraining.OrganizerKey, startDate, endDate);

                if (sessions != null && sessions.Count > 0)
                    PutSessionListInCache(startDate, endDate, sessions);
            }
            return sessions;
        }
        public List<GTT.Session> GetSessions(string trainingKey)
        {
            List<GTT.Session> sessions = GetSessionListFromCache(trainingKey);
            if (sessions == null)
            {
                sessions = _gttContext.GetSessions(_gotoTraining.OrganizerKey, trainingKey);

                if (sessions != null && sessions.Count > 0)
                    PutSessionListInCache(trainingKey, sessions);
            }
            return sessions;
        }
        public List<GTT.Attendee> GetAttendees(string sessionKey)
        {
            List<GTT.Attendee> attendees = GetAttendeeListFromCache(sessionKey);
            if (attendees == null)
            {
                attendees = _gttContext.GetAttendees(_gotoTraining.OrganizerKey, sessionKey);

                if (attendees != null && attendees.Count > 0)
                    PutAttendeeListInCache(sessionKey, attendees);
            }
            return attendees;
        }
        public List<GTT.Registrant> GetRegistrants(string trainingKey)
        {
            List<GTT.Registrant> registrants = GetRegistrantListFromCache(trainingKey);
            if (registrants == null)
            {
                registrants = _gttContext.GetRegistrants(_gotoTraining.OrganizerKey, trainingKey);

                if (registrants != null && registrants.Count > 0)
                    PutRegistrantListInCache(trainingKey, registrants);
            }
            return registrants;
        }

        #region Caching

        #region Training

        private string GetTrainingCacheKey(string trainingKey)
        {
            return string.Format("TRAINING-KEY:{0}", trainingKey);
        }
        private GTT.Training GetTrainingFromCache(string trainingKey)
        {
            return _cache.Get(GetTrainingCacheKey(trainingKey), CacheScope.All) as GTT.Training;
        }
        private void PutTrainingInCache(GTT.Training training)
        {
            _cache.Put(GetTrainingCacheKey(training.TrainingKey), training, CacheScope.All);
        }
        private void RemoveTrainingFromCache(string trainingKey)
        {
            _cache.Remove(GetTrainingCacheKey(trainingKey), CacheScope.All);
        }

        #endregion Training

        #region Sessions

        private string GetSessionListCacheKey(string key)
        {
            return string.Format("SESSIONLIST-{0}", key);
        }
        private string GetSessionListCacheKey(DateTime startDate, DateTime endDate)
        {
            string key = string.Format("FROM::{0}--TO::{1}", startDate.ToString("yyyy-MM-ddThh:mm:ssZ"), endDate.ToString("yyyy-MM-ddThh:mm:ssZ"));
            return GetSessionListCacheKey(key);
        }
        private List<GTT.Session> GetSessionListFromCache(DateTime startDate, DateTime endDate)
        {
            return GetSessionListFromCache(GetSessionListCacheKey(startDate, endDate));
        }
        private List<GTT.Session> GetSessionListFromCache(string key)
        {
            return _cache.Get(key, CacheScope.All) as List<GTT.Session>;
        }
        private void PutSessionListInCache(DateTime startDate, DateTime endDate, List<GTT.Session> sessions)
        {
            PutSessionListInCache(GetSessionListCacheKey(startDate, endDate), sessions); 
        }
        private void PutSessionListInCache(string key, List<GTT.Session> sessions)
        {
            _cache.Put(GetSessionListCacheKey(key), sessions, CacheScope.All);
        }
        private void RemoveSessionListFromCache(DateTime startDate, DateTime endDate)
        {
            RemoveSessionListFromCache(GetSessionListCacheKey(startDate, endDate));
        }
        private void RemoveSessionListFromCache(string key)
        {
            _cache.Remove(key, CacheScope.All);
        }

        #endregion Sessions

        #region Attendees

        private string GetAttendeeListCacheKey(string key)
        {
            return string.Format("ATTENDEELIST-{0}", key);
        }
        private List<GTT.Attendee> GetAttendeeListFromCache(string key)
        {
            return _cache.Get(GetAttendeeListCacheKey(key), CacheScope.All) as List<GTT.Attendee>;
        }
        private void PutAttendeeListInCache(string key, List<GTT.Attendee> attendees)
        {
            _cache.Put(GetAttendeeListCacheKey(key), attendees, CacheScope.All);
        }
        private void RemoveAttendeeListFromCache(string key)
        {
            _cache.Remove(GetAttendeeListCacheKey(key), CacheScope.All);
        }

        #endregion Attendees

        #region Registrants

        private string GetRegistrantListCacheKey(string key)
        {
            return string.Format("ATTENDEELIST-{0}", key);
        }
        private List<GTT.Registrant> GetRegistrantListFromCache(string key)
        {
            return _cache.Get(GetRegistrantListCacheKey(key), CacheScope.All) as List<GTT.Registrant>;
        }
        private void PutRegistrantListInCache(string key, List<GTT.Registrant> attendees)
        {
            _cache.Put(GetRegistrantListCacheKey(key), attendees, CacheScope.All);
        }
        private void RemoveRegistrantListFromCache(string key)
        {
            _cache.Remove(GetRegistrantListCacheKey(key), CacheScope.All);
        }
        
        #endregion Registrants

        #endregion Caching
    }
}
