﻿using System;

namespace Omnicell.Custom.Components
{
    public class RelatedDocuments
    {
        private int _id = 0;
        private DateTime _createDate = DateTime.Now;
        private DateTime _lastUpdateDate = DateTime.Now;

        public RelatedDocuments() { }

        public int ID {
            get { return _id; }
            set { _id = value; }
        }
        public int GroupId { get; set; }
        public string DocumentValues { get; set; }
        public DateTime CreateDate
        {
            get { return _createDate; }
            set { _createDate = value; } 
        }

        public DateTime LastAccessDate
        {
            get { return _lastUpdateDate; }
            set { _lastUpdateDate = value; }
        }

    }
}
