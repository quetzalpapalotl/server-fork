﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Omnicell.Custom.Components
{
    public class TrainingGalleryFilters
    {
        public int GalleryId { get; set; }
        public List<MediaFormat> MediaFormatFilters {get; set;}
        public List<CourseType> CourseTypeFilters { get; set; }
        public int GalleryType { get; set; }
        public bool ShowCourseInfo { get; set; }
        public int CourseDateType { get; set; }

        public bool ContainsMediaFormat(int formatId)
        {
            return MediaFormatFilters.Count(f => f.Id == formatId) > 0;
        }
        public bool ContainsCourseType(int typeId)
        {
            return CourseTypeFilters.Count(t => t.Id == typeId) > 0;
        }
    }
}
