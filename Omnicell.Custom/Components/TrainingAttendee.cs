﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Omnicell.Custom.Components
{
    public class TrainingAttendee
    {
        public int AttendeeId { get; set; }
        public string EmailAddress { get; set; }
        public string GivenName { get; set; }
        public string Surname { get; set; }
        public int TimeInSession { get; set; }
        public string SessionKey { get; set; }
        public string SessionStart { get; set; }
        public string SessionEnd { get; set; }
        public string TrainingName { get; set; }
        public string TrainingKey { get; set; }
    }
}
