﻿using System;
using System.Collections.Generic;
using System.Reflection;
using TApi = Telligent.Evolution.Extensibility.Api.Version1;
using TEpi = Telligent.Evolution.Extensibility.Api.Entities.Version1;


namespace Omnicell.Custom.Components
{
    public class OmnicellWiki : TEpi.Wiki
    {

        public List<OmnicellWikiPage> wikiPages;
        public OmnicellWikiMetadata wikiMeta;

        public OmnicellWiki() { }

        public OmnicellWiki(Guid contentId)
        {
            wikiPages = new List<OmnicellWikiPage>();
            var wiki = TApi.PublicApi.Wikis.Get(contentId);
            if(wiki != null)
                MapFromWiki(wiki);
        }
        
        public OmnicellWiki(int wikiId)
        {
            wikiMeta = new OmnicellWikiMetadata();
            wikiPages = new List<OmnicellWikiPage>();
            var wiki = TApi.PublicApi.Wikis.Get(new TApi.WikisGetOptions { Id = wikiId });
            if(wiki != null)
                MapFromWiki(wiki);
        }

        public OmnicellWiki(TEpi.Wiki wiki)
        {
            wikiPages = new List<OmnicellWikiPage>();
            wikiPages = new List<OmnicellWikiPage>();
            MapFromWiki(wiki);
        }

        private void MapFromWiki(TEpi.Wiki wiki)
        {
            PropertyInfo[] basePIs = typeof(TEpi.Wiki).GetProperties();
            Type thisObjectType = this.GetType();

            foreach (PropertyInfo basePI in basePIs)
            {
                PropertyInfo newPI = thisObjectType.GetProperty(basePI.Name);
                if (newPI != null)
                {
                    try
                    {
                        
                        if (newPI.CanWrite && newPI != null)
                        {
                            newPI.SetValue(this, basePI.GetValue(wiki, null), null);
                        }
                        
                    }
                    catch { }
                }
            }
        }
    }
}
