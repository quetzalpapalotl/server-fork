﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Telligent.Caching;
using Telligent.Common;
using Telligent.Evolution.Components;
using Telligent.Evolution.Extensibility.Api.Entities.Version1;
using Telligent.Evolution.Extensibility.Api.Version1;

using Omnicell.Custom.Components;
using Omnicell.Custom.Data;

namespace Omnicell.Custom.Components
{
    public interface IServiceTicketService
    {
        void BulkUpdate(string xml);
    }

    public class ServiceTicketService : IServiceTicketService
    {
        public readonly ICacheService _cache = null;

        public ServiceTicketService()
        {
            _cache = Telligent.Common.Services.Get<ICacheService>();
        }

        public void BulkUpdate(string xml)
        {
            ServiceTicketDataProvider.Instance.BulkUpdate(xml);
        }



        #region Caching

        private string GetServiceTicketPageCacheKey(int pageIndex, int pageSize)
        {
            return string.Format("PK_SERVICE_TICKET_LIST::Index:{0}-Size:{1}", pageIndex, pageSize);
        }
        private string GetServiceTicketPageTag()
        {
            return string.Format("TAG:SERVICE_TICKET_PAGE");
        }
        private string GetServiceTicketCacheKey(int serviceTicketId)
        {
            return string.Format("PK_SERVICE_TICKET::{0}", serviceTicketId);
        }
        private string GetServiceTicketCacheKey(string number)
        {
            return string.Format("PK_SERVICE_TICKET::NUMBER:{0}", number);
        }
        private string GetServiceTicketTag(int serviceTicketId)
        {
            return string.Format("TAG:SERVICETICKET:ID:{0}", serviceTicketId);
        }


        private void PushServiceTicketPageToCache(PagedList<ServiceTicket> page)
        {
            _cache.Put(GetServiceTicketPageCacheKey(page.PageIndex, page.PageSize), page, CacheScope.All, new string[] { GetServiceTicketPageTag() });
        }
        private void PushTrainingAttendeeToCache(ServiceTicket serviceTicket)
        {
            _cache.Put(GetServiceTicketCacheKey(serviceTicket.ServiceTicketId), serviceTicket, CacheScope.All);

            string[] idTag = new string[] { GetServiceTicketTag(serviceTicket.ServiceTicketId) };
            _cache.Put(GetServiceTicketCacheKey(serviceTicket.Number), serviceTicket, CacheScope.All, idTag);
        }

        private PagedList<ServiceTicket> GetServiceTicketPageFromCache(int pageIndex, int pageSize)
        {
            return _cache.Get(GetServiceTicketPageCacheKey(pageIndex, pageSize), CacheScope.All) as PagedList<ServiceTicket>;
        }
        private ServiceTicket GetServiceTicketFromCache(int serviceTicketId)
        {
            return _cache.Get(GetServiceTicketCacheKey(serviceTicketId), CacheScope.All) as ServiceTicket;
        }
        private ServiceTicket GetServiceTicketFromCache(string number)
        {
            return _cache.Get(GetServiceTicketCacheKey(number), CacheScope.All) as ServiceTicket;
        }

        private void RemoveServiceTicketFromCache(int serviceTicketId)
        {
            _cache.Remove(GetServiceTicketCacheKey(serviceTicketId), CacheScope.All);
            _cache.RemoveByTags(new string[] { GetServiceTicketTag(serviceTicketId), GetServiceTicketPageTag() }, CacheScope.All);
        }

        #endregion Caching


    }
}
