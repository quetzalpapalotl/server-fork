﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Telligent.Caching;
using Telligent.Common;
using Telligent.Evolution.Components;
using Telligent.Evolution.MediaGalleries.Components;

using Omnicell.Data.Model;

namespace Omnicell.Custom.Components
{
    public interface ITrainingService
    {
    }
    public class TrainingService : ITrainingService
    {
        private readonly ICacheService _cache = null;
        private OmnicellEntities _odb = null;

        public TrainingService()
        {
            _cache = Telligent.Common.Services.Get<ICacheService>();
            _odb = new OmnicellEntities();
        }

    }
}
