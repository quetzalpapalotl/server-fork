﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Telligent.Caching;
using Telligent.Common;
using Telligent.Evolution.Components;
using Telligent.Evolution.Extensibility.Api.Version1;
using V1Entities = Telligent.Evolution.Extensibility.Api.Entities.Version1;

using Omnicell.Data.Model;
using Omnicell.Custom.Components;

namespace Omnicell.Custom.Components
{
    public interface IDocumentService
    {
        OmnicellDocumentPost Get(int documentPostId);
        OmnicellDocumentPost Save(OmnicellMediaOptions options);
    }
    public class DocumentService : IDocumentService
    {
        private readonly ICacheService _cache = null;
        private OmnicellEntities _odb = null;

        public DocumentService()
        {
            _cache = Telligent.Common.Services.Get<ICacheService>();
            _odb = new OmnicellEntities();
        }

        public OmnicellDocumentPost Get(int documentPostId)
        {
            OmnicellDocumentPost post = GetDocumentPostFromCache(documentPostId);
            if (post == null)
            {
                V1Entities.Media media = PublicApi.Media.Get(documentPostId);
                if (media != null)
                {
                    post = new OmnicellDocumentPost(media);
                    PutDocumentPostInCache(post);
                }
            }
            return post;
        }
        public OmnicellDocumentPost Save(OmnicellMediaOptions options)
        {
            OmnicellCoursePost returnPost = null;

            if (options.PostId > 0)
            {
                RemoveDocumentPostFromCache(options.PostId);
            }

            return Get(options.PostId);
        }

        #region Caching

        private string GetDocumentPostCacheKey(int documentPostId)
        {
            return string.Format("DOCUMENT-POST:{0}", documentPostId);
        }
        private void PutDocumentPostInCache(OmnicellDocumentPost post)
        {
            _cache.Put(GetDocumentPostCacheKey(post.Id.Value), post, CacheScope.All);
        }
        private OmnicellDocumentPost GetDocumentPostFromCache(int documentPostId)
        {
            return _cache.Get(GetDocumentPostCacheKey(documentPostId), CacheScope.All) as OmnicellDocumentPost;
        }
        private void RemoveDocumentPostFromCache(int documentPostId)
        {
            _cache.Remove(GetDocumentPostCacheKey(documentPostId), CacheScope.All);
        }

        #endregion Caching
    }
}
