﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Telligent.Caching;
using Telligent.Common;
using Telligent.Evolution.Components;
using Telligent.Evolution.Extensibility.Api.Entities.Version1;
using Telligent.Evolution.Extensibility.Api.Version1;

using Omnicell.Custom.Components;
using Omnicell.Custom.Data;
using Omnicell.Custom.Search;

using OData = Omnicell.Data.Model;

namespace Omnicell.Custom.Components
{
    public interface IDocumentTypeService
    {
        DocumentType Get(int id);
        DocumentType Save(DocumentType documentType);
        IList<DocumentType> List();
        void Delete(int id);
    }
    public class DocumentTypeService : IDocumentTypeService
    {
        public readonly ICacheService _cache = null;

        public DocumentTypeService()
        {
            _cache = Telligent.Common.Services.Get<ICacheService>();
        }

        public DocumentType Get(int id)
        {
            DocumentType type = GetDocumentTypeFromCache(id);
            if (type == null)
            {
                type = DocumentTypeDataProvider.Instance.Get(id);
                if (type != null)
                    PushDocumentTypeToCache(type);
            }
            return type;
        }
        public DocumentType Save(DocumentType documentType)
        {
            RemoveDocumentTypeListFromCache();
            int id = documentType.Id;
            if (id > 0)
            {
                RemoveDocumentTypeFromCache(documentType.Id);

                DocumentTypeDataProvider.Instance.Update(documentType);
            }
            else
            {
                id = DocumentTypeDataProvider.Instance.Add(documentType);
            }

            // keep the custom db in sync
            using (var db = new OData.OmnicellEntities())
            {
                var dbEntity = db.Omnicell_MediaDocumentType.SingleOrDefault(x => x.Id == documentType.Id);
                if (dbEntity == null)
                {
                    dbEntity = new OData.Omnicell_MediaDocumentType();
                    db.Omnicell_MediaDocumentType.AddObject(dbEntity);
                }

                dbEntity.Value = documentType.Value;
                db.SaveChanges();
            }

            return Get(id);
        }
        public IList<DocumentType> List()
        {
            IList<DocumentType> list = GetDocumentTypeListFromCache();
            if (list == null)
            {
                list = DocumentTypeDataProvider.Instance.List();
                if (list != null && list.Count > 0)
                    PushDocumentTypeListToCache(list);
            }
            return list;
        }
        public void Delete(int id)
        {
            RemoveDocumentTypeListFromCache();
            RemoveDocumentTypeFromCache(id);
            DocumentTypeDataProvider.Instance.Delete(id);

            // keep the custom db in sync
            using (var db = new OData.OmnicellEntities())
            {
                var dbEntity = db.Omnicell_MediaDocumentType.SingleOrDefault(x => x.Id == id);
                if (dbEntity != null)
                {
                    db.DeleteObject(dbEntity);
                    db.SaveChanges();
                }
            }
        }

        #region Cache

        private string GetDocumentTypeCacheKey(string key)
        {
            return string.Format("PK_DOCUMENT_TYPE::{0}", key);
        }
        private string GetDocumentTypeCacheKey(int id)
        {
            return GetDocumentTypeCacheKey("ID:" + id);
        }

        private void PushDocumentTypeListToCache(IList<DocumentType> list)
        {
            PushDocumentTypeListToCache("ALL-TYPES", list);
        }
        private void PushDocumentTypeListToCache(string key, IList<DocumentType> list)
        {
            _cache.Put(GetDocumentTypeCacheKey(key), list, CacheScope.All);
        }
        private void PushDocumentTypeToCache(DocumentType type)
        {
            _cache.Put(GetDocumentTypeCacheKey(type.Id), type, CacheScope.All);
        }

        private IList<DocumentType> GetDocumentTypeListFromCache()
        {
            return GetDocumentTypeListFromCache("ALL-TYPES") as IList<DocumentType>;
        }
        private IList<DocumentType> GetDocumentTypeListFromCache(string key)
        {
            return _cache.Get(GetDocumentTypeCacheKey(key), CacheScope.All) as IList<DocumentType>;
        }
        private DocumentType GetDocumentTypeFromCache(int id)
        {
            return _cache.Get(GetDocumentTypeCacheKey(id), CacheScope.All) as DocumentType;
        }

        private void RemoveDocumentTypeListFromCache()
        {
            RemoveDocumentTypeListFromCache("ALL-TYPES");
        }
        private void RemoveDocumentTypeListFromCache(string key)
        {
            _cache.Remove(GetDocumentTypeCacheKey(key), CacheScope.All);
        }
        private void RemoveDocumentTypeFromCache(int id)
        {
            _cache.Remove(GetDocumentTypeCacheKey(id), CacheScope.All);
        }

        #endregion Cache
    }
}
