﻿(function ($) {

    if (typeof $.omnicell === 'undefined') $.omnicell = {};
    if (typeof $.omnicell.propertyControls === 'undefined') $.omnicell.propertyControls = {};

    var prodGroupSelSetCheckEnabled = function(context) {
        var $this = $(context.filterTypeListId),
            val = parseInt($this.val()),
            prodList = $(context.productListId);


        if (val == 0) {
            $(context.productListCheckBoxes).prop('disabled', 'true');
            $(context.productListId).css('background-color', '#f0f0f0');
        } else {
            $(context.productListCheckBoxes).removeProp('disabled');
            $(context.productListId).css('background-color', '');
        }
    }

    $.omnicell.propertyControls.productGroupSelection = {
        register: function (context) {
            $(context.allProductsId).change(function (e) {
                var isChecked = $(this).is(":checked");
                $(context.productListCheckBoxes).prop('checked', isChecked);
            });
            $(context.filterTypeListId).change(function (e) {
                prodGroupSelSetCheckEnabled(context);
            });

            // set the initial state of the check boxes
            prodGroupSelSetCheckEnabled(context);
        }
    }

})(jQuery);