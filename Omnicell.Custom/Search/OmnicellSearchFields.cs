﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Omnicell.Custom.Search
{
    public static class OmnicellSearchFields
    {

        public const string CourseBeginDate = "CourseBeginDate_dt";
        public const string CourseEndDate = "CourseEndDate_dt";

        public const string CourseInfo = "CourseInfo_t";

        public const string MediaFormatId = "MediaFormatId_i";
        public const string MediaFormat = "MediaFormat_s";

        public const string ProductIdFilter = "ProductIdFilter_smv";
        public const string ProductValueFilter = "ProductValueFilter_wsmv";

        public const string CourseTypeId = "CourseTypeId_i";
        public const string CourseType = "CourseType_s";

        public const string DocumentTypeId = "DocumentTypeId_i";
        public const string DocumentType = "DocumentType_s";

        public const string AudienceTypeId = "AudienceTypeId_smv";
        public const string AudienceType = "AudienceType_wsmv";

        public const string PublishDate = "PublishDate_dt";

        // SMR - 2013-08-13
        public const string Versions = "Versions_wsmv";
        public const string FeatureList = "FeatureList_wsmv";

        public const string InterfacesId = "Interfaces_smv";
        public const string Interfaces = "Interfaces_wsmv";

        public const string HealthReportStatus = "HealthReportStatus_s";
        public const string HealthReportStatusCode = "HealthReportStatusCode_s";
        public const string HealthReportCompletionStatus = "HealthReportCompletionStatus_s";
        public const string HealthReportCompletionStatusCode = "HealthReportCompletionStatusCode_s";
        public const string ProductId = "ProductId_i";
        public const string ProductName = "Product_s";
        public const string IRGroup = "IRGroup_s";

        public const string FactSheetTOM = "FactSheetTOM_i";
        public const string FactSheetDOD = "FactSheetDOD_i";
        public const string FactSheetCSN = "FactSheetCsn_s";
        public const string FactSheetCity = "FactSheetCity_s";
        public const string FactSheetState = "FactSheetState_s";
        public const string FactSheetCountry = "FactSheetCountry_s";

        public const string Action = "Action_s";

        public const string Sku = "Sku_wsmv";

        public const string BrightCoveID = "BrightCoveID_wsmv";
    }
}
