﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Telligent.Caching;
using Telligent.Common;
using Telligent.Evolution.Components;
using Telligent.Evolution.Extensibility.Api.Entities.Version1;
using Telligent.Evolution.Extensibility.Api.Version1;

using Omnicell.Fetch.Data;
using Omnicell.Fetch.Components;
using Omnicell.Custom.Search;

namespace Omnicell.Fetch.Components
{
    public interface IFactSheetService
    {
        FactSheet Get(int id);
        FactSheet GetByGroup(int groupId);
        FactSheet GetByCSN(string csn);
        List<FactSheet> GetList();
        FactSheet Save(FactSheet factSheet);
        void Delete(int id);
        void DeleteByGroup(int groupId);
    }
    public class FactSheetService: IFactSheetService
    {
        private readonly ICacheService _cache = null;

        public FactSheetService()
        {
            _cache = Services.Get<ICacheService>();
        }

        public FactSheet Get(int id)
        {
            FactSheet factSheet = GetFactSheetFromCache(id);
            if (factSheet == null)
            {
                factSheet = FactSheetDataProvider.Instance.Get(id);
                if (factSheet != null)
                    PushFactSheetToCache(factSheet);
            }
            return factSheet;
        }
        public FactSheet GetByGroup(int groupId)
        {
            FactSheet factSheet = GetFactSheetFromCacheByGroup(groupId);
            if (factSheet == null)
            {
                factSheet = FactSheetDataProvider.Instance.GetByGroup(groupId);
                if (factSheet != null)
                    PushFactSheetToCache(factSheet);
            }
            return factSheet;
        }

        public FactSheet GetByCSN(string csn) 
        {
            FactSheet factSheet = GetFactSheetFromCacheByCSN(csn);
            if (factSheet == null)
            {
                factSheet = FactSheetDataProvider.Instance.GetByCSN(csn);
                if (factSheet != null)
                {
                    PushFactSheetToCache(factSheet);
                }
            }

            return factSheet;
        }

        public List<FactSheet> GetList()
        {
            List<FactSheet> factSheets = FactSheetDataProvider.Instance.List();

            return factSheets;
        }

        public FactSheet Save(FactSheet factSheet)
        {
            int id = factSheet.FactSheetId;
            if (id > 0)
            {
                RemoveFactSheetFromCache(factSheet);
                FactSheetDataProvider.Instance.Update(factSheet);
            }
            else
            {
                id = FactSheetDataProvider.Instance.Add(factSheet);
            }

            return Get(id);
        }
        public void Delete(int id)
        {
            FactSheet factSheet = Get(id);
            if (factSheet != null)
            {
                RemoveFactSheetFromCache(factSheet);
                FactSheetDataProvider.Instance.Delete(id);
            }
        }

        public void DeleteByGroup(int groupId)
        {
            FactSheet factSheet = GetByGroup(groupId);

            if (factSheet != null)
            {
                RemoveFactSheetFromCache(factSheet);
                FactSheetDataProvider.Instance.DeleteByGroup(groupId);
            }
        }

        #region Cache

        private string GetFactSheetCacheKey(int id)
        {
            return string.Format("FACTSHEET::ID::{0}", id);
        }
        private string GetFactSheetGroupCacheKey(int groupId)
        {
            return string.Format("FACTSHEET::GROUPID::{0}", groupId);
        }

        private string GetFactSheetCSNCacheKey(string csn) 
        {
            return string.Format("FACTSHEET::CSN::{0}", csn);
        }

        private void PushFactSheetToCache(FactSheet factSheet)
        {
            _cache.Put(GetFactSheetCacheKey(factSheet.FactSheetId), factSheet, CacheScope.All);
            _cache.Put(GetFactSheetGroupCacheKey(factSheet.GroupId), factSheet, CacheScope.All);
            _cache.Put(GetFactSheetCSNCacheKey(factSheet.FacilityCsn), factSheet, CacheScope.All);
        }

        private FactSheet GetFactSheetFromCache(int id)
        {
            return _cache.Get(GetFactSheetCacheKey(id), CacheScope.All) as FactSheet;
        }
        private FactSheet GetFactSheetFromCacheByGroup(int groupId)
        {
            return _cache.Get(GetFactSheetGroupCacheKey(groupId), CacheScope.All) as FactSheet;
        }

        private FactSheet GetFactSheetFromCacheByCSN(string csn) 
        {
            return _cache.Get(GetFactSheetCSNCacheKey(csn), CacheScope.All) as FactSheet;
        }

        private void RemoveFactSheetFromCache(FactSheet factSheet)
        {
            _cache.Remove(GetFactSheetGroupCacheKey(factSheet.GroupId), CacheScope.All);
            _cache.Remove(GetFactSheetCacheKey(factSheet.FactSheetId), CacheScope.All);
        }

        #endregion Cache
    }
}
