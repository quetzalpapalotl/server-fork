##
## Queries and renders a page of comments for an activity story
## Requires in scope:
##   $storyId or $story instance
##   $commentPageIndex (Reversed 0-based. 0 represents last page)
##

#set ($pageSize = 20)

#if (!$story)
	#set ($story = $core_v2_activityStory.Get($storyId))
#end

#set ($accessingUser = $core_v2_user.Accessing)
#set ($accessingUserIsRegistered = false)
#if ($core_v2_user.IsRegistered($accessingUser.Id))
	#set ($accessingUserIsRegistered = true)
#end

#set ($commentsSupported = false)
#set ($commentsSupported = $core_v2_comments.SupportsComments($story.ContentTypeId))

#set ($accessingUserCanComment = false)
#if ($commentsSupported)
	#set ($accessingUserCanComment = $core_v2_comments.CanCreateComment($story.ContentId, $story.ContentTypeId))
#end

#set ($reversedComments = false)
#set ($commentOptions = "%{ ContentId = $story.ContentId, ContentType = $story.ContentTypeId, IsApproved = 'true', SortBy = 'CreatedUtcDate', SortOrder = 'Descending', PageSize = $pageSize, PageIndex = $commentPageIndex }")
#if ($story.TypeId)
	$commentOptions.Add('CommentType', $story.TypeId)
#end

#set ($reversedComments = $core_v2_comments.List($commentOptions))

#if ($reversedComments)
	## reverse the last page into proper order, but PagedList doesn't support this natively, so first copy into an array
	#set ($comments = [])
	#foreach ($comment in $reversedComments)
		#set ($resp = $comments.Add($comment))
	#end
	#set ($resp = $comments.Reverse())

	## render comments
	#set ($collapse = false)
	#if (!$singleStory && $comments.Count > $collapseWhenOver)
		#set ($collapse = true)
	#end

	#set ($collapseCount = 0)

	#set ($hasMorePages = false)
	#set ($nextPageIndex = $commentPageIndex + 1)
	#if ($reversedComments.TotalCount > $comments.Count)
		#set ($totalPages = $reversedComments.TotalCount / $reversedComments.PageSize)
		#set ($totalPages = $totalPages - 1)
		#if ($totalPages >= $reversedComments.PageIndex)
			#set ($hasMorePages = true)
		#end
	#end
	#if ($hasMorePages)
		<li class="content-item action collapse" #if ($singleStory) style="display:block" #end>
			<a href="#" data-pageindex="$nextPageIndex" data-storyid="$story.StoryId">$core_v2_language.GetResource('view_more_comments')</a>
		</li>
	#end

	#if ($collapse)
		<li class="content-item action collapsed-comments">
			<a href="#">$core_v2_language.GetResource('view_more_comments')</a>
		</li>
	#end

	#foreach ($comment in $comments)
		#set ($accessingUserCanDelete = $core_v2_comments.CanDeleteComment($comment.CommentId))

		#set ($supportsLikes = $core_v2_like.SupportsLikes($comment.CommentContentTypeId))
		#set ($commentHasLikes = false)
		#if ($core_v2_like.SupportsLikes($comment.CommentContentTypeId))
			#set ($commentLikes = false)
			#set ($commentLikes = $core_v2_like.List("%{ ContentId = $comment.CommentId, ContentTypeId = $comment.CommentContentTypeId, PageSize = 1 }"))
			#if ($commentLikes && $commentLikes.TotalCount > 0)
				#set ($commentHasLikes = true)
			#end
		#end

		<li class="content-item comment #if ($collapse && $collapseCount < ($comments.Count - $showWhenCollapsed)) collapse #end  #if ($commentHasLikes) with-likes #end" data-commentid="$!comment.CommentId" data-contenttypeid="$!comment.CommentContentTypeId" data-commentdate="$!core_v2_language.FormatDate($comment.CreatedDate, $dateTimeRoundTripFormat)">
			<div class="full-post-header comment"></div>
			<div class="full-post comment">

				<div class="post-author">
					<span class="avatar">
						#if($comment.User.ProfileUrl)
							<a href="$core_v2_encoding.HtmlAttributeEncode($comment.User.ProfileUrl)">
								$core_v2_ui.GetResizedImageHtml($comment.User.AvatarUrl, 32, 32, "%{border='0',ResizeMethod = 'ZoomAndCrop', alt=$comment.User.DisplayName}")
							</a>
						#else
							$core_v2_ui.GetResizedImageHtml($comment.User.AvatarUrl, 32, 32, "%{border='0',ResizeMethod = 'ZoomAndCrop', alt=$comment.User.DisplayName}")
						#end
					</span>
					<span class="user-name">
						#if($comment.User.ProfileUrl)
							<a href="$core_v2_encoding.HtmlAttributeEncode($comment.User.ProfileUrl)" class="internal-link view-user-profile"><span></span>$comment.User.DisplayName</a>
						#else
							<span>$comment.User.DisplayName</span>
						#end
					</span>
				</div>

				#if ($accessingUserIsRegistered)
					#set ($moderateOptions = "%{}")
					#if($accessingUserCanDelete)
						#set ($DQ = '"')
						$moderateOptions.Add('AdditionalLinks',"[{${DQ}href${DQ}:${DQ}#${DQ},${DQ}text${DQ}:${DQ}${core_v2_language.GetResource('delete')}${DQ},${DQ}className${DQ}:${DQ}delete${DQ}}]")
					#end
					<div class="post-moderate">
						$core_v2_ui.Moderate($comment.CommentId, $comment.CommentContentTypeId, $moderateOptions)
					</div>
				#end

				<div class="post-content user-defined-markup">
					$comment.Body()
				</div>

				<div class="post-actions">
					<div class="navigation-list-header"></div>
					<ul class="navigation-list">
						<li class="navigation-item">
							$core_v2_language.FormatAgoDate($comment.CreatedDate)
						</li>
						#if ($core_v2_like.CanLike($comment.CommentId, $comment.CommentContentTypeId))
							<li class="navigation-item">
								$core_v2_ui.Like($comment.CommentId, $comment.CommentContentTypeId, "%{ Format = '{toggle}' }")
							</li>
						#end
						#if ($core_v2_like.SupportsLikes($comment.CommentContentTypeId))
							<li class="navigation-item comment-like-count">
								#set ($DQ = '"')
								$core_v2_ui.Like($comment.CommentId, $comment.CommentContentTypeId, "%{ Format = '<span class=${DQ}like-indicator inline${DQ}></span>{count}' }")
							</li>
						#end
					</ul>
					<div class="navigation-list-footer"></div>
				</div>
			</div>
			<div class="full-post-footer comment"></div>
		</li>

		#set ($collapseCount = $collapseCount + 1)
	#end

	## only show comment form as part of the first (initial) page of comments
	#if ($accessingUserCanComment && $commentPageIndex == 0)
		<li class="content-item comment comment-form" #if($reversedComments.TotalCount == 0)style="display:none;"#end>
			<div class="field-list-header"></div>
			<fieldset class="field-list">
				<ul class="field-list">
					<li class="field-item">
						<span class="field-item-input">
							<span class="avatar">
								$core_v2_ui.GetResizedImageHtml($accessingUser.AvatarUrl, 32, 32, "%{alt=$accessingUser.DisplayName,style='border-width:0;', ResizeMethod = 'ZoomAndCrop'}")
							</span>
							<textarea rows="1" maxlength="$core_v2_configuration.ActivityMessageReplyMaxLength" placeholder="$core_v2_encoding.HtmlAttributeEncode($core_v2_language.GetResource('writeComment'))"></textarea>
						</span>
					</li>
				</ul>
			</fieldset>
			<div class="field-list-footer"></div>
		</li>
	#end
#end
