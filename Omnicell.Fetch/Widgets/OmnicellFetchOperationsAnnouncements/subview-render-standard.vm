#set($showComments = $core_v2_widget.GetBoolValue("showComments", true))
#set($excerptSize = $core_v2_widget.GetIntValue('excerptSize', 250))

#foreach($post in $posts)
#each
    #set ($blog = $core_v2_blog.Get("%{ Id = $post.BlogId }"))
    #if ($blog && $blog.Id > 0)
    <li class="content-item">
        <div class="abbreviated-post-header"></div>
        <div class="abbreviated-post">

            #if($blog && !$oneBlog)
                <div class="post-application">
                    <span class="label"></span>
                    <span class="value">
                            <a class="internal-link view-application" href="$core_v2_encoding.HtmlAttributeEncode($blog.Url)">
                            <span></span>$blog.Name
                        </a>
                    </span>
                </div>
            #end

            <h4 class="post-name">
                <a href="$core_v2_encoding.HtmlAttributeEncode($post.Url)" class="internal-link view-post">
                    <span></span>$post.Title
                </a>
            </h4>
            <div class="post-date">
                <span class="label">$core_v2_language.GetResource('Weblog_EntryList_Posted')</span>
                <span class="value">$core_v2_language.FormatAgoDate($post.PublishedDate)</span>
            </div>

            <div class="post-author">
                <span class="evolution2-post-author-by fiji-post-author-by">$core_v2_language.GetResource('by')</span>
                #if($userFilterType != 'all')
                        <span class="avatar"><a href="$core_v2_encoding.HtmlAttributeEncode($post.Url)">$core_v2_ui.GetResizedImageHtml($post.Author.AvatarUrl, 32, 32, "%{border='0', alt=$post.Author.DisplayName}")</a></span>
                #else
                        <span class="avatar">
                            #if ($post.Author.ProfileUrl)
                                <a href="$core_v2_encoding.HtmlAttributeEncode($post.Author.ProfileUrl)">
                                    $core_v2_ui.GetResizedImageHtml($post.Author.AvatarUrl, 32, 32, "%{border='0', alt=$post.Author.DisplayName}")
                                </a>
                            #else
                                $core_v2_ui.GetResizedImageHtml($post.Author.AvatarUrl, 32, 32, "%{border='0', alt=$post.Author.DisplayName}")
                            #end
                        </span>
                #end
                <span class="user-name">
                     #if ($post.Author.ProfileUrl)
                         <a class="internal-link view-user-profile" href="$core_v2_encoding.HtmlAttributeEncode($post.Author.ProfileUrl)">
                             <span></span>$post.Author.DisplayName
                         </a>
                     #else
                         <span></span>$post.Author.DisplayName
                     #end
                </span>
            </div>

            <div class="post-rating">
                #set ($rating = false)
                #set ($rating = $core_v2_blogPost.GetRating($post.Id).Average)
                #if ($rating && $core_v2_blogPost.AreRatingsEnabled($post.Id))
                    $core_v2_ui.Rate($post.ContentId, $core_v2_blogPost.ContentTypeId, "%{ReadOnly='true',CssClass='small'}")
                #end
            </div>

            #set ($showCommentCount = false)
            #if($showComments && !$post.IsExternal && ($post.CommentCount > 0 || ($blog.EnableCommentsOverride && !$post.IsLocked)))
                #set ($showCommentCount = true)
            #end
            <div class="post-attributes">
                <div class="attribute-list-header"></div>
                <ul class="attribute-list">
                    #set ($likes = false)
                    #set ($likes = $core_v2_like.List("%{ ContentId = $post.ContentId, ContentTypeId = $core_v2_blogPost.ContentTypeId, PageSize = 1 }"))
                    #if (($showCommentCount && $post.CommentCount > 0) || ($likes && $likes.TotalCount > 0))
                        <li class="attribute-item counts">
                            <a href="$core_v2_encoding.HtmlAttributeEncode($post.Url)#comments">
                                #if ($showCommentCount && $post.CommentCount > 0)
                                    <span class="attribute-value reply-count">
                                        <span class="avatar"></span>$post.CommentCount
                                    </span>
                                #end
                                #if ($likes && $likes.TotalCount > 0)
                                    <span class="attribute-value like-count">
                                        <span class="avatar"></span>$core_v2_ui.Like($post.ContentId, $core_v2_blogPost.ContentTypeId, "%{ Format = '{count}' }")
                                    </span>
                                #end
                            </a>
                        </li>
                    #end
                </ul>
                <div class="attribute-list-footer"></div>
            </div>

            #if ($standardView == 'list')
                $core_v2_page.SetCookieValue('BlogPostList-ViewType', 'List')
                <div class="post-summary">
                    #if($post.Excerpt && $post.Excerpt.Length > 0)
                        $core_v2_language.Truncate($post.Excerpt, $excerptSize, '...')
                    #else
                        $core_v2_language.Truncate($post.Body('WebQuote'), $blog.PostSummaryLengthDefault, '...')
                    #end
                </div>
            #elseif ($standardView == 'detail')
                $core_v2_page.SetCookieValue('BlogPostList-ViewType', 'Detail')
                $core_v2_blogPost.IncrementViewCount($post.Id)
                <div class="post-content user-defined-markup">$post.Body()</div>
            #end

        </div>
        <div class="abbreviated-post-footer"></div>
    </li>
    #end
#nodata
    <div class="message norecords">$core_v2_language.GetResource('Weblog_NoPosts_Created')</div>
#end
