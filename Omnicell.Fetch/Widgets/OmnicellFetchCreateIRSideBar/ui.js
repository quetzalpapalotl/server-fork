(function ($) {
    var deleteAddresses = function(context, addressIds) {
            $.telligent.evolution.post({
                    url: context.deleteUrl,
                    data: {
                            selectedAddresses: addressIds.join()
                    },
                    success: function(response) {
		          //window.location.href = window.location.href;
		          window.location.reload();
                    }
             });

        },
        addAddress = function(context, address, type) {
            $.telligent.evolution.post({
                    url: context.addUrl,
                    data: {
                            address: address,
                            addressType: type
                    },
                    success: function(response) {
		          //window.location.href = window.location.href;
		          window.location.reload();
                    }
            });
        },
        saveData = function(context) {
            $.telligent.evolution.post({
                    url: context.saveUrl,
                    data: context.wrapperId.closest('form').serialize(),
                    success: function(response) {
		          //window.location.href = window.location.href;
		          window.location.reload();
                    }
            });
        },
        queryData = function(context) {
            var url = context.queryUrl;
            if (url.indexOf('?') > -1)
            {
                        url = url + '&CSN=';
            }
            else
            {
                        url = url + '?CSN=';
            }
            window.location.href = url + $(context.csn).val();
        };
    var api = {
//    $.widgets.serviceRequestsWidget = {
        register: function (context) {
		context.wrapperId = $(context.wrapperId);
		$(context.saveButton, context.wrapperId).bind('click', function(e,data){
			var email = $(context.email, context.wrapperId).val();
			
                                   if(!email || email.length == 0) {
                                           alert('You must enter an email address before you can save ticket update notifications.');
                                   }
                                   else {
				saveData(context);
			}
			return false;
		});
		$(context.queryButton, context.wrapperId).bind('click', function(e,data){
			var csn = $(context.csn, context.wrapperId).val();
			
                                   if(!csn || csn.length == 0) {
                                           alert('You must enter one or more csns in order to submit a query.');
                                   }
                                   else {
				queryData(context);
			}
			return false;
		});

        }
    };
    
    if (typeof $.widgets == 'undefined') { $.widgets = {}; }
    $.widgets.serviceRequestsWidget = api;

}(jQuery));