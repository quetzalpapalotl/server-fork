(function(j, global){

	if (typeof j.telligent === 'undefined')
		j.telligent = {};

	if (typeof j.telligent.evolution === 'undefined')
		j.telligent.evolution = {};

	if (typeof j.telligent.evolution.widgets === 'undefined')
		j.telligent.evolution.widgets = {};

	j.telligent.evolution.widgets.groupLinks = {
		register: function(options) {
			var joinOptions = {
				url: options.joinUrl,
				data: {GroupId:options.groupId, UserId:options.userId},
				success: function() { global.location.reload(); }
			};

			// activate group joining buttons
			options.joinButton.click(function(){
				joinOptions.data.GroupMembershipType = 'Member';
				j.telligent.evolution.post(joinOptions);
				return false;
			});
			options.requestJoinButton.click(function(e){
				e.preventDefault();
				if (options.canJoinGroup) {
					joinOptions.data.GroupMembershipType = 'PendingMember';
					j.telligent.evolution.post(joinOptions);
				} else {
					Telligent_Modal.Open(options.requestJoinUrl, 550, 300, function() { global.location.reload(); });
				}
				return false;
			});

			// activate membership cancellation links
			options.cancelLink.click(function(){
				if(confirm(options.cancelConfirmMessage)) {
					j.telligent.evolution.del({
						url: options.cancelUrl,
						data: {GroupId:options.groupId, UserId:options.userId},
						success: function(){ global.location.reload(); }
					});
				}
				return false;
			});
		}
	};

})(jQuery, window);
