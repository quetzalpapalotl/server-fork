(function($){

    if (typeof $.omnicell === 'undefined') { $.omnicell = {}; }
    if (typeof $.omnicell.fetch === 'undefined') { $.omnicell.fetch = {}; }
    if (typeof $.omnicell.fetch.widgets === 'undefined') { $.omnicell.fetch.widgets = {}; }    

        attachHandlers = function(context) {
           
            
        },
        attachValidation = function (context) {
            context.updateButton
                .evolutionValidation({
                	onValidated: function(isValid, buttonClicked, c) {
                		if (isValid) {
                			context.updateButton.removeClass('disabled');
                		} else {
                			context.updateButton.addClass('disabled');
                		}
                	},
                	onSuccessfulClick: function(e) {
                		e.preventDefault();
                		context.updateButton.parent().addClass('processing');
                		context.updateButton.addClass('disabled');
                		
                		if(context.originalCSN.val() === context.facilityCsnInput.val()) {
                		    updateRoom(context);
                		}
                		else {
                		    var data = { w_facilityCsn: context.facilityCsnInput.val() };
                		// check if a group already exists for the csn number
                		$.telligent.evolution.post({
                		    url: context.checkCsnUrl,
                		    data: data ,
                		     success: function(response) {
                                if(response.result === "True") {
                                    
                                    context.updateButton.removeClass('disabled');
                                    $(context.csnExistsSpan).show();
                                }
                                else {
                                    updateRoom(context);
                                }
                            },
                            defaultErrorMessage : "Error",
                            error : function(xhr, desc, ex) {
                            $.telligent.evolution.notifications.show(desc+ex, {
                            type : 'error'
                            });
                            }
                		 });
                		}
                	}
                }).evolutionValidation('addField', context.roomNameInput, {
                        required: true,
                        groupnameexists: {
                            getParentId: function() {
                                return context.fetchParentId;
                            }
                        },
                        messages: {
                            required: context.requiredNameValidationMessage,
                            groupnameexists: context.uniqueNameValidationMessage
                        }
                    }, context.roomNameInput.closest('.field-item').find('.field-item-validation'), null
                ).evolutionValidation('addField', context.roomTypeInput, {
                        required: true,
						messages: { required: context.roomTypeRequired }
                    }, context.roomTypeInput.closest('.field-item').find('.field-item-validation'), null
                ).evolutionValidation('addField', context.facilityCsnInput, {
                        required: true,
						messages: { required: context.facilityCSNRequired }
                    }, context.facilityCsnInput.closest('.field-item').find('.field-item-validation'), null
             
                );
        },
        updateRoom = function(context) {
            var roomData = {
                w_roomName: context.roomNameInput.val(),
                w_roomType: context.roomTypeInput.val(),
                w_facilityCsn: context.facilityCsnInput.val()
            };
                
            $.telligent.evolution.post({
                url: context.updateRoomUrl,
                data: roomData,
                success: function(response) {
                    window.location = response.redirectUrl;
                },
                error: function(a, b, c){
                    var stopper = true;
                }
                
            });
            
        };
    
    $.omnicell.fetch.widgets.updateImplementationRoom = {
    	register: function(context) {
            
            context.roomNameInput = $(context.roomNameInput);
            context.roomTypeInput = $(context.roomTypeInput);
            context.facilityCsnInput = $(context.facilityCsnInput);
            context.csnExistsSpan = $(context.csnExistsSpan);
            context.updateButton = $(context.updateButton);
            context.originalCSN = $(context.originalCSN);

            attachHandlers(context);
            attachValidation(context);
            
    	}
	};
    
}(jQuery));
