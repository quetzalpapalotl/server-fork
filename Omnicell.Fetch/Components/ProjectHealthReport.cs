﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Telligent.Evolution.Extensibility.Api.Entities.Version1;
using Telligent.Evolution.Extensibility.Api.Version1;
using Telligent.Evolution.Extensibility.Content.Version1;
using TEPublicApi = Telligent.Evolution.Extensibility.Api.Version1.PublicApi;


namespace Omnicell.Fetch.Components
{
    public class ProjectHealthReport : ApiEntity, IContent
    {
        private Group _implementationRoomGroup;
        private ImplementationRoom _implementationRoom;
        private Group _productGroup;
        private User _createdByUser;

        public int ProjectHealthReportId { get; set; }
        public Guid ContentId { get; set; }
        public int ImplementationRoomGroupId { get; set; }
        public int ProductId { get; set; }
        public DateTime HealthReportDate { get; set; }
        public string HealthReportStatus { get; set; }
        public string HealthReportStatusCode { get; set; }
        public string HealthReportCompletionStatus { get; set; }
        public string HealthReportCompletionStatusCode { get; set; }
        public int CreatedByUserId { get; set; }

        public Guid ContentTypeId
        {
            get { return ContentTypes.Constants.ProjectHealthReportContentTypeId; }
        }


        public Group ImplementationRoomGroup
        {
            get
            {
                if (_implementationRoomGroup == null)
                    _implementationRoomGroup = TEPublicApi.Groups.Get(new GroupsGetOptions() { Id = ImplementationRoomGroupId });

                return _implementationRoomGroup;
            }
        }
        public ImplementationRoom ImplementationRoom
        {
            get
            {
                if (_implementationRoom == null)
                    _implementationRoom = new ImplementationRoom(ImplementationRoomGroupId);

                return _implementationRoom;
            }
        }

        public User CreatedByUser
        {
            get
            {
                if (_createdByUser == null)
                    _createdByUser = TEPublicApi.Users.Get(new UsersGetOptions() { Id = CreatedByUserId });

                return _createdByUser;
            }
        }

        public Group ProductGroup
        {
            get
            {
                if (_productGroup == null && ProductId != ImplementationRoomGroupId)
                    _productGroup = TEPublicApi.Groups.Get(new GroupsGetOptions() { Id = ProductId });

                return _productGroup;
            }
        }

        public string Name
        {
            get
            {
                return string.Format("{0} Health Report for {1}", HealthReportDate.ToShortDateString(), ImplementationRoomGroup.Name);
            }
            set {}
        }

        public string Description
        {
            get
            {
                return string.Format("{0} Health Report for {1}: {2}", HealthReportDate.ToShortDateString(), ImplementationRoomGroup.Name, HealthReportStatus);
            }
            set {}
        }

        public ProjectHealthReport():base(){}

        public ProjectHealthReport(AdditionalInfo additionalInfo)
			: base(additionalInfo)
		{
		}

        public ProjectHealthReport(IList<Warning> warnings, IList<Error> errors)
			: base(warnings, errors)
		{
		}

        #region IContent Members

        IApplication IContent.Application
        {
            get { return ImplementationRoomGroup; }
        }

        string IContent.AvatarUrl
        {
            get { return null; }
        }

        Guid IContent.ContentId
        {
            get { return ContentId; }
        }

        Guid IContent.ContentTypeId
        {
            get { return ContentTypeId; }
        }

        int? IContent.CreatedByUserId
        {
            get { return CreatedByUserId; }
        }

        DateTime IContent.CreatedDate
        {
            get { return HealthReportDate; }
        }

        string IContent.HtmlDescription(string target)
        {
            return Description;
        }

        string IContent.HtmlName(string target)
        {
            return Name;
        }

        bool IContent.IsEnabled
        {
            get { return true; }
        }

        string IContent.Url
        {
            get { return ImplementationRoomGroup.Url; }
        }

        #endregion IContent Members

        public IList<SearchIndexDocument> GetContentToIndex()
        {
            throw new NotImplementedException();
        }

        public string GetViewHtml(IContent content, Target target)
        {
            throw new NotImplementedException();
        }

        public int[] GetViewSecurityRoles(Guid contentId)
        {
            throw new NotImplementedException();
        }

        public bool IsCacheable
        {
            get { throw new NotImplementedException(); }
        }

        public void SetIndexStatus(Guid[] contentIds, bool isIndexed)
        {
            throw new NotImplementedException();
        }

        public bool VaryCacheByUser
        {
            get { throw new NotImplementedException(); }
        }

        public Guid[] ApplicationTypes
        {
            get { throw new NotImplementedException(); }
        }

        public void AttachChangeEvents(IContentStateChanges stateChanges)
        {
            throw new NotImplementedException();
        }

        public string ContentTypeName
        {
            get { throw new NotImplementedException(); }
        }

        public IContent Get(Guid contentId)
        {
            throw new NotImplementedException();
        }


        public void Initialize()
        {
            throw new NotImplementedException();
        }
    }
}

