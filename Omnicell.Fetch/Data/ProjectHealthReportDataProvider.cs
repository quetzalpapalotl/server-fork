﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using Telligent.Evolution.Extensibility.Api.Entities.Version1;

using Omnicell.Fetch.Components;
using Omnicell.Custom.Data;

namespace Omnicell.Fetch.Data
{
    public class ProjectHealthReportDataProvider : SqlDataProvider<ProjectHealthReportDataProvider, ProjectHealthReport>
    {

        public ProjectHealthReport Get(int id)
        {
            string procName = ProcNameWithOwner("omnicell_ProjectHealthReport_GetById");
            List<SqlParameter> parameters = new List<SqlParameter>()
            {
                new SqlParameter{ParameterName = "@ProjectHealthReportId", SqlDbType = SqlDbType.Int, Value = id}
            };
            return Get<ProjectHealthReport>(procName, parameters);
        }

        public ProjectHealthReport Get(Guid id)
        {
            string procName = ProcNameWithOwner("omnicell_ProjectHealthReport_GetByContentId");
            List<SqlParameter> parameters = new List<SqlParameter>()
            {
                new SqlParameter{ParameterName = "@ContentId", SqlDbType = SqlDbType.UniqueIdentifier, Value = id}
            };
            return Get<ProjectHealthReport>(procName, parameters);
        }

        public List<ProjectHealthReport> GetList(int groupId)
        {
            string procName = ProcNameWithOwner("omnicell_ProjectHealthReport_GetByGroupId");
            List<SqlParameter> parameters = new List<SqlParameter>()
            {
                new SqlParameter{ParameterName = "@ImplementationRoomGroupId", SqlDbType = SqlDbType.Int, Value = groupId}
            };
            return base.GetList<ProjectHealthReport>(procName, parameters);
        }

        public int Add(ProjectHealthReport healthReport)
        {
            string procName = ProcNameWithOwner("omnicell_ProjectHealthReport_Add");
            List<SqlParameter> parameters = new List<SqlParameter>()
            {
                new SqlParameter{ParameterName = "@ContentId", SqlDbType = SqlDbType.UniqueIdentifier, Value = healthReport.ContentId},
                new SqlParameter{ParameterName = "@ImplementationRoomGroupId", SqlDbType = SqlDbType.Int, Value = healthReport.ImplementationRoomGroupId},
                new SqlParameter{ParameterName = "@ProductId", SqlDbType = SqlDbType.Int, Value = healthReport.ProductId},
                new SqlParameter{ParameterName = "@HealthReportDate", SqlDbType = SqlDbType.DateTime, Value = healthReport.HealthReportDate},
                new SqlParameter{ParameterName = "@HealthReportStatus", SqlDbType = SqlDbType.NVarChar, Size = 128, Value = healthReport.HealthReportStatus},
                new SqlParameter{ParameterName = "@HealthReportStatusCode", SqlDbType = SqlDbType.NVarChar, Size = 50, Value = healthReport.HealthReportStatusCode},
                new SqlParameter{ParameterName = "@HealthReportCompletionStatus", SqlDbType = SqlDbType.NVarChar, Size = 256, Value = healthReport.HealthReportCompletionStatus},
                new SqlParameter{ParameterName = "@HealthReportCompletionStatusCode", SqlDbType = SqlDbType.NVarChar, Size = 50, Value = healthReport.HealthReportCompletionStatusCode},
                new SqlParameter{ParameterName = "@CreatedByUserId", SqlDbType = SqlDbType.Int, Value = healthReport.CreatedByUserId}
            };
            return Add(procName, parameters);
        }

        public int Update(ProjectHealthReport healthReport)
        {
            string procName = ProcNameWithOwner("omnicell_ProjectHealthReport_Update");
            List<SqlParameter> parameters = new List<SqlParameter>()
            {
                new SqlParameter{ParameterName = "@ProjectHealthReportId", SqlDbType = SqlDbType.UniqueIdentifier, Value = healthReport.ProjectHealthReportId},
                new SqlParameter{ParameterName = "@ImplementationRoomGroupId", SqlDbType = SqlDbType.Int, Value = healthReport.ImplementationRoomGroupId},
                new SqlParameter{ParameterName = "@ProductId", SqlDbType = SqlDbType.Int, Value = healthReport.ProductId},
                new SqlParameter{ParameterName = "@HealthReportDate", SqlDbType = SqlDbType.DateTime, Value = healthReport.HealthReportDate},
                new SqlParameter{ParameterName = "@HealthReportStatus", SqlDbType = SqlDbType.NVarChar, Size = 128, Value = healthReport.HealthReportStatus},
                new SqlParameter{ParameterName = "@HealthReportStatusCode", SqlDbType = SqlDbType.NVarChar, Size = 50, Value = healthReport.HealthReportStatusCode},
                new SqlParameter{ParameterName = "@HealthReportCompletionStatus", SqlDbType = SqlDbType.NVarChar, Size = 256, Value = healthReport.HealthReportCompletionStatus},
                new SqlParameter{ParameterName = "@HealthReportCompletionStatusCode", SqlDbType = SqlDbType.NVarChar, Size = 50, Value = healthReport.HealthReportCompletionStatusCode},
                new SqlParameter{ParameterName = "@CreatedByUserId", SqlDbType = SqlDbType.Int, Value = healthReport.CreatedByUserId}
            };
            return Edit(procName, parameters);
        }


        public void Delete(int id)
        {
            string procName = ProcNameWithOwner("omnicell_ProjectHealthReport_Delete");
            List<SqlParameter> parameters = new List<SqlParameter>()
            {
                new SqlParameter{ParameterName = "@ProjectHealthReportId", SqlDbType = SqlDbType.Int, Value = id}
            };
            Delete(procName, parameters);
        }

        public PagedList<ProjectHealthReport> GetListToReIndex(int pageSize, int pageIndex)
        {
            string procName = ProcNameWithOwner("omnicell_ProjectHealthReport_GetListToReIndex");
            List<SqlParameter> parameters = new List<SqlParameter>()
            {
                new SqlParameter{ParameterName = "@PageStart", SqlDbType = SqlDbType.Int, Value = pageSize * pageIndex},
                new SqlParameter{ParameterName = "@PageEnd", SqlDbType = SqlDbType.Int, Value = pageSize * (pageIndex + 1)}
            };

            List<ProjectHealthReport> healthReports = GetList(procName, parameters);
            return new PagedList<ProjectHealthReport>(healthReports, pageSize, pageIndex, healthReports.Count);
        }

        public void SetIndexStatus(IEnumerable<Guid> healthReportIds, bool isIndexed = true)
        {
            string procName = ProcNameWithOwner("omnicell_ProjectHealthReport_SetIndexStatus");
            List<SqlParameter> parameters = new List<SqlParameter>()
            {
                new SqlParameter{ParameterName = "@HealthReportIds", SqlDbType = SqlDbType.Xml, Value = "<ProjectHealthReports>" + string.Join("", healthReportIds.Select(x => "<ProjectHealthReport ContentId=\"" + x.ToString() + "\" />")) + "</ProjectHealthReports>"},
                new SqlParameter{ParameterName = "@isIndexed", SqlDbType = SqlDbType.Bit, Value = isIndexed}
            };
            ExecuteNonQuery(procName, parameters);
        }

        public void SetIndexStatus(int groupId, bool isIndexed = true)
        {
            string procName = ProcNameWithOwner("omnicell_ProjectHealthReport_SetIndexStatusByGroupID");
            List<SqlParameter> parameters = new List<SqlParameter>()
            {
                new SqlParameter{ParameterName = "@ImplementationRoomGroupId", SqlDbType = SqlDbType.Xml, Value = groupId},
                new SqlParameter{ParameterName = "@isIndexed", SqlDbType = SqlDbType.Bit, Value = isIndexed}
            };
            ExecuteNonQuery(procName, parameters);
        }
    }
}

