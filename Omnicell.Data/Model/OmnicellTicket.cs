﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Omnicell.Data.Model
{
    public class OmnicellTicket
    {
        public OmnicellTicket()
        {
        }

        public OmnicellTicket(Omnicell_Ticket ticket)
        {
            Ticket = ticket;
        }

        public OmnicellTicketSubscription TicketSubscription { get; set; }

        private Omnicell_Ticket ticket = null;
        private Omnicell_Ticket Ticket
        {
            get
            {
                if (ticket == null)
                {
                    ticket = new Omnicell_Ticket();
                }

                return ticket;
            }

            set
            {
                ticket = value;
            }
        }

        public string Abstract 
        {
            get { return Ticket.Abstract; }
            set { Ticket.Abstract = value;}
        }

        public string CSN
        {
            get { return Ticket.CSN; }
            set { Ticket.CSN = value; }
        }

        public int Id
        {
            get { return Ticket.Id; }
            set { Ticket.Id = value; }
        }

        public bool IsActive
        {
            get { return Ticket.IsActive; }
            set { Ticket.IsActive = value; }
        }

        public string LastActivityUpdate
        {
            get { return Ticket.LastActivityUpdate; }
            set { Ticket.LastActivityUpdate = value; }
        }

        public string Number
        {
            get { return Ticket.Number; }
            set { Ticket.Number = value; }
        }

        public string ResolutionCode
        {
            get { return Ticket.ResolutionCode; }
            set { Ticket.ResolutionCode = value; }
        }

        public string Status
        {
            get { return Ticket.Status; }
            set { Ticket.Status = value; }
        }

        public string SubStatus
        {
            get { return Ticket.SubStatus; }
            set { Ticket.SubStatus = value; }
        }

        public string SymptomCode
        {
            get { return Ticket.SymptomCode; }
            set { Ticket.SymptomCode = value; }
        }

        // SMR - 20130823 - Added Serial Number so it can be displayed in the widget
        public string SerialNumber
        {
            get { return Ticket.SerialNumber; }
            set
            {
                if (value != null)
                {
                    Ticket.SerialNumber = value;
                }
                else
                {
                    Ticket.SerialNumber = string.Empty;
                }
            }
        }

        public DateTime? Updated
        {
            get { return Ticket.Updated; }
            set { Ticket.Updated = value; }
        }

        public int? StatusId
        {
            get { return Ticket.StatusId; }
            set { Ticket.StatusId = value; }
        }

        public int? SubStatusId
        {
            get { return Ticket.SubStatusId; }
            set { Ticket.SubStatusId = value; }
        }

        public int? SymptomCodeId
        {
            get { return Ticket.SymptomCodeId; }
            set { Ticket.SymptomCodeId = value; }
        }

        public int? SymptomAreaId
        {
            get { return Ticket.SymptomAreaId; }
            set { Ticket.SymptomAreaId = value; }
}

        public int? ResolutionCodeId
        {
            get { return Ticket.ResolutionCodeId; }
            set { Ticket.ResolutionCodeId = value; }
        }

        public int? ResolutionAreaId
        {
            get { return Ticket.ResolutionAreaId; }
            set { Ticket.ResolutionAreaId = value; }
        }

        public DateTime? LastActivityUpdateDate
        {
            get { return Ticket.LastActivityUpdateDate; }
            set { Ticket.LastActivityUpdateDate = value; }
        }

        public LookupType StatusLookupType { get; set; }
        public LookupType SubStatusLookupType { get; set; }
        public LookupType SymptomCodeLookupType { get; set; }
        public LookupType SymptomAreaLookupType { get; set; }
        public LookupType ResolutionCodeLookupType { get; set; }
        public LookupType ResolutionAreaLookupType { get; set; }

        private IList<OmnicellTicketSubscription> ticketSubscriptions = null;
        public IList<OmnicellTicketSubscription> TicketSubscriptions 
        {
            get
            {
                if (ticketSubscriptions == null)
                {
                    ticketSubscriptions = new List<OmnicellTicketSubscription>();
                }

                return ticketSubscriptions;
            }

            set
            {
                ticketSubscriptions = value;
            }
        }

    }
}
