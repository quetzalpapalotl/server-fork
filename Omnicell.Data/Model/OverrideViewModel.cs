﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Omnicell.Data.Model
{
    public class OverrideViewModel
    {
        public string Email { get; set; }
        public int Id { get; set; }
        public string Type { get; set; }
        public DateTime Created { get; set; }
    }
}
