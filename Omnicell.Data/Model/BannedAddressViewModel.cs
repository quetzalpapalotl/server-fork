﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Omnicell.Data.Model
{
    public class BannedAddressViewModel
    {
        public int Id { get; set; }
        public string Address { get; set; }
        public string Type { get; set; }
        public DateTime CreateDate { get; set; }
    }
}
