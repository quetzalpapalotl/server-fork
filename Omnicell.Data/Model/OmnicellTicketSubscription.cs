﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Omnicell.Data.Model
{
    public class OmnicellTicketSubscription
    {
        public OmnicellTicketSubscription()
        {
        }

        public OmnicellTicketSubscription(Omnicell_TicketSubscription ticketSubscription)
        {
            TicketSubscription = ticketSubscription;
        }

        private Omnicell_TicketSubscription ticketSubscription = null;
        private Omnicell_TicketSubscription TicketSubscription
        {
            get
            {
                if (ticketSubscription == null)
                {
                    ticketSubscription = new Omnicell_TicketSubscription();
                    Ticket = new OmnicellTicket();
                }

                return ticketSubscription;
            }

            set
            {
                ticketSubscription = value;
            }
        }

        public OmnicellTicket Ticket { get; set; }

        public int Id
        {
            get
            {
                return TicketSubscription.Id;
            }

            set
            {
                TicketSubscription.Id = value;
            }
        }

        public int? TicketId
        {
            get
            {
                return TicketSubscription.TicketId;
            }

            set
            {
                TicketSubscription.TicketId = value;
            }
        }

        public int? UserId
        {
            get
            {
                return TicketSubscription.UserId;
            }

            set
            {
                TicketSubscription.UserId = value;
            }
        }

        public string CSN
        {
            get
            {
                return TicketSubscription.CSN;
            }
            set
            {
                TicketSubscription.CSN = value;
            }
        }

    }
}
