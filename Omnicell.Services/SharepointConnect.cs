﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SPClient = Telligent.Evolution.Extensions.SharePoint.Client;
using SPApi = Telligent.Evolution.Extensions.SharePoint.Client.Api.Version1;
using Telligent.Evolution.Extensibility.Api.Entities.Version1;
namespace Omnicell.Services
{
    public class SharepointConnect
    {
        // BEGIN Sharepoint Adds
        public string GetSPDateCreated()
        {
            // connect to Sharepoint and get a list of the libraries
            SPClient.version1.SharePointLibrary spLibrary = new SPClient.version1.SharePointLibrary();
            SPClient.version2.SharePointLibrary spLib2 = new SPClient.version2.SharePointLibrary();
            SPClient.version2.SharePointFile spFile = new SPClient.version2.SharePointFile();

            SPApi.Library splib = new SPApi.Library();
            try
            {
                // convert the library ID to GUID
                Guid LibIDGuid = new Guid("2e59f92e-fb3a-41c8-8fb0-94d0ac358d6b");
                // SMR - This is a hard coded file GUID and it works
                // Guid DocIDGuid = new Guid("4502cf56-a6f4-4ec5-baf4-a1077674a408");

                splib = spLib2.Get(LibIDGuid);
                Guid AppGuidID = new Guid(splib.ApplicationId.ToString());
                // this library has a groupID, get all the files associated with the group ID
                int igroupID = splib.GroupId;
                PagedList<SPApi.Document> lstspfiles = new PagedList<SPApi.Document>();
                SPApi.SPListCollectionOptions optnssplist = new SPApi.SPListCollectionOptions();
                Dictionary<String, String> options = new Dictionary<String, String>();
                
                options["PageSize"] = "50";
                options["SortBy"] = "Name";
                SPApi.Document spdoc = new SPApi.Document();
                lstspfiles = spFile.List(AppGuidID);

                // spdoc = spFile.Get(DocIDGuid);

                //mh
                SPApi.DocumentListOptions testOps = new SPApi.DocumentListOptions();
//                testOps.SortBy = "Name";
                testOps.FolderPath = splib.Root;  // /Repository/Publications /?
                //testOps.FolderPath = "/Repository/Publications";  
                //testOps.Url = splib.Url;

                PagedList<SPApi.Document> testDocs = SPApi.PublicApi.Documents.List(LibIDGuid, testOps);


                // /mh

            }
            catch (Exception ex)
            {
                Console.WriteLine("Message: " + ex.Message);
            }
            return spLib2.Current.Created.ToString();
        }

        // END Sharepoint Adds

    }
}
