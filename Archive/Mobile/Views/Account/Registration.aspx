﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<TelligentEvolution.Mobile.Web.Models.RegistrationViewModel>" MasterPageFile="~/Views/Shared/Simple.Master" ValidateRequest="false" %>
<%@ Import Namespace="TelligentEvolution.Mobile.Web.Models" %>
<%@ Import Namespace="TelligentEvolution.Mobile.Web.Data.Entities" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	<%= Html.LocalizedString("Core_Registration_Title")%>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<script type="text/javascript">
$(function () {
    $('.field-list.registration .field-item.profile-field .date').datepicker();
    $('.field-list.registration .field-item.profile-field .date-time').datepicker({ dateFormat: 'mm/dd/yy 00:00' });
});
</script>
<% using (Html.BeginForm(new { area = "", controller = "Account", action = "Registration", ReturnUrl = Model.ReturnUrl })) { %>
    <div class="field-list-header registration"></div>
    <fieldset class="field-list registration"><legend><%= Html.LocalizedString("Core_Registration_Title")%></legend>
        <% if (ViewData.ModelState.ContainsKey("errorResourceName")){ %>
            <span class="field-validation-summary"><%= Html.LocalizedString(ViewData.ModelState["errorResourceName"].Errors[0].ErrorMessage)%></span>
        <% } %>
        <ul class="field-list">
            <li class="field-item email">
                <label for="Email" class="field-item-text"><%= Html.LocalizedString("Core_Registration_Email")%></label>
                <%= Html.ValidationMessageFor(m => m.Email) %>
                <span class="field-item-input"><%= Html.TextBoxFor(m => m.Email)%></span>
            </li>
            <li class="field-item user-name">
                <label for="UserName" class="field-item-text"><%= Html.LocalizedString("Core_Registration_UserName")%></label>
                <%= Html.ValidationMessageFor(m => m.UserName) %>
                <span class="field-item-input"><%= Html.TextBoxFor(m => m.UserName)%></span>
            </li>
            <li class="field-item password">
                <label for="Password" class="field-item-text"><%= Html.LocalizedString("Core_Registration_Password")%></label>
                <%= Html.ValidationMessageFor(m => m.Password) %>
                <span class="field-item-input"><%= Html.PasswordFor(m => m.Password)%></span>
            </li>
            <li class="field-item confirm-password">
                <label for="ConfirmPassword" class="field-item-text"><%= Html.LocalizedString("Core_Registration_ConfirmPassword")%></label>
                <%= Html.ValidationMessageFor(m => m.ConfirmPassword) %>
                <span class="field-item-input"><%= Html.PasswordFor(m => m.ConfirmPassword)%></span>
            </li>
            <% foreach (ProfileField field in Model.ProfileFields) { %>
            <li class="field-item profile-field <%= field.Field.Name.ToLower() %>">
                <label for="<%= field.Field.Name %>" class="field-item-text"><%= field.Field.DisplayName%></label>
                <span class="field-item-input">
                    <%= Html.ProfileField(field) %>
                </span>
            </li>
            <%} %>
            <li class="field-item submit">
                <span class="field-item-input">
                    <a href="#" onclick="$(this).parents('form').submit(); return false;" class="internal-link submit"><span></span><%= Html.LocalizedString("Core_Registration_Join")%></a>
                </span>
            </li>
        </ul>
    </fieldset>
    <div class="field-list-footer registration"></div>
<% } %>
</asp:Content>