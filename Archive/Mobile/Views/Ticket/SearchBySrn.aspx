﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<SearchTicketModel>" MasterPageFile="~/Views/Shared/Site.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="landing-page results">
	    <div class="splash">
        <div class="search-form" style="min-height:30px; padding:5px;">
        <% using (Html.BeginForm(new { controller = "Ticket", action = "SearchBySrn" }))
           { %>
            <%= Html.TextBoxFor(m => m.txtOmniSearch)%>
            <% Html.ValidateFor(m => m.txtOmniSearch); %>
            <%= Html.HiddenFor(m => m.isCsnSearch)%>
            <%--<%= Html.CheckBoxFor(m => m.activeOnly)%> Active Only--%>
			    <input id="btnSearch" type="submit" value="Search" />
        <%} %>
	    </div>
        <%
            if (Model != null && Model.Tickets != null)
            {
                Html.RenderPartial("EditTicketList", Model);
            }
        %>
    </div>
    </div>
    <span style="clear:both"></span>
</asp:Content>