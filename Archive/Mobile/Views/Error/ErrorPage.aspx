﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Error.Master" Inherits="System.Web.Mvc.ViewPage<TelligentEvolution.Mobile.Web.Models.ErrorPageViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	<%= Model.Title %>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="error-container">
        <h2 class="error-header"><%= Html.LocalizedString("Error_Oops") %></h2>
        <div class="error-message">
            <%= Model.Content %>
        </div>
        <a href='<%= Url.HomeAction() %>' class="internal-link view-activity"><span></span><%= Html.LocalizedString("Error_ViewHome")%></a>
    </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ActionTitle" runat="server">
    <%= Model.ActionTitle %>
</asp:Content>

