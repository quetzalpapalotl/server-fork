<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<TelligentEvolution.Mobile.Web.Data.Entities.SearchResult>" %>

<li class="list-item">
    <div class="avatar"><img src='<%= Model.AvatarUrl %>' alt="" /></div>
    <div class="item-header">          
        <div class="content-name">
            <% if (Model.ContentType == "user" && Model.Author != null)
                { %>
                <a href='<%= Url.UserProfileAction(Model.Author.Username) %>' class="internal-link view-content"><%: Model.Title %></a>
            <% }
                   else if (Model.ContentType == "group")
                { %>
                <a href='<%= Url.GroupInfoAction(Model.ContentId) %>' class="internal-link view-content"><%: Model.Title %></a>
            <% }
                   else if (Model.IsApplication)
                { %>
                <a href='<%= Url.ApplicationInfoAction(Model.ApplicationId, Model.ApplicationType) %>' class="internal-link view-content"><%: Model.Title %></a>
            <% }
                else
                {%>
                <a href='<%= Model.Url %>' class="internal-link view-content"><%: Model.Title %></a>
            <% } %>
        </div>
    </div>
    <% if (Model.Author != null && Model.ContentType != "user")
        { %>
        <div class="content-author">
            <%= Html.UserProfileLink(Model.Author, false)%> - <%= Html.LocalizedString(Model.ContentTypeResourceName)%>
        </div>
    <% } %>
    <% if (Model.ContentType == "user") { %>
        <div class="content-date"><%= Html.LocalizedString("Search_Results_Created")%> <%= Html.FormatAgoDate(Model.CreatedDate)%></div>
    <% } else { %>
        <div class="content-date"><%= Html.FormatAgoDate(Model.CreatedDate)%></div>
    <% } %>
    <% if (Model.Author != null && Model.ContentType != "user" && !String.IsNullOrEmpty(Model.ApplicationName))
        { %>
        <div class="content-app"><%= Html.LocalizedString("Search_Results_In") %> <a href='<%= Url.ApplicationInfoAction(Model.ApplicationId, Model.ApplicationType) %>' class="internal-link view-content"><%= Model.ApplicationName%></a></div>
    <% } %>
</li>