﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<TelligentEvolution.Mobile.Web.Models.MessagePageViewModel>" MasterPageFile="~/Views/Shared/Site.Master" %>

<asp:Content ContentPlaceHolderID="MainContent" runat="server">
    <div class="message-container">
        <h2 class="message-header"><%= Model.ActionTitle %></h2>
        <div class="message-content">
            <%= Model.Content %>
        </div>
    </div>
</asp:Content>