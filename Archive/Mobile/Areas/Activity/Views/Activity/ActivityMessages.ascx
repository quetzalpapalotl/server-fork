<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<TelligentEvolution.Mobile.Web.Data.Entities.Message>" %>

<li id="<%= Model.Id %>" class="list-item">
    <div class="item-author">
        <span class="avatar"><%= Html.UserProfileLink(Model.Author, true) %></span>
    </div>
    <div class="message-body user-defined-markup">
        <% if (Model.IsStatus)
           { %>
            <%= Html.UserProfileLink(Model.Author, false) %>
        <% } %>
        <%= Model.MessageBody %>
		<% if (!Model.GroupView && Model.IsStatus && Model.Group != null) { %>
			<span class="message-group"><%= Html.LocalizedString("Status_StatusMessage_InGroup")%>
                <%=Html.GroupInfoLink(Model.Group, false) %>
            </span>
		<% } %>
    </div>
    <% if (Model.SimilarMessagesCount > 1)
       { %>
        <div class="item-similar-count"><%= Model.SimilarMessagesCount %></div>
    <% } %>
    <div class="item-date"><%= Html.FormatAgoDate(Model.CreatedDate) %></div>
    <div class="item-date-hidden"><%= Model.CreatedDate.Ticks %></div>
    <% if (Model.SupportsReplies)
       { %>
        <div class="reply-count">
            <%= Html.ReplyCountButton(Model)%>
	    </div>
    <% } %>
</li>