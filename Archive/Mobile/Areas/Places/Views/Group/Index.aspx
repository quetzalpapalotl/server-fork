﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<TelligentEvolution.Mobile.Web.Models.GroupViewModel>" %>
<%@ Import Namespace="TelligentEvolution.Mobile.Web.Models" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ScriptContent" runat="server">
    <script type="text/javascript">
	    // <![CDATA[

        $(function ()
        {        
            <% if (Model.MembershipRequest != null) { %>
            var createUrl = '<%=Url.Action("CreateMembership", "Group", new { area = "Places" })%>';
            var cancelUrl = '<%=Url.Action("CancelMembership", "Group", new { area = "Places" })%>';
            $('.group-details').membershipRequest(createUrl, cancelUrl, '#createMembershipRequestContainer');
            <% } %>
        });

    // ]]>
    </script>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <% if (Model.MembershipRequest != null && (!Model.MembershipRequest.UserCanJoinGroup && Model.MembershipRequest.UserCanJoinGroupByRequest))
    {
        Html.RenderPartial("CreateMembershipRequest", new CreateMessageViewModel());
    }%>

    <div class="group-details">
        <span class="avatar"><img src='<%= Model.Group.AvatarUrl %>' border="0" alt="<%= Model.Group.Name %>" /></span>
        <span class="group-name"><%= Model.Group.Name %></span>
        <% if (Model.MembershipRequest != null ) Html.RenderPartial("MembershipActions", Model.MembershipRequest); %>
    </div>
    <% if (Model.UserCanReadGroup) { %>
        <% Html.RenderPartial("TabsContainer", Model); %>
    <% } else { %>
            <% bool pending = Model.MembershipRequest != null && Model.MembershipRequest.MembershipType == TelligentEvolution.Mobile.Web.Data.Entities.MembershipType.PendingMember;  %>
            <div class="no-request-msg" style="display:<%= pending ? "none" : "" %>">
                <%=Html.LocalizedString("Group_No_Pending_Request_Message")%>
            </div>
            <div class="pending-request-msg" style="display:<%= !pending ? "none" : "" %>">
                <%=Html.LocalizedString("Group_Pending_Request_Message")%>
            </div>
    <% } %>
</asp:Content>
