<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<TelligentEvolution.Mobile.Web.Models.CreateCommentViewModel>" %>
<%@ Import Namespace="TelligentEvolution.Mobile.Web.Data.Entities" %>

<script type="text/javascript">
// <![CDATA[
    function showCreateCommentContainer(link) { $('#createCommentContainer').createMessageContainer(link); };
    $(function () { $("textarea").maxlength(<%= Html.SiteSettings().MaxMessageLength %>); })
// ]]>
</script>

<div id="createCommentContainer" class="post-form-area" style="display: none;">
    <% using (Html.BeginForm(new { area = "Places", controller = "Application", action = "CreateComment"})) { %>
        <div class="field-list-header"><span></span></div>
        <fieldset class="field-list">
	        <ul class="field-list">
		        <li class="field-item post-content">
			        <label for="Message" class="field-item-header"></label>
			        <span class="field-item-input"><%= Html.TextAreaFor(m => m.Message, new { cols = 30, rows = 4 })%></span>
                    <% Html.ValidateFor(m => m.Message); %>
			        <%= Html.AntiForgeryToken() %>
		        </li>
		        <li class="field-item submit">
			        <span class="field-item-input">
                        <% if (Model.ThreadType == ThreadTypes.QuestionAndAnswer) { %>
                            <%= Html.CheckBoxFor(m => m.IsSuggestedAnswer)%>
                            <label for="IsSuggestedAnswer" class="field-item-label"><%= Html.LocalizedString("ApplicationContent_Comments_SuggestAsAnswer")%></label>
                        <% } %>
				        <a href="#" class="internal-link cancel-post"><span></span></a>
				        <a href="#" class="internal-link add-post"><span></span></a>
			        </span>
		        </li>
	        </ul>
        </fieldset>
        <div class="field-list-footer comment"><span></span></div>
    <% } %>
</div>