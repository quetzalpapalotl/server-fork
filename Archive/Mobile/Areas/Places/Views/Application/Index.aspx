<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<TelligentEvolution.Mobile.Web.Models.ApplicationViewModel>" %>
<%@ Import Namespace="TelligentEvolution.Mobile.Web.Data" %>
<%@ Import Namespace="TelligentEvolution.Mobile.Web.Models" %>

<asp:Content ID="Content1" ContentPlaceHolderID="CreateMessage" runat="server">
    <% if (Model.ShowCreateMessageButton){ 
           if (Model.UserCanPostMessage) {%>
                <% Html.RenderPartial(Model.CreatePostViewName, Model.CreatePostModel); %>
            <% } else { %>
                <% Html.RenderPartial("CreateMessage", new CreateMessageViewModel() { ShowForm = Model.ShowCreateMessageButton }); %>
            <% } %>
     <% } %>
</asp:Content>

<asp:Content ContentPlaceHolderID="MainContent" runat="server">
<script type="text/javascript">
	// <![CDATA[
        
        <% if (Model.UserCanPostMessage) {%>
        
        $(function ()
        {
		    $('.new-app-post').bind('telligent_mobile_createmessage_success', function(){ $('.list-view').trigger("telligent_mobile_list_reload", []); } );
        });

        <%} %>

	// ]]>
    </script>
    <div class="application-container">
        <div class="application-name"><div><%= Model.Application.Name %></div></div>
        <% Html.RenderAction("ContentList", "Application", new { area = "Places", id = Model.Application.Id, applicationType = Model.Application.ApplicationType }); %>
    </div>
</asp:Content>

