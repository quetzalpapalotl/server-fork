<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<TelligentEvolution.Mobile.Web.Models.ApplicationContentViewModel>" %>
<%@ Import Namespace="TelligentEvolution.Mobile.Web.Models" %>

<asp:Content ContentPlaceHolderID="MainContent" runat="server">
	<script type="text/javascript">
	// <![CDATA[

        var replyCount = <%= Model.ApplicationContent.ReplyCount %>;
        $(function ()
        {
		    $('#createCommentContainer').bind('telligent_mobile_createmessage_show', function () { $('.add-reply-area').hide(); })
                                        .bind('telligent_mobile_createmessage_close', function () { $('.add-reply-area').show(); })
                                        .bind('telligent_mobile_createmessage_success', commentCreated );
        });

		function commentCreated() 
        {   
            replyCount++;
			$('.reply-count .add-reply').html('<span></span>' + replyCount);
            $('.list-view').trigger("telligent_mobile_list_reload", []);
		}

	// ]]>
    </script>

    <div class="application-content">
        <div class="content-header">
            <span class="content-title"><%: Model.ApplicationContent.Title %></span>
            <span class="avatar"><img src='<%= Model.ApplicationContent.Author.AvatarUrl %>' border="0" alt="<%= Model.ApplicationContent.Author.DisplayName %>" /></span>
            <span class="user-name"><%= Html.LocalizedString("ApplicationContent_Index_CreatedBy") %> <%= Html.UserProfileLink(Model.ApplicationContent.Author, false)%></span>
            <span class="content-date"><%= Html.FormatAgoDate(Model.ApplicationContent.PublishedDate)%></span>
            <span class="reply-count"><a href="#" class="internal-link add-reply"><span></span><%= Model.ApplicationContent.ReplyCount > 0 ? Model.ApplicationContent.ReplyCount.ToString() : "+"%></a></span>
        </div>
    </div>
	<ul class="item-list replies">
		<% Html.RenderAction("CommentList", "Application", new { area = "Places", id = Model.ApplicationContent.Id, applicationId = Model.ApplicationContent.ApplicationId, applicationType = Model.ApplicationContent.ApplicationType }); %>
	</ul>

    <% if (Model.UserCanReply)
       { %>
	    <div class="add-reply-area">
            <%= Html.ReplyCommentCountButton(Model.ApplicationContent, Html.LocalizedString("ApplicationContent_Comments_AddAComment"))%>
	    </div>
	    <% Html.RenderPartial("CreateComment", new CreateCommentViewModel() { ThreadType = Model.ApplicationContent.ThreadType }); %>
    <% } %>
</asp:Content>
