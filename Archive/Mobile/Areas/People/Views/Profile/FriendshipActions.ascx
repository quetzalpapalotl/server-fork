﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<TelligentEvolution.Mobile.Web.Data.Entities.FriendshipRequest>" %>
<%@ Import Namespace="TelligentEvolution.Mobile.Web.Areas.People.Controllers" %>
<%@ Import Namespace="TelligentEvolution.Mobile.Web.Data.Entities" %>
<% int? userId = ViewBag.UserId as int?; %>
<div class="request-actions-area">
<% if (Model.State == FriendshipState.NotSpecified && Model.RequesteeId == 0 && Model.RequestorId == 0) { %>
    <%= Html.CreatePendingRequestActionLink(userId, Html.LocalizedString("People_Profile_Pending_Request_Create"), "internal-link create-request")%>    
<% } else if (Model.State == FriendshipState.Approved) { %>
    <%= Html.UpdatePendingRequestActionLink(userId, Html.LocalizedString("People_Profile_Pending_Request_Remove"), "internal-link remove-request", (int)PendingRequestState.Reject)%>
<% } else if (Model.State == FriendshipState.Pending && userId.HasValue && userId.Value == Model.RequesteeId) { %>
    <%= Html.UpdatePendingRequestActionLink(userId, Html.LocalizedString("People_Profile_Pending_Request_Cancel"), "internal-link cancel-request", (int)PendingRequestState.Reject)%>
<% } %>
</div>
