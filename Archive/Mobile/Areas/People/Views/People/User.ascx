﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<TelligentEvolution.Mobile.Web.Data.Entities.User>" %>

<li class="list-item">
    <div class="avatar"><%= Html.UserProfileLink(Model, true)%></div>
    <div class="user-name"><%= Html.UserProfileLink(Model, false)%></div>
</li>