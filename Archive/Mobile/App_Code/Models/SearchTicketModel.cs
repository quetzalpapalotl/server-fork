﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TelligentEvolution.Mobile.Web.Models;
using TelligentEvolution.Mobile.Web.Data;
using Omnicell.Data.Model;

/// <summary>
/// Summary description for SearchTicketModel
/// </summary>
public class SearchTicketModel :BasePageViewModel
{
    public string txtOmniSearch { get; set; }
    public bool activeOnly { get; set; }
    public bool isCsnSearch { get; set; }

    public List<Ticket> Tickets { get; set; }

    public SearchTicketModel(IUserContext userContext)
        : base(userContext)
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public SearchTicketModel()
        : this(Services.Get<IUserContext>())
    {
    }
}