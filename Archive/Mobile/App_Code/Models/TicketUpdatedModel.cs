﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TelligentEvolution.Mobile.Web.Models;
using TelligentEvolution.Mobile.Web.Data;
using Omnicell.Data.Model;

/// <summary>
/// Summary description for TicketUpdatedModel
/// </summary>
public class TicketUpdatedModel :BasePageViewModel
{
    public Ticket Ticket { get; set; }

    public List<string> Statuses { get; set; }
    public List<string> SubStatuses { get; set; }

    public bool IsPost { get; set; }
    public string SearchText { get; set; }
    public bool IsActiveOnly { get; set; }
    public bool IsCsnSearch { get; set; }

    public TicketUpdatedModel(IUserContext userContext)
        : base(userContext)
	{
        //Statuses = new List<string>();
        //Statuses.Add("New");
        //Statuses.Add("Pending");
        //Statuses.Add("Closed");

        //SubStatuses = new List<string>();
        //SubStatuses.Add("Replace");
        //SubStatuses.Add("Repair");
        //SubStatuses.Add("Closed");
    }

    public TicketUpdatedModel()
        : this(Services.Get<IUserContext>())
    {
    }
}