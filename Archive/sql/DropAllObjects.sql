declare @cmd varchar(4000), @id int
DECLARE @cmds table (id int identity(1, 1) not null, cmd varchar(4000) not null)

set nocount on

insert into @cmds ( cmd )
select cmd from (
	/* order 10, 20 for Foreign Key, then Primary */
	select
		N'alter table ' + quotename(TABLE_SCHEMA) + N'.' + quotename(TABLE_NAME) + N' drop constraint ' + quotename(CONSTRAINT_NAME) + N';'
		, case CONSTRAINT_TYPE WHEN N'PRIMARY KEY' then 20 else 10 end as Ordinal
	from
		INFORMATION_SCHEMA.TABLE_CONSTRAINTS
	where
		CONSTRAINT_TYPE IN (N'PRIMARY KEY', N'FOREIGN KEY')  AND TABLE_NAME is not null
	union all
	/* 30: Drop all sprocs next */
	select
		N'drop ' + ROUTINE_TYPE + N' ' + quotename(ROUTINE_SCHEMA) + N'.' + quotename(ROUTINE_NAME) + N';'
		, 30 as Ordinal
	from
		INFORMATION_SCHEMA.ROUTINES
	union all
	/* 40: Drop all views next */
	select
		N'drop view ' + quotename(TABLE_SCHEMA) + N'.' + quotename(TABLE_NAME) + N';'
		, 40 as Ordinal
	from
		INFORMATION_SCHEMA.TABLES
	where
		TABLE_TYPE = N'VIEW'
	union all
	/* 50: Drop all tables next */
	select
		N'drop table ' + quotename(TABLE_SCHEMA) + N'.' + quotename(TABLE_NAME) + N';'
		, 50 as Ordinal
	from
		INFORMATION_SCHEMA.TABLES
	where
		TABLE_TYPE = N'BASE TABLE'
    union all
    /* 60: Drop all Schemas next */
    select
        N'drop schema ' + quotename(SCHEMA_NAME) + N';'
        , 60 as Ordinal
    from
        INFORMATION_SCHEMA.SCHEMATA
    where
        SCHEMA_NAME LIKE N'aspnet_%'
    union all
    /* 70: Drop all Roles next */
    select
        N'DROP ROLE ' + quotename(name) + N';',
		70 as Ordinal
    from 
		sys.database_principals
    WHERE
		name like N'aspnet_%'

) as z (cmd, Ordinal)
order by
	Ordinal

set @id = 1 /* starting point of the identity column we created */
WHILE (1=1)
BEGIN
	select @cmd = c.cmd from @cmds c where c.id = @id
	if @@ROWCOUNT = 0 BREAK
	
	print 'Executing: ' + @cmd
    EXEC (@cmd)
    
    SET @id = @id + 1
END
