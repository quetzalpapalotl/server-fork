(function($) {

    var openQueueContext = null;

    // read queue UI which can render endlessly-scrolling items in a popup and notification bubbles
    var ReadQueue = (function($){
        // private static stuff
        var titleCount = /^\([0-9]+\)\s+/,
            totalTitleCount = 0, // title bar count which multiple instances can contribute to
            defaults = {
                link: '',
                initialUnreadCount: 0,
                headingLabel: 'Queue',
                showCount: true,
                endlessScroll: false,
                showMoreUrl: null,
                showMoreLabel: null,
                template: '',
                loading: false,
                titleCount: false,
                previousCount: 0,
                unreadCountMessageSingular: 'You have {0} unread item',
                unreadCountMessagePlural: 'You have {0} unread items',
                onShow: function() {
                    // called on each show
                },
                onLoad: function(pageIndex, shouldRefreshUnreadCount, filter, complete) {
                    // load more items
                    // for each item loaded, marks it as read
                    // after items loaded and marks as read, gets new unread count and updates via updateUnreadCount via refreshUnreadCount
                    // complete(contentToShow)
                    complete('');
                },
                onRefreshUnreadCount: function(complete) {
                    // get new unread count
                    // update it via complete()
                    complete(5);
                },
                onBuildFilter: null // function(filtered) {
                    // when defined, returns a DOM node to be inserted at the top which presentes a filtering UI
                    // filtered(filter) can be called to raise a filter
                // }
            };

        function updateTitle() {
            if(totalTitleCount <= 0) {
                document.title = document.title.replace(titleCount, '');
            } else {
                document.title = ' (' + totalTitleCount + ') ' + document.title.replace(titleCount, '');
            }
        }

        function blockScrollingWhileOver(element) {
            var el = $(element);
            el.on('mouseenter', function(){
                el.on('DOMMouseScroll mousewheel', function(e){
                    var e = e || window.event,
                        originalEvent = e.originalEvent || e,
                        delta = 0,
                        scrollTop = el.get(0).scrollTop,
                        scrollHeight = el.get(0).scrollHeight,
                        height = el.height();

                    if (originalEvent.wheelDelta)
                        delta = originalEvent.wheelDelta/120;
                    if (originalEvent.detail)
                        delta = -originalEvent.detail/3;

                    if((scrollTop === (scrollHeight - height) && delta < 0) || (scrollTop === 0 && delta > 0)) {
                        if(e.preventDefault)
                            e.preventDefault();
                        e.returnValue = false;
                    }
                })
            })
            .on('mouseleave', function(){
                el.off('DOMMouseScroll mousewheel');
            });
        }

        function buildCountBubble(context) {
            var wrapper = context.link.wrap('<span></span>').parent('span').css({
                position: 'relative'
            });
            var count = $('<a href="#" class="read-queue-count"></a>');
            count.appendTo(wrapper).hide().on('click', function(e){
                e.preventDefault();
                context.link.click();
                //handlePopoupOpenerClick(context);
            });
            return count;
        }

        function buildUnreadCountMessage(context, count) {
            count = count || 0;
            if(count === 1) {
                return context.unreadCountMessageSingular.replace(/\{0\}/gi, count);
            } else {
                return context.unreadCountMessagePlural.replace(/\{0\}/gi, count);
            }
        }

        function showUnreadCount(context, count) {
            if(!context.showCount)
                return;
            var difference = count - context.previousCount;
            context.previousCount = count;
            if(context.titleCount) {
                totalTitleCount += difference;
                updateTitle();
            }
            var unreadCountMessage = buildUnreadCountMessage(context, count);
            if(count <= 0) {
                // remove bubble ui
                if(context.count)
                    context.count.fadeOut(200);
                context.link.attr('title', unreadCountMessage);
            } else {
                // add bubble ui if not there
                context.count = context.count || buildCountBubble(context);
                // set the count and display it
                context.count
                    .html(count)
                    .attr('title', unreadCountMessage)
                    .fadeIn(200);
                context.link.attr('title', unreadCountMessage);
            }
        }

        function loadContent(context, pageIndex, shouldRefreshUnreadCount) {
            context.loading = true;
            context.popupContentLoader.show();
            // call the injected loading method, and return a promise
            // which, when done, provides the content from the loading method
            return $.Deferred(function(dfd){
                context.onLoad(pageIndex, shouldRefreshUnreadCount, context.filter || '', function(content){
                    dfd.resolve(content);
                });
            }).promise();
        }

        function buildPopUp(context) {
            context.compiledTemplate = context.compiledTemplate || $.telligent.evolution.template.compile(context.template);

            context.popup = $(context.compiledTemplate(context));
            context.popupContentList = context.popup.find('.content-list');
            context.popupContentLoader = context.popup.find('.loading');
            context.popup.glowPopUpPanel({
                cssClass: 'menu read-queue queue-content',
                hideOnDocumentClick: true,
                position: 'down',
                zIndex: 1000
            }).on('glowPopUpPanelHidden', function(){
                context.link.removeClass('active');
            });
            if(context.onBuildFilter !== null) {
                var filter = context.onBuildFilter(function(result){
                    context.filter = result;
                    context.currentPageIndex = 0;
                    loadContent(context, context.currentPageIndex, true).then(function(content) {
                        context.popupContentLoader.hide();
                        appendToPopup(context, content, true);
                    });
                });
                if(filter) {
                    $(filter).insertBefore(context.popupContentList);
                }
            }
        }

        function showPopup(context, content) {
            if(openQueueContext) {
                hidePopup(openQueueContext);
            }
            openQueueContext = context;
            context.popupContentList.html(content);
            context.popup.glowPopUpPanel('show', context.link);
            context.loading = false;
            context.hasMore = context.popupContentList.children('li:last').data('hasmore') ? true : false;
            blockScrollingWhileOver(context.popupContentList);
            context.link.addClass('active');
            if(context.onShow)
                context.onShow();
            context.popupContentLoader.hide();
            context.popupContent = context.popupContentList.closest('.read-queue');
            context.popupContainer = context.popupContent.parent();
        }

        function hidePopup(context) {
            context.popup.glowPopUpPanel('hide');
        }

        function appendToPopup(context, content, shouldReplace) {
            if(shouldReplace) {
                context.popupContentList.html(content);
            } else {
                context.popupContentList.append(content);
            }
            context.loading = false;
            context.hasMore = context.popupContentList.children('li:last').data('hasmore') ? true : false;
            context.popupContentLoader.hide();
            context.popupContainer.css({ height: context.popupContent.outerHeight() });
        }

        function handlePopoupOpenerClick(context) {
            if(context.popup.glowPopUpPanel('isOpening')) {
                return;
            } else if(context.popup.glowPopUpPanel('isShown')) {
                hidePopup(context);
            } else {
                context.currentPageIndex = 0;
                context.hasMore = true;
                loadContent(context, context.currentPageIndex, true).then(function(content) {
                    showPopup(context, content);
                });
            }
        }

        function handleEvents(context) {
            context.link.on('click', function(e){
                e.preventDefault();
                handlePopoupOpenerClick(context);
                return false;
            });
            // handle endless scrolling if enabled
            if(context.endlessScroll) {
                context.popupContentList.on('scrollend', function(){
                    if(!context.hasMore) {
                        return false;
                    }
                    if(context.loading) {
                        return false;
                    }
                    context.popupContentLoader.show();
                    context.currentPageIndex++;
                    loadContent(context, context.currentPageIndex, true).then(function(content) {
                        context.popupContentLoader.hide();
                        appendToPopup(context, content);
                    });
                    return false;
                });
            }
            context.popupContentList.on('click','.content-item', function(e){
                e.preventDefault();
                var url = $(this).data('contenturl');
                if(url) {
                    window.location = url;
                }
            });
            context.popupContentList.on('click','a',function(e){
                e.stopPropagation();
            });
        }

        function initContext(context) {
            context.link = $(context.link);
        }

        var api = function(options) {
            // private instance stuff
            var context = $.extend({}, defaults, options || {});
            initContext(context);

            // lazy-setup until clicked
            context.link.one('click', function(e){
                e.preventDefault();
                buildPopUp(context);
                handleEvents(context);
                context.link.click();
            });
            showUnreadCount(context, context.initialUnreadCount);

            return {
                refreshUnreadCount: function() {
                    return context.onRefreshUnreadCount(function(newCount){
                        showUnreadCount(context, newCount);
                    });
                },
                refreshContent: function() {
                    // refreshing content only has an effect if already shown
                    if(context.popup && context.popup.glowPopUpPanel('isShown')) {
                        context.currentPageIndex = 0;
                        loadContent(context, context.currentPageIndex, false).then(function(content) {
                            appendToPopup(context, content, true);
                        });
                    }
                },
                content: function() {
                    return context.popupContentList;
                }
            };
        };
        api.hideCurrent = function() {
            if(openQueueContext) {
                hidePopup(openQueueContext);
            }
        };

        return api;

    })($);

    var initNotificationQueue = function(context) {
        var initPreferenceHandlers = function() {
            // display preference change ui on 'x' click
            context.notificationsQueue.content().on('click', 'a.preference', function(e) {
                e.preventDefault();
                displayNotificationPreference(context, $(this).closest('li'));
                return false;
            });

            // handle cancels of preference change when clicking 'cancel'
            context.notificationsQueue.content().on('click', '.notification-preference .cancel', function(e){
                e.preventDefault();
                e.stopPropagation();
                hideNotificationPreference(context, $(this).closest('.notification-preference'));
                context.preferenceUi = null;
                return false;
            });

            // handle cancels of preference change by clicking anything other than 'turn off'
            context.notificationsQueue.content().on('click', '.notification-preference', function(e){
                e.preventDefault();
                e.stopPropagation();
                hideNotificationPreference(context, $(this));
                context.preferenceUi = null;
                return false;
            });

            // handle confirmed preference changes
            context.notificationsQueue.content().on('click', '.notification-preference .confirm', function(e){
                e.preventDefault();
                var notificationPreference = $(this).closest('.notification-preference'),
                    notificationItem = notificationPreference.closest('li');
                disableNotificationType(context, $(this).data('notificationtypeid')).then(function(){
                    notificationItem.fadeOut(200, function() {
                        notificationPreference.remove();
                        notificationItem.remove();
                    });
                    context.preferenceUi = null;
                });
                return false;
            });
        };

        context.notificationsQueue = ReadQueue({
            headingLabel: context.notificationsLabel,
            initialUnreadCount: context.notificationsUnread,
            link: context.notificationsLink,
            endlessScroll: true,
            titleCount: true,
            unreadCountMessageSingular: context.notificationssUnreadCountMessageSingular,
            unreadCountMessagePlural: context.notificationssUnreadCountMessagePlural,
            onLoad: function(pageIndex, shouldRefreshUnreadCount, filter, complete) {
                if(!context.isInited) {
                    context.isInited = true;
                    initPreferenceHandlers();
                }
                if(context.notificationList) { context.notificationList.css({ height: 'auto' }); }
                $.telligent.evolution.get({
                    url: context.notificationsUrl,
                    data: {
                        w_pageIndex: pageIndex
                    },
                    success: function(response) {
                        // show response
                        complete(response);
                        // update count
                        if(shouldRefreshUnreadCount)
                            context.notificationsQueue.refreshUnreadCount();
                    }
                });
            },
            onRefreshUnreadCount: function(complete) {
                var query = {
                    IsRead: false,
                    PageSize: 1,
                    PageIndex: 0,
                };
                // exclude conversation notifications
                query['_Filters_' + context.conversationNotificationTypeId] = 'Exclude';
                return $.telligent.evolution.get({
                    url: $.telligent.evolution.site.getBaseUrl() + 'api.ashx/v2/notifications.json',
                    cache: false,
                    data: query,
                    success: function(response) {
                        complete(response.TotalCount);
                    }
                });
            },
            template: context.notificationsTemplate
        });

        // update the notification queue's count when a new notification is received which isn't a conversation type
        $.telligent.evolution.messaging.subscribe('notification.raised', function(notification) {
            if(notification.typeId !== context.conversationNotificationTypeId) {
                context.notificationsQueue.refreshUnreadCount().then(function(){
                    context.notificationsQueue.refreshContent();
                });
            }
        });
    },
    displayNotificationPreference = function(context, notificationItem) {
        // set the notification list size during preference-displaying so that it does not resize out of the bounds of the popup
        context.notificationList = context.notificationList || notificationItem.closest('ul');
        context.notificationList.css({
            height: context.notificationList.height()
        });
        if(context.preferenceUi) {
            hideNotificationPreference(context, context.preferenceUi);
            context.preferenceUi = null;
        }
        context.preferenceTemplate = context.preferenceTemplate || $.telligent.evolution.template.compile(context.notificationPreferenceTemplate);
        context.preferenceUi = $(context.preferenceTemplate({
                notificationTypeName: notificationItem.data('notificationtypename'),
                notificationTypeId: notificationItem.data('notificationtypeid')
            }))
            .hide()
            .appendTo(notificationItem);
        context.preferenceUi.css({
            position: 'absolute',
            top: 0,
            left: 0,
            zIndex: 2000,
            backgroundColor: '#fff'
        });

        notificationItem.addClass('with-preference');
        notificationItem.data('originalHeight', notificationItem.height());
        notificationItem.animate({ height: context.preferenceUi.height() }, 150);

        context.preferenceUi.fadeTo(150, 0.95);
    },
    hideNotificationPreference = function(context, preferenceItem) {
        context.notificationList.css({ height: 'auto' });
        if(preferenceItem) {
            var notificationItem = preferenceItem.parent();
            notificationItem.animate({ height: notificationItem.data('originalHeight') }, 150);
            preferenceItem.fadeOut(150, function(){
                notificationItem.removeClass('with-preference');
                preferenceItem.remove();
            });
        }
    },
    disableNotificationType = function(context, notificationTypeId) {
        return $.telligent.evolution.put({
            url: $.telligent.evolution.site.getBaseUrl() + 'api.ashx/v2/notificationpreference.json',
            data: {
                NotificationTypeId: notificationTypeId,
                IsEnabled: false
            },
            dataType: 'json'
        });
    },
    initConversationQueue = function(context) {

        context.conversationsQueue = ReadQueue({
            headingLabel: context.conversationsLabel,
            initialUnreadCount: context.conversationsUnread,
            link: context.conversationsLink,
            endlessScroll: true,
            titleCount: true,
            showMoreUrl: context.conversationsMoreUrl,
            showMoreLabel: context.conversationsMoreLabel,
            unreadCountMessageSingular: context.conversationsUnreadCountMessageSingular,
            unreadCountMessagePlural: context.conversationsUnreadCountMessagePlural,
            onLoad: function(pageIndex, shouldRefreshUnreadCount, filter,complete) {
                $.telligent.evolution.get({
                    url: context.conversationsUrl,
                    data: {
                        w_pageIndex: pageIndex
                    },
                    success: function(response) {
                        // show response
                        complete(response);
                        // update count
                        if(shouldRefreshUnreadCount)
                            context.conversationsQueue.refreshUnreadCount();
                    }
                });
            },
            onRefreshUnreadCount: function(complete) {
                return $.telligent.evolution.get({
                    url: $.telligent.evolution.site.getBaseUrl() + 'api.ashx/v2/conversations.json',
                    cache: false,
                    data: {
                        ReadStatus: 'Unread'
                    },
                    success: function(response) {
                        complete(response.TotalCount);
                    }
                });
            },
            template: context.conversationsTemplate
        });

        // update the message queue's count when a new notification is received which is a conversation type
        $.telligent.evolution.messaging.subscribe('notification.raised', function(notification) {
            if(notification.typeId === context.conversationNotificationTypeId) {
                context.conversationsQueue.refreshUnreadCount().then(function(){
                    context.conversationsQueue.refreshContent();
                });
            }
        });

        // update the message queue's count when a message was read on the conversation list
        $.telligent.evolution.messaging.subscribe('ui.messageread', function(notification) {
            context.conversationsQueue.refreshUnreadCount();
        });
    },
    initBookmarksList = function(context) {
        // gets all selected content types from the content type filter
        var getCurrentContentTypes = function(bookmarkFilter) {
            var contentTypes = [];
            var selectedFilters = context.bookmarkFilter.find('a.selected').each(function(){
                contentTypes.push($(this).data('contenttypeids'));
            });
            return contentTypes.join(',');
        };
        context.bookmarksList = ReadQueue({
            headingLabel: context.bookmarksLabel,
            link: context.bookmarksLink,
            endlessScroll: true,
            titleCount: false,
            showCount: false,
            showMoreUrl: context.bookmarksMoreUrl,
            showMoreLabel: context.bookmarksMoreLabel,
            onLoad: function(pageIndex, shouldRefreshUnreadCount, filter, complete) {
                var filteredContentTypeIds = filter || getCurrentContentTypes(context.bookmarkFilter);
                $.telligent.evolution.get({
                    url: context.bookmarksUrl,
                    data: {
                        w_pageIndex: pageIndex,
                        w_contentTypeIds: filteredContentTypeIds
                    },
                    success: function(response) {
                        if(filteredContentTypeIds && filteredContentTypeIds.length > 0 && getCurrentContentTypes(context.bookmarkFilter) !== filteredContentTypeIds)
                            return;
                        // show response
                        complete(response);
                        // update count
                        if(shouldRefreshUnreadCount)
                            context.bookmarksList.refreshUnreadCount();
                    }
                });
            },
            onRefreshUnreadCount: function(complete) {
                complete(0);
            },
            onBuildFilter: function(filtered) {
                var filterTemplate = $.telligent.evolution.template.compile(context.bookmarksFilterTemplate),
                    filterTemplateData = {
                        contentTypeIds: '',
                        applicationContentTypeIds: '',
                        containerTypes: []
                    };
                if(context.bookmarksContentTypes.length > 0)
                    filterTemplateData.contentTypeIds = context.bookmarksContentTypes.substr(0, context.bookmarksContentTypes.length - 1)
                if(context.bookmarksApplicationContentTypes.length > 0)
                    filterTemplateData.applicationContentTypeIds = context.bookmarksApplicationContentTypes.substr(0, context.bookmarksApplicationContentTypes.length - 1)
                if(context.bookmarksContainerContentTypes.length > 0) {
                    var rawContainers = context.bookmarksContainerContentTypes.split(',');
                    $.each(rawContainers, function(i, rawContainer) {
                        if(rawContainer && rawContainer.length > 0) {
                            var containerComponents = rawContainer.split(':', 2);
                            if(containerComponents.length === 2) {
                                filterTemplateData.containerTypes.push({
                                    name: containerComponents[1],
                                    id: containerComponents[0]
                                });
                            }
                        }
                    });
                }

                context.bookmarkFilter = $(filterTemplate(filterTemplateData));
                context.bookmarkFilter.find('a:first').addClass('selected');
                context.bookmarkFilter.on('click','a', function(e){
                    e.preventDefault();
                    e.stopPropagation();
                    var target = $(e.target);
                    target.closest('ul').find('a').removeClass('selected');
                    target.addClass('selected');
                    filtered(getCurrentContentTypes(context.bookmarkFilter));
                });

                return context.bookmarkFilter;
            },
            template: context.bookmarksTemplate
        });
    },
    initGroupsList = function(context) {

        context.groupsList = ReadQueue({
            headingLabel: context.groupsLabel,
            link: context.groupsLink,
            endlessScroll: true,
            titleCount: false,
            showCount: false,
            showMoreUrl: context.groupsMoreUrl,
            showMoreLabel: context.groupsMoreLabel,
            onLoad: function(pageIndex, shouldRefreshUnreadCount, filter, complete) {
                var filterQuery = filter;
                $.telligent.evolution.get({
                    url: context.groupsUrl,
                    data: {
                        w_pageIndex: pageIndex,
                        w_query: filterQuery
                    },
                    success: function(response) {
                        if(filterQuery && filterQuery.length > 0 && context.groupFilterInput.val() !== filterQuery) {
                            return;
                        }
                        // show response
                        complete(response);
                        // update count
                        if(shouldRefreshUnreadCount)
                            context.groupsList.refreshUnreadCount();
                    }
                });
            },
            onRefreshUnreadCount: function(complete) {
                complete(0);
            },
            onBuildFilter: function(filtered) {
                var filterInterval;

                var groupFilter = $($.telligent.evolution.template.compile(context.groupsFilterTemplate)());
                context.groupFilterInput = groupFilter.find('input');

                // live group filtering
                context.groupFilterInput
                    .on('click', function(e){ e.stopPropagation(); })
                    .on('keydown', function(e){
                        clearTimeout(filterInterval);
                        filterInterval = setTimeout(function() {
                            filtered(context.groupFilterInput.val());
                        }, 200);
                        if(e.which === 13) {
                            e.preventDefault();
                            filtered(context.groupFilterInput.val());
                        }
                    });

                return groupFilter;
            },
            onShow: function() {
                context.groupFilterInput.val('');
            },
            template: context.groupsTemplate
        });
    };

    var api = {
        register: function(context) {
            if(context.notificationsLink.length > 0)
                initNotificationQueue(context);
            if(context.conversationsLink.length > 0)
                initConversationQueue(context);
            if(context.groupsLink.length > 0)
                initGroupsList(context);
            if(context.bookmarksLink.length > 0)
                initBookmarksList(context);
        }
    };

    $.telligent = $.telligent || {};
    $.telligent.evolution = $.telligent.evolution || {};
    $.telligent.evolution.widgets = $.telligent.evolution.widgets || {};
    $.telligent.evolution.widgets.siteUserLinks = api;

}(jQuery));
