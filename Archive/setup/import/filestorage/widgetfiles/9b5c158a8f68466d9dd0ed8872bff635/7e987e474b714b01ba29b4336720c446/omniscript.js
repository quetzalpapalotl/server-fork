/*var contextQuery = jQuery('#question');
var contextSearchResultsPopup = jQuery('#answers_box');
var contextChangeTimeout;
var contextLastQuery;
var contextLastKeyCode;
var contextSearchTimeout;    

    function search()
	{
		$.telligent.evolution.get({
			url: contextSearchUrl,
			data: {
				w_searchText: contextQuery.val()
			},
			success: function(response)
			{
				contextSearchResultsPopup.html(response);
			},
			defaultErrorMessage: 'An error has occurred.',
			error: function(xhr, desc, ex)
			{
				contextSearchResultsPopup.html('<div class="message error error__message">' + desc + '</div>');
			}
		});
	};
	function checkForSearchQueryChange ()
	{
		clearTimeout(contextChangeTimeout);

		var value = contextQuery.val();
		if (value !== contextLastQuery)
		{
			contextLastQuery = value;
			contextLastKeyCode = -1;

			clearTimeout(contextSearchTimeout);

			if (contextLastQuery.length > 0)
			{
				contextQuery.removeClass('empty');
				contextSearchTimeout = setTimeout(function() { search(); }, 499);
			}
			else
			{
				contextQuery.addClass('empty');
			}
		}

		contextChangeTimeout = setTimeout(function() { checkForSearchQueryChange(); }, 99);
	}
*/

$(document).ready(function () {
    $('.technical_info').hide();
/*	contextQuery.focus(function()
	{
		if(!$(this).hasClass('empty'))
		{
			contextLastKeyCode = -1;
			clearTimeout(contextSearchTimeout);
			search();
		}

		clearTimeout(contextChangeTimeout);
		contextChangeTimeout = setTimeout(function() { checkForSearchQueryChange(); }, 99);
	})
	.blur(function()
	{
		clearTimeout(contextSearchTimeout);
		clearTimeout(contextChangeTimeout);
	});*/
});

function validateEmail(src) {
    var regex = /^[a-zA-Z0-9._-]+@([a-zA-Z0-9.-]+\.)+[a-zA-Z0-9.-]{2,4}$/;
    return regex.test(src);
}

function OmniformSeverityChange() {
    var severityType = $('#severity').val();
    
    if(severityType == 'down')
    {
        alert('Please call 800-910-2220 for immediate response.');
    }
}

function OmniformDropdownChange() {
    var emailConfigOption = $('#question_type option:selected').index()-1;

    var qType = $('#question_type').val();

    if (qType == 'technical') {
        $('.technical_info').show();
        $('.nontech').hide();
    }
    else {
        $('.technical_info').hide();
        $('.nontech').show();
    }

    $("#rcptEmail").val(omniRcptEmails[emailConfigOption]);
    $("#subjectLine").val(omnisubjectLines[emailConfigOption]);

}
omniFileCount = 1;

function OmniformAddFile() {
    $('.file_inputs').append('<input id="file' + omniFileCount + '" name="file' + omniFileCount + '" type=file><br />');
    omniFileCount++;
}

function submitOmniform() {
}




$(document).ready(function () {
    var monthNames = new Array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
    var d = new Date();
    var curMonth = d.getMonth();
    var curDate = d.getDate();
    var curYear = d.getFullYear();
    var displayDate = monthNames[curMonth] + " " + curDate + ", " + curYear;
    $("#request_date").val(displayDate);
    $("span.request_date").html(displayDate);

    //$("#omniformSubmittedMessage").hide();

    var url = window.location.href.split('?')[0];
    $("#returnUrl").val(url);

    if (window.location.href.indexOf("btnSubmit") != -1) {
        $("#omniformSubmittedMessage").show();
        $("#submitButtons").hide();
        $("#omniform").hide();
    }



});

function toggleOmniForm(btn) {
    if (btn.disabled) {
        btn.disabled = false;
        $("#submitButtons").show();
        $("#submitMessage").hide();
    } else {
        btn.disabled = true;
        $("#submitButtons").hide();
        $("#submitMessage").show();
    }
}

function resetOmniformFields() {
    
}

function completeOmniform(btn) {
    toggleOmniForm(btn);

    $("#submitButtons").hide();
    $("#submitMessage").hide();

    if (btn.id != "btnSubmitRestart") {
        $("#omniform").hide();
    } else {
        $("#submitButtons").show();
    }

    $("#omniformSubmittedMessage").show();

    resetOmniformFields();
}


function sendOmniformRequest(btn, url) {
    $("#returnUrl").val($("#returnUrl").val() + "?r=" + btn.id);

    //$("#" + btn.form.id).get(0).setAttribute('action', '/custom/SendOmniform.ashx');
    //$("#" + btn.form.id).get(0).setAttribute('enctype', 'multipart/form-data');
    //$("#" + btn.form.id).get(0).setAttribute('encoding', 'multipart/form-data');
    $("#" + btn.form.id).submit();    
}

function submitOmniform(btn, url, disableCSN) {
//    toggleOmniForm(btn);

var errorString = "";

    if ($('#first_name').val() == '') {
        errorString += "First Name is required.\n";
    }

    if ($('#last_name').val() == '') {
        errorString += "Last Name is required.\n";
    }

    if ($('#title').val() == '') {
        errorString += "Title is required.\n";
    }

    if ($('#email').val() == '') {
        errorString += "A valid email address is required.\n";
    }

    if ($('#facility').val() == '') {
        errorString += "Facility is required.\n";
    }

    //if ($('#zip_code').val() == '') {
    //    errorString += "Postal Code is required.\n";
    //}
    if (disableCSN != "yes"){
        if ($('#csn').val() == '') {
            errorString += "CSN is required.\n";
        }
    }

    if ($('#phone').val() == '') {
        errorString += "Phone Number is required.\n";
    }

    if ($('#question_type').val() == '') {
        errorString += "Question type is required.\n";
    }

    if ($('#question').val() == '') {
        errorString += "Question is required.\n";
    }
    
    if ($('#question_type').val() == 'technical'){
        if($('#severity').val() == ''){
            errorString += "Problem Severity is required.\n";   
        }
        
        if($('#product').val() == ''){
            errorString += "Product is required.\n";   
        }
        
        if($('#unit_serial').val() == ''){
            errorString += "Unit Serial is required.\n";   
        }                
        
        if($('#problem_description').val() == ''){
            errorString += "Problem Description is required.\n";   
        }
    }

    //var errors = 0;
    //errors += toggleErrorMessage(($('contactName').value == ''), $('contactNameRequired'));
    //errors += toggleErrorMessage(($('contactEmail').value == '' || !validateEmail($('contactEmail').value)), $('contactEmailRequired'));

//    if (errors == 0) {
    if (errorString == '') {
        sendOmniformRequest(btn, url);
    } else {
        alert("Validation failed: " + errorString);
//        toggleOmniForm(btn);
    }

}
