(function ($) {
    if (typeof $.telligent === 'undefined') { $.telligent = {}; }
    if (typeof $.telligent.evolution === 'undefined') { $.telligent.evolution = {}; }
    if (typeof $.telligent.evolution.widgets === 'undefined') { $.telligent.evolution.widgets = {}; }

    if (typeof $.omnicell === 'undefined') { $.omnicell = {}; }
    if (typeof $.omnicell.training === 'undefined') { $.omnicell.training = {}; }
    if (typeof $.omnicell.training.widgets === 'undefined') { $.omnicell.training.widgets = {}; }

    if (typeof $.omnicell.traininguser === 'undefined') { $.omnicell.traininguser = {}; }

    $.omnicell.traininguser.subscriptionMemberSearch = {
        register: function (context) {
            context.searchButton.click(function () {
                //alert('search clicked')
                var nonMemberValue = getParameterByName('nonmembers');
                var qs = '?nonmembers=' + nonMemberValue;
                var searchCriteriaCSNQS = '';
                var searchCriteriaEmailQS = '';

                var searchCriteriaCSN = context.searchTextboxCSN.val();

                if (searchCriteriaCSN != '') {
                    searchCriteriaCSNQS = 'csn=' + encodeURIComponent(searchCriteriaCSN);
                    if (qs == '') {
                        qs = qs + '?';
                    }
                    else {
                        qs = qs + '&';
                    }
                    qs = qs + searchCriteriaCSNQS;
                }

                var searchCriteriaEmail = context.searchTextboxEmail.val();
                if (searchCriteriaEmail != '') {
                    if (qs == '') {
                        qs = qs + '?';
                    }
                    else {
                        qs = qs + '&';
                    }
                    searchCriteriaEmailQS = 'eml=' + encodeURIComponent(searchCriteriaEmail);
                    qs = qs + searchCriteriaEmailQS;
                }

                if (qs == '') {
                    qs = qs + '?';
                }
                //alert(qs);
                window.location = qs;

                return false;
            })

            $("#memberFilter input:radio[name=memberfiltergroup]").click(function () {
                var value = $(this).val();
                //alert(value);
                window.location = '?nonmembers=' + value;
            });

            function getParameterByName(name) {
                name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
                var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
                    results = regex.exec(location.search);
                return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
            }

        }
    };

    $.omnicell.traininguser.subscriptionMember = {
        register: function (context) {
            context.exportButton.click(function () {
                window.location.href = context.exportUrl;
            })

            context.removeButton.click(function () {
                //alert('removeclick');
                var list = "";
                var i = 0;
                $('.remove-checkbox:checked').each(function () {
                    var isChecked = (this.checked ? "1" : "0");
                    var userId = $(this).attr('data-userid');
                    if (isChecked == "1") {
                        i++;
                        list += (list == "" ? userId : "," + userId);
                    }
                });
                //alert(list);
                if (list.length > 0) {
                    if (window.confirm('Remove members? ' + 'Members are about to lose view access.  Are you sure you would like to remove ' + i.toString() + ' member(s) ?')) {
                        //alert(list);
                        //alert(context.groupId + ' : ' + list);
                        //alert(context.removeUrl);
                        $.telligent.evolution.post({
                            url: context.removeUrl,
                            data: { GroupId: context.groupId, ids: list },
                            success: function (response) {
                                alert('Member(s) removed');
                                document.location.reload(true);

                            },
                            error: function (jqXHR) {
                                alert(jqXHR.status + ' ' + jqXHR.responseText);
                            }
                        });
                    }
                    return false;
                }

            })

            context.checkAllLink.click(function () {
                if (context.checkAllLink.text() == 'De-select') {
                    $('.remove-checkbox').attr('checked', false);
                    context.checkAllLink.text('Select All');
                }
                else {
                    $('.remove-checkbox').attr('checked', true);
                    context.checkAllLink.text('De-select');
                }

            })
        }
    };


    $.omnicell.traininguser.subscriptionMemberAdd = {
        register: function (context) {
            context.exportButton.click(function () {
                window.location.href = context.exportUrl;
            })

            context.addButton.click(function () {
                var list = "";
                var i = 0;
                $('.add-checkbox').each(function () {
                    var isChecked = (this.checked ? "1" : "0");
                    var userId = $(this).attr('data-userid');
                    if (isChecked == "1") {
                        i++;
                        list += (list == "" ? userId : "," + userId);
                    }
                });
                //alert(list);
                if (list.length > 0) {
                    if (window.confirm('Allow members? ' + 'Members are about to gain view access.  Are you sure you would like to Allow ' + i.toString() + ' member(s) to view ?')) {
                        //alert(list);
                        //alert(context.groupId + ' : ' + list);
                        //alert(context.removeUrl);
                        $.telligent.evolution.post({
                            url: context.addMemberUrl,
                            data: { GroupId: context.groupId, ids: list },
                            success: function (response) {
                                alert('Member(s) allowed. Members have successfully been granted access');
                                document.location.reload(true);

                            },
                            error: function (jqXHR) {
                                alert(jqXHR.status + ' ' + jqXHR.responseText);
                            }
                        });
                    }
                    return false;
                }

            })

            context.checkAllLink.click(function () {
                if (context.checkAllLink.text() == 'De-select') {
                    $('.add-checkbox').attr('checked', false);
                    context.checkAllLink.text('Select All');
                }
                else {
                    $('.add-checkbox').attr('checked', true);
                    context.checkAllLink.text('De-select');
                }
            })
        }
    };
}
(jQuery));