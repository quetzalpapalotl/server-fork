(function($) {

	var deletePost = function(context) {
		if(confirm(context.deleteConfirmText)) {
			var url, successUrl, data = {
				ForumId: context.forumId,
				ThreadId: context.threadId,
				DeleteChildren: $(context.deleteChildrenInput).is(':checked'),
				SendAuthorDeleteNotification: $(context.sendAuthorDeleteNotificationInput).is(':checked'),
				DeleteReason: $(context.deleteReasonInput).val()
			};
			if(context.replyIsThreadStarter) {
				url = context.deleteThreadUrl;
				successUrl = context.forumUrl;
			} else {
				url = context.deleteReplyUrl;
				data.ReplyId = context.replyId;
				successUrl = context.threadUrl;
			}
			$.telligent.evolution.del({
				url: url,
				data: data,
				success: function(response) {
					window.location = successUrl;
				}
			});
		}
	};

	var api = {
		register: function(context) {

			context.deleteButton = $(context.deleteButton)
				.evolutionValidation({
					onValidated: function(isValid, buttonClicked, c) { },
					onSuccessfulClick: function(e) {
						e.preventDefault();
						e.stopPropagation();
						deletePost(context);
					}
				})
				.evolutionValidation('addField',$(context.deleteReasonInput), {
					required: true,
					messages: { required: context.deleteReasonValidationText }
				}, $(context.deleteReasonInput).closest('.field-item').find('.field-item-validation'), null);
			$(context.reasonTemplate).change(function() {
				var data = {
					TemplateID: $(this).val()
				};
				$.telligent.evolution.get({
					url: context.reasonTemplateUrl,
					data: data,
					dataType: 'json',
					success: function(response) {
						$(context.deleteReasonInput).val(response.template);
					}
				});
			});
		}
	};

	// expose api publicly
	if (typeof $.telligent === 'undefined') { $.telligent = {}; }
	if (typeof $.telligent.evolution === 'undefined') { $.telligent.evolution = {}; }
	if (typeof $.telligent.evolution.widgets === 'undefined') { $.telligent.evolution.widgets = {}; }
	$.telligent.evolution.widgets.deletePost = api;

})(jQuery);
