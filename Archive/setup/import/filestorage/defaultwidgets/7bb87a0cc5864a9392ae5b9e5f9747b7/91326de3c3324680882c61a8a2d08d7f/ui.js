(function($) {

	var unfavorite = function(context, contentId, favoriteItem) {
		$.telligent.evolution.post({
			url: context.unfavoriteUrl,
			data: {
				contentId: contentId
			},
			success: function(response) {
				favoriteItem.fadeOut(200, function(){
					favoriteItem.remove();
				});
			}
		});
	};

	var api = {
		register: function(context) {
			$(context.wrapperId + ' a.favorite-on').live('click', function(e,data){
				var favoriteItem = $(this).closest('li.content-item');
				var link = favoriteItem.find('a.view-post');
				var contentId = link.attr('data-contentid');
				unfavorite(context, contentId, favoriteItem);
				return false;
			});
		}
	};

	if (typeof $.telligent === 'undefined') { $.telligent = {}; }
	if (typeof $.telligent.evolution === 'undefined') { $.telligent.evolution = {}; }
	if (typeof $.telligent.evolution.widgets === 'undefined') { $.telligent.evolution.widgets = {}; }
	$.telligent.evolution.widgets.favorableContentList = api;

}(jQuery));
