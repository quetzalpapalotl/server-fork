(function($) {
	if (typeof $.telligent === 'undefined') { $.telligent = {}; }
	if (typeof $.telligent.evolution === 'undefined') { $.telligent.evolution = {}; }
	if (typeof $.telligent.evolution.widgets === 'undefined') { $.telligent.evolution.widgets = {}; }

	var acceptFriendship = function(recipientId, requestorId) {
			var data = {
				RequestorId: requestorId,
				RequesteeId: recipientId,
				FriendshipState: 'Approved'
			};

			$.telligent.evolution.put({
				url: $.telligent.evolution.site.getBaseUrl() + 'api.ashx/v2/users/{RequestorId}/friends/{RequesteeId}.json',
				data: data,
				success: function(response) {
					reloadPage();
				}
			});
		},
		deleteFriendship = function(recipientId, requestorId){
			$.telligent.evolution.del({
				url: $.telligent.evolution.site.getBaseUrl() + 'api.ashx/v2/users/{RequestorId}/friends/{RequesteeId}.json',
				data: {RequestorId: requestorId, RequesteeId: recipientId},
				success: function(response) {
					reloadPage();
				}
			});
		},
		reloadPage = function() {
			window.location = window.location;
		};

	$.telligent.evolution.widgets.friendshipRequestList = {
		register: function(context) {
			$(context.wrapperSelector)
				.find('.internal-link.deny-friend-request')
				.click(function(){
					var p = $(this).parents('.content-item');
					if (p.length > 0) {
						var id = p.attr('data-requestor-id');
						deleteFriendship(context.userId, id);
					}
					return false;
				});

			$(context.wrapperSelector)
				.find('.internal-link.approve-friend-request')
				.click(function(){
					var p = $(this).parents('.content-item');
					if (p.length > 0) {
						var id = p.attr('data-requestor-id');
						acceptFriendship(context.userId, id);
					}
					return false;
				});
		}
	};
}(jQuery));
