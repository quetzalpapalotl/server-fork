(function($)
{
	if (typeof $.telligent === 'undefined') { $.telligent = {}; }
	if (typeof $.telligent.evolution === 'undefined') { $.telligent.evolution = {}; }
	if (typeof $.telligent.evolution.widgets === 'undefined') { $.telligent.evolution.widgets = {}; }

	var _createFile = function(context)
		{
			$.telligent.evolution.post({
				url: context.createUrl,
				data: { fileName:context.fileName, userId:context.userId, contextId:context.contextId, folderName:context.parentFolderPath },
				dataType: 'json',
				defaultErrorMessage: context.createFileErrorMessage,
				success: function(response)
				{
					window.location = context.viewParentFolderUrl;
				}
			});
		};

	$.telligent.evolution.widgets.addUserFile =
	{
		register: function(context)
		{
			$(context.fileUploadContainerId).glowUpload({
				hiddenInput: context.uploadFileStateId,
				fileFilter: null,
				uploadUrl: context.uploadFileUrl
			})
			.bind('glowUploadComplete', function(e, file)
				{
					if (file && file.name.length > 0)
					{
						context.fileName = file.name;
						context.saveFileButton.removeAttr('disabled');
					}
				});
				
			context.saveFileButton.click(function(e)
			{
				_createFile(context);
			});
		}
	};
})(jQuery);
