(function($)
{
    if (typeof $.telligent === 'undefined')
            $.telligent = {};

    if (typeof $.telligent.evolution === 'undefined')
        $.telligent.evolution = {};

    if (typeof $.telligent.evolution.widgets === 'undefined')
        $.telligent.evolution.widgets = {};

    var showReply = function(context, replyId, isThread, replyButton, replyWrapper, moderatedWrapper, focusInput)
    {
        $('.field-item-input.processing', replyWrapper).removeClass('processing');
        replyWrapper.show();
        replyButton.hide();
        moderatedWrapper.hide();
        var replyInput = $('textarea:last', replyWrapper);
        replyInput.one('focus', function(){
            replyInput.evolutionComposer({
                plugins: ['mentions','hashtags']
            }).evolutionComposer('val','');
        });

        if (focusInput)
            replyInput.focus().focus();

        var replyBound = $(replyWrapper).data('replyBound');
        if(!replyBound) {
            $(replyWrapper).data('replyBound', true);

            $('.cancel-reply', replyWrapper).click(function()
            {
                hideReply(replyButton, replyWrapper);
            });

            $('.add-post', replyWrapper).click(function()
            {
                addAdvancedReply(context, replyId, isThread, replyWrapper, moderatedWrapper);
            });

            var validationItem = $('.field-item-validation', replyWrapper);

            $('.add-inline-post', replyWrapper).evolutionValidation(
            {
                validateOnLoad: false,
                onSuccessfulClick: function(e)
                {
                    addReply(context, replyWrapper, replyButton, moderatedWrapper);
                }
            }).evolutionValidation('addField', $('textarea:last', replyWrapper),
            {
                required: true,
                messages: { required: context.replyBodyRequiredText }
            }, validationItem, null);
        }
    },
    hideReply = function(replyButton, replyWrapper)
    {
        replyWrapper.hide("fast");
        replyButton.show();
    },
    addAdvancedReply = function(context, replyId, isThread, replyWrapper)
    {
        var data = {
            replyId: replyId,
            isThread: isThread ? 'True' : 'False',
            threadId: context.threadId,
            replyBody: $('textarea', replyWrapper).evolutionComposer('val') || '',
            isSuggestion: !context.isQuestion || replyWrapper.find(':checkbox').is(':checked')
        };

        $.telligent.evolution.post({
            url: context.advancedReplyUrl,
            data: data,
            dataType: 'json',
            success: function(response)
            {
                window.location = response.replyUrl;
            }
        });
    },
    addReply = function(context, replyWrapper, replyButton, moderatedWrapper)
    {
        var p = $(replyWrapper).parents('.full-post-container');
        var parentReplyId = 0;
        if (p.length > 0 && p.attr('data-thread-starter') !== 'true') {
            parentReplyId = p.attr('data-reply-id');
        }

        var data = {
            replyBody: $('textarea:last', replyWrapper).evolutionComposer('val'),
            threadId:context.threadId,
            isSuggestion: !context.isQuestion || !$('input:checkbox', replyWrapper).is(':checked') ? 'False' : 'True',
            parentReplyId: parentReplyId
        };

        $('.field-item-input', replyWrapper).addClass('processing');

        $.telligent.evolution.post({
            url: context.replyUrl,
            data: data,
            dataType: 'json',
            success: function(response)
            {
                if (response.isApproved == "false")
                {
                    moderatedWrapper.show();
                    $('.add-inline-post', replyWrapper).evolutionValidation('reset');
                    window.setTimeout(function() { hideReply(replyButton, replyWrapper); }, 999);
                    window.setTimeout(function() { moderatedWrapper.hide(); }, 19999);
                }
                else if (response.replyUrl && response.replyUrl.length > 0)
                    window.location = response.replyUrl;
                else
                   window.location = context.threadReplyUrl;
            },
            error: function(xhr, desc)
            {
                $.telligent.evolution.notifications.show(desc,{type:'error'});
                $('.field-item-input', replyWrapper).removeClass('processing');
            }
        });
    },
    changeAnswerStatus = function(context, replyId, shouldMarkAnswer)
    {
        $.telligent.evolution.post({
            url: context.markAnswerUrl,
            data: { replyId: replyId, threadId: context.threadId, forumId: context.forumId, shouldMarkAnswer: shouldMarkAnswer ? 'True' : 'False' },
            dataType: 'json',
            success: function(response)
            {
                reloadPage();
            }
        });
    },
    toggleLock = function(context)
    {
        $.telligent.evolution.post({
            url: context.toggleLockUrl,
            data: { threadId: context.threadId, forumId: context.forumId },
            dataType: 'json',
            success: function(response)
            {
                if (response.isSuccess)
                    reloadPage();
                else
                    alert(context.toggleLockErrorMessage);
            }
        });
    },
    toggleUserModeration = function(context, userId)
    {
        $.telligent.evolution.post({
            url: context.toggleUserModerationUrl,
            data: { userId: userId },
            dataType: 'json',
            success: function(response)
            {
                if (response.isSuccess)
                    reloadPage();
                else
                    alert(context.toggleUserModerationErrorMessage);
            }
        });
    },
    deletePageAssociation = function(context)
    {
        if (!confirm(context.deleteWikiConfirmationText))
            return false;

        $.telligent.evolution.post({
            url: context.deletePageAssociationUrl,
            data: { threadId: context.threadId },
            dataType: 'json',
            success: function(response)
            {
                reloadPage();
            }
        });
    },
    reloadPage = function()
    {
        window.location.reload(true);
    };

    $.telligent.evolution.widgets.thread =
    {
        register: function(context)
        {
            $('textarea').evolutionResize();

            var wrapper = $(context.wrapperSelector);

            $(context.removeCaptureButtonSelector).click(function()
            {
                deletePageAssociation(context);
                return false;
            });

            $('.internal-link.not-answer', wrapper).click(function ()
            {
                var p = $(this).parents('.full-post-container');
                if (p.length > 0)
                {
                    var id = p.attr('data-reply-id');
                    changeAnswerStatus(context,  id, false);
                }
            });

            $('.internal-link.verify-answer', wrapper).click(function ()
            {
                var p = $(this).parents('.full-post-container');
                if (p.length > 0)
                {
                    var id = p.attr('data-reply-id');
                    changeAnswerStatus(context, id, true);
                }
            });

            if (context.showInlineReplies)
            {
                $('.reply-wrapper', wrapper).each(function()
                {
                    var replyWrapper = $(this);
                    var showReplyForm = $(this).attr('data-show-reply') == "true";
                    var p = $(this).parents('.full-post-container');
                    if (p.length > 0)
                    {
                        var id = p.attr('data-reply-id');
                        var isThread = p.attr('data-thread-starter') == "true";
                        var inlineReplyButton = $('.inline-reply-button', p);

                        if (showReplyForm)
                            showReply(context, id, isThread, inlineReplyButton, replyWrapper, $('.moderated-message', p), false);

                        inlineReplyButton.click(function ()
                        {
                            showReply(context, id, isThread, inlineReplyButton, replyWrapper, $('.moderated-message', p), true);
                        });
                    }
                });
            }

            $('.full-post-container', wrapper).bind('evolutionModerateLinkClicked', function(e, link) {
                var container = $(this),
                    link = $(link);
                if(link.hasClass('toggleLock')) {
                    toggleLock(context);
                } else if(link.hasClass('toggleUserModeration')) {
                    toggleUserModeration(context, container.data('authorid'));
                }
            });
        }
    };
})(jQuery);
