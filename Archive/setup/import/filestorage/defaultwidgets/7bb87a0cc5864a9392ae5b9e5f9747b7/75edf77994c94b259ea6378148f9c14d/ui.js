(function($)
{
    if (typeof $.telligent === 'undefined') { $.telligent = {}; }
    if (typeof $.telligent.evolution === 'undefined') { $.telligent.evolution = {}; }
    if (typeof $.telligent.evolution.widgets === 'undefined') { $.telligent.evolution.widgets = {}; }

    var _attachHandlers = function(context) {
        $('#' + context.fileUploadId).click(function() {
            $.glowModal(context.uploadFileUrl, { width:500, height:300, onClose:function(returnValue){_modalClosed(context, returnValue);}});
            return false;
        });

        var selectTagsBox = $('#' + context.selectTagsId).click(function() {
            $('#' + context.tagBoxId).evolutionTagTextBox('openTagSelector'); return false;
        });
        if (!context.tags || context.tags.length === 0) {
            selectTagsBox.hide();
        }

        $('#'+ context.featuredImageUrl).evolutionUserFileTextBox({
            removeText: context.removeResource,
            selectText: context.selectUploadResource,
            noFileText: context.noFileSelectedResource,
            initialPreviewHtml: context.featuredImagePreview
        });

        $('#' + context.featuredPostId).click(function() {
            if(this.checked) {
                $('#' + context.featuredImageId).show();
            } else {
                $('#' + context.featuredImageId).hide();
            }
        });

        $.telligent.evolution.navigationConfirmation.enable();
        var saveButton = $('#' + context.wrapperId + ' a.save-post');
        saveButton.click(function()
        {
            if (!$(this).evolutionValidation('isValid')) {
                return;
            }

            _save(context);
            return false;
        });
        $.telligent.evolution.navigationConfirmation.enable();
        $.telligent.evolution.navigationConfirmation.register(saveButton);
    },
    _addValidation = function(context) {
        var saveButton = $('#' + context.wrapperId + ' a.save-post');

        saveButton.evolutionValidation({
            validateOnLoad: context.mediaId <= 0 ? false : null,
            onValidated: function(isValid, buttonClicked, c) {
                if (isValid) {
                    saveButton.removeClass('disabled');
                } else {
                    saveButton.addClass('disabled');
                    var tabbedPane = $('#' + context.tabId).glowTabbedPanes('getByIndex', 0);
                    if (tabbedPane) {
                        $('#' + context.tabId).glowTabbedPanes('selected', tabbedPane);
                    }
                }
            },
            onSuccessfulClick: function(e) {
                saveButton.parent().addClass('processing');
                saveButton.addClass('disabled');
            }
        });

        // File uploaded
        saveButton.evolutionValidation('addCustomValidation', 'mediafileuploaded', function()
            {
                return context.fileUploaded;
            },
            context.noFileUploadedError,
            '#' + context.wrapperId + ' .field-item.post-attachment .field-item-validation',
            null
        );

        // Has name
        saveButton.evolutionValidation('addField', '#' + context.postNameId,
            {
                required: true,
                messages: { required: context.postNameMissing }
            },
            '#' + context.wrapperId + ' .field-item.post-name .field-item-validation',
            null
        );
    },
    _modalClosed = function(context, file) {
        if(file) {
            $('#' + context.fileNameId).text(file.fileName);
            context.fileUploaded = true;
            context.file = file;
            $('#' + context.wrapperId + ' a.save-post').evolutionValidation('validateCustom', 'mediafileuploaded');
        }
    },
    _save = function(context) {
        var data = _createPostRequestData(context);

        $.telligent.evolution.post({
            url: context.saveUrl,
            data: data,
            success: function(response)
            {
                if (response.message) {
                    alert(response.message);
                }

                if (response.redirectUrl) {
                    window.location = response.redirectUrl;
                }
            },
            defaultErrorMessage: context.saveErrorText,
            error: function(xhr, desc, ex)
            {
                $.telligent.evolution.notifications.show(desc,{type:'error'});
                $('#' + context.wrapperId + ' a.save-post').parent().removeClass('processing');
                $('#' + context.wrapperId + ' a.save-post').removeClass('disabled');
            }
        });
    },
    _createPostRequestData = function(context) {
        var inTags = $('#' + context.tagBoxId).val().split(/[,;]/g);
        var tags = [];
        for (var i = 0; i < inTags.length; i++)
        {
            var tag = $.trim(inTags[i]);
            if (tag) {
                tags[tags.length] = tag;
            }
        }
        tags = tags.join(',');

        var data = {
            Title: $('#' + context.postNameId).evolutionComposer('val'),
            Body: context.getBody(),
            Tags: tags,
            GalleryId: context.galleryId,
            FileChanged: context.file ? '1' : '0'
        };

        var subscribe = $('#' + context.subscribeId);
        if (subscribe.length > 0)
            data.Subscribe = subscribe.is(':checked') ? 1 : 0;
        else
            data.Subscribe = -1;

        var featured = $('#' + context.featuredPostId);
        if (featured.length > 0)
        {
            data.Featured = featured.is(':checked') ? 1 : 0;
            var featuredImage = $('#'+ context.featuredImageUrl);
            if (featuredImage.length > 0)
                data.FeaturedImageUrl = featuredImage.val();
        }
        else
            data.Featured = -1;

        if (context.file)
        {
            if (context.file.isRemote)
            {
                data.FileName = context.file.fileName;
                data.FileUrl = context.file.url;
                data.FileIsRemote = '1';
            }
            else
            {
                data.FileName = context.file.fileName;
                data.FileContextId = context.file.contextId;
                data.FileIsRemote = '0';
            }
        }

        if (context.mediaId > 0)
        {
            data.Id = context.mediaId;
        }

        return data;
    },
    _preview = function(context) {
        var data = _createPostRequestData(context);

        $('#' + context.previewTabId).html('<div class="message loading loading__message">' + context.previewLoadingText + '</div>');

        $.telligent.evolution.post({
            url: context.previewUrl,
            data: data,
            success: function(response)
            {
                $('#' + context.previewTabId).hide().html(response).fadeIn('fast');
            },
            defaultErrorMessage: context.previewErrorText,
            error: function(xhr, desc, ex)
            {
                $('#' + context.previewTabId).html('<div class="message error error__message">' + desc + '</div>');
            }
        });
    };

    $.telligent.evolution.widgets.uploadEditMediaGalleryPost = {
        register: function(context) {
            $('#' + context.tabId).glowTabbedPanes({
                cssClass:'tab-pane',
                tabSetCssClass:'tab-set with-panes',
                tabCssClasses:['tab'],
                tabSelectedCssClasses:['tab selected'],
                tabHoverCssClasses:['tab hover'],
                enableResizing:false,
                tabs:
                [
                    [context.composeTabId, context.composeTabText, null],
                    [context.previewTabId, context.previewTabText, function() { _preview(context); }]
                ]
            });

            $('textarea').evolutionResize();

            $('#' + context.postNameId).evolutionComposer({
                plugins: ['hashtags']
            }).evolutionComposer('onkeydown', function(e) {
                if(e.which === 13) {
                    return false;
                } else {
                    return true;
                }
            });

            $('#' + context.tagBoxId).evolutionTagTextBox({allTags:context.tags});

            _attachHandlers(context);
            _addValidation(context);
        }
    };
})(jQuery);
