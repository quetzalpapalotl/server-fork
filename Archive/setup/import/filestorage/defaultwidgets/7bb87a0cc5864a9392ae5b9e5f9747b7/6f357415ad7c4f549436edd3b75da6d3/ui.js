jQuery(function($){
    if (typeof $.telligent === 'undefined')
            $.telligent = {};

    if (typeof $.telligent.evolution === 'undefined')
        $.telligent.evolution = {};

    if (typeof $.telligent.evolution.widgets === 'undefined')
        $.telligent.evolution.widgets = {};

    var _errorHtml = '<div class="message error">{ErrorText}</div>',
        _loadingHtml = '<div class="message loading">{LoadingText}</div>',
        _load = function(context, rebasePager) {
            var data = { w_baseUrl: context.baseUrl };
            if(rebasePager) {
                data[context.pageIndexQueryStringKey] = 1;

                var hashData = $.telligent.evolution.url.hashData();
                hashData[context.pageIndexQueryStringKey] = 1;
                $.telligent.evolution.url.hashData(hashData);
            }
            _setContent(context, _loadingHtml.replace(/{LoadingText}/g, context.loadingText));
            $.telligent.evolution.get({
                url: context.loadCommentsUrl,
                data: data,
                success: function(response) {
                    if(response) {
                        _setContent(context, response);
                    }
                },
                defaultErrorMessage: context.errorText,
                error: function(xhr, desc, ex) {
                    _setContent(context, _errorHtml.replace(/{ErrorText}/g, desc));
                }
            });
        },
        _attachHandlers = function(context)
        {
            $(context.wrapper).bind('evolutionModerateLinkClicked',function(e, targetLink) {
                var moderater = $(e.target);
                targetLink = $(targetLink);
                var commentId = moderater.closest('.content-item').data('commentid');
                if(targetLink.hasClass('delete-post')) {
                    _deleteWikiComment(context, context.wikiId, context.pageId, commentId, e);
                } else if (targetLink.hasClass('edit-post')) {
                    var editCommentUrl = moderater.closest('.content-item').data('editurl');
                    Telligent_Modal.Open(editCommentUrl, 500, 400, null);
                }
                return false;
            });
        }
        _setContent = function(context, html) {
            context.wrapper.html(html).css("visibility", "visible");
        },
        _deleteWikiComment = function(context, wikiId, pageId, commentId, event) {
            if(confirm(context.deleteVerificationText)) {
                $.telligent.evolution.del({
                    url: $.telligent.evolution.site.getBaseUrl() + 'api.ashx/v2/wikis/{WikiId}/pages/{WikiPageId}/comments/{CommentId}.json',
                    data: {
                        WikiId: wikiId,
                        WikiPageId: pageId,
                        CommentId: commentId
                    },
                    success: function(response) {
                        var remainingItems = $(event.target).closest('ul').find('li.content-item');
                        $('a[name=comment-'+commentId+']', context.wrapper)
                            .parents('li.content-item')
                            .slideUp(function() {
                                $('a[name=comment-'+commentId+']', context.wrapper)
                                    .parents('li.content-item').remove();
                                // if there were no more comments, hide the comments list altogether
                                if(context.wrapper.find('li').length === 0) {
                                    context.wrapper.css("visibility", "hidden");
                                }
                                _load(context, remainingItems.length === 1);
                            });
                    }
                });
            }
        };

    $.telligent.evolution.widgets.commentList = {
        register: function(context) {
            _attachHandlers(context);

            $(document).bind('telligent_wikis_commentposted', function(e, message) {
                _load(context);
            });
        }
    };
});