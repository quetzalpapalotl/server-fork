(function($)
{
	if (typeof $.telligent === 'undefined') { $.telligent = {}; }
	if (typeof $.telligent.evolution === 'undefined') { $.telligent.evolution = {}; }
	if (typeof $.telligent.evolution.widgets === 'undefined') { $.telligent.evolution.widgets = {}; }

	var _deleteFolder = function(context, folderPath)
		{
			$.telligent.evolution.post({
				url: context.deleteUrl,
				data: { folderPath:folderPath, userId:context.userId },
				dataType: 'json',
				success: function(response)
				{
					window.location = context.viewFolderUrl;
				}
			});
		};

	$.telligent.evolution.widgets.userFolderList =
	{
		register: function(context)
		{
			var w = $('#' + context.wrapperId);
			$(context.deleteFolderSelector, w).each(function(index, e)
			{
				$(e).click(function()
				{
					var folderPath = $('input', $(this).parent()).val();
					if (confirm(context.deleteFolderConfirmation))
						_deleteFolder(context, folderPath);
						
					return false;
				});
			});
		}
	};
})(jQuery);
