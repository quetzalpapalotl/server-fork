(function($)
{
	if (typeof $.telligent === 'undefined') { $.telligent = {}; }
	if (typeof $.telligent.evolution === 'undefined') { $.telligent.evolution = {}; }
	if (typeof $.telligent.evolution.widgets === 'undefined') { $.telligent.evolution.widgets = {}; }

	var _createInvite = function(context)
		{
			var data = { 
					emailAddress:context.emailInput.val(),
					message:context.messageInput.val()
				};
			
			$.telligent.evolution.post({
				url: context.createUrl,
				data: data,
				dataType: 'json',
				success: function(response)
				{
					$(context.successMessageContainer).show();
				},
				defaultErrorMessage: context.createErrorMessage,
				error: function(xhr, desc, ex) 
				{ 
					$.telligent.evolution.notifications.show(desc,{type:'error'});
					context.sendButton.removeAttr('disabled');
				}
			});
		};

	$.telligent.evolution.widgets.inviteUser =
	{
		register: function(context)
		{
			context.sendButton.evolutionValidation(
			{
				validateOnLoad : false,
				onValidated: function(isValid, buttonClicked, c) 
				{ 
					if (isValid) 
						context.sendButton.removeAttr('disabled'); 
					else
						context.sendButton.attr('disabled', 'disabled');
				},
				onSuccessfulClick: function(e) 
				{
					context.sendButton.attr('disabled', 'disabled');
					_createInvite(context);
				}
			}).evolutionValidation('addField', context.emailInput,	
			{ 
				required: true,
				emails: true,
				messages: 
				{ 
					required: context.emailRequiredText,
					emails: context.emailRequiredText
				} 
			}, '#' + context.wrapperId + ' .field-item.email-address .field-item-validation', null);
		}
	};
})(jQuery);
