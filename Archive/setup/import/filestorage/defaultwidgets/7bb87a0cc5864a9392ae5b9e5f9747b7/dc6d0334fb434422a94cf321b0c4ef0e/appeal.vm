#set ($isAnonymous = false)
#if (!$appeal.AuthorUser || !$core_v2_user.IsRegistered($appeal.AuthorUser.Id))
	#set ($isAnonymous = true)
#end

<li data-appealid="$appeal.AppealId" class="content-item appeal #if($shouldExpand) expanded #end">

	<div class="abbreviated-post-header">
		$core_v2_language.GetResource('AppealNotice')
	</div>

	<div class="abbreviated-post">
		<a class="expander" href="#"><span></span>$core_v2_language.GetResource('expand')</a>

		## author attributes
		<div class="attribute-list-header"></div>
		<ul class="attribute-list author">
		#if ($isAnonymous)
			<li class="attribute-item author">
				$core_v2_language.GetResource('Account_Deleted')
			</li>
		#else
			<li class="attribute-item author">
				#if ($appeal.AuthorUser)
					<div class="post-author">
						<span class="avatar">
							#if ($appeal.AuthorUser.ProfileUrl)
								<a href="$core_v2_encoding.HtmlAttributeEncode($appeal.AuthorUser.ProfileUrl)">
									$core_v2_ui.GetResizedImageHtml($appeal.AuthorUser.AvatarUrl,32,32,"%{alt=$appeal.AuthorUser.DisplayName,style='border-width:0;'}")
								</a>
							#else
								$core_v2_ui.GetResizedImageHtml($appeal.AuthorUser.AvatarUrl,32,32,"%{alt=$appeal.AuthorUser.DisplayName,style='border-width:0;'}")
							#end
						</span>
						<span class="user-name">
							#if($appeal.AuthorUser.ProfileUrl)
								<a href="$core_v2_encoding.HtmlAttributeEncode($appeal.AuthorUser.ProfileUrl)" class="internal-link view-user-profile">
									<span></span>$appeal.AuthorUser.DisplayName
								</a>
							#else
								<span></span>$appeal.AuthorUser.DisplayName
							#end
						</span>
					</div>
				#end
				<div class="post-date">
					#if ($appeal.AuthorResponseDate)
						$core_v2_language.FormatAgoDate($appeal.AuthorResponseDate)
					#end
				</div>
			</li>

			#if($shouldExpand)

				## join date
				#if ($appeal.AuthorUser.JoinDate)
					<li class="attribute-item join-date">
						<span class="attribute-name">$core_v2_language.GetResource('MemberSince')</span>
						<span class="attribute-value">$core_v2_language.FormatDate($appeal.AuthorUser.JoinDate)</span>
					</li>
				#end

				## roles
				#set ($userRoles = $core_v2_role.List("%{ Include = 'user', UserId = $appeal.AuthorUser.Id, PageIndex = 0, PageSize = 20 }"))
				#foreach ($role in $userRoles)
				#beforeall
					<li class="attribute-item roles">
						<span class="attribute-name">$core_v2_language.GetResource('CurrentRoles')</span>
						<span class="attribute-value">
				#each
					#if ($role && (!$role.GroupId || $role.GroupId == 0))
						$role.Name,&nbsp;
					#end
				#afterall
						</span>
					</li>
				#end

				#set($contents = $core_v2_abusiveContent.List("%{ AuthorUserId = $appeal.AuthorUser.ContentId, PageSize = 10, PageIndex = 0 }"))

				## count of abusive content in various states
				#set($reportedAbusiveContent = $core_v2_abusiveContent.List("%{ AbuseState = 'Reported', AuthorUserId = $appeal.AuthorUser.ContentId, PageSize = 1, PageIndex = 0 }"))
				#set($appealedAbusiveContent = $core_v2_abusiveContent.List("%{ AppealState = 'Initiated', AuthorUserId = $appeal.AuthorUser.ContentId, PageSize = 1, PageIndex = 0 }"))
				#set($appealResponsedAbusiveContent = $core_v2_abusiveContent.List("%{ AppealState = 'AuthorResponded', AuthorUserId = $appeal.AuthorUser.ContentId, PageSize = 1, PageIndex = 0 }"))
				#set($confirmedAbusiveContent = $core_v2_abusiveContent.List("%{ AppealState = 'Rejected', AuthorUserId = $appeal.AuthorUser.ContentId, PageSize = 1, PageIndex = 0 }"))
				#set($expiredAbusiveContent = $core_v2_abusiveContent.List("%{ AppealState = 'Expired', AuthorUserId = $appeal.AuthorUser.ContentId, PageSize = 1, PageIndex = 0 }"))
				#set($notAbusiveContent = $core_v2_abusiveContent.List("%{ AbuseState = 'NotAbusive', AuthorUserId = $appeal.AuthorUser.ContentId, PageSize = 1, PageIndex = 0 }"))

				## counts of abuse appeals in various states
				#set ($initiatedAppeals = $core_v2_abuseAppeal.List("%{ AppealState = 'Initiated', ContentAuthorId = $appeal.AuthorUser.ContentId, PageSize = 1, PageIndex = 0	}"))
				#set ($acceptedAppeals = $core_v2_abuseAppeal.List("%{ AppealState = 'Accepted', ContentAuthorId = $appeal.AuthorUser.ContentId, PageSize = 1, PageIndex = 0  }"))
				#set ($rejectedAppeals = $core_v2_abuseAppeal.List("%{ AppealState = 'Rejected', ContentAuthorId = $appeal.AuthorUser.ContentId, PageSize = 1, PageIndex = 0  }"))
				#set ($expiredAppeals = $core_v2_abuseAppeal.List("%{ AppealState = 'Expired', ContentAuthorId = $appeal.AuthorUser.ContentId, PageSize = 1, PageIndex = 0	}"))
				#set ($authorResponseAppeals = $core_v2_abuseAppeal.List("%{ AppealState = 'AuthorResponded', ContentAuthorId = $appeal.AuthorUser.ContentId, PageSize = 1, PageIndex = 0  }"))

				#set ($totalCommentCount = 0)
				#set ($allComments = $core_v2_comments.List("%{ PageIndex = 0, PageSize = 1, UserId = $appeal.AuthorUser.Id }"))
				#set ($totalCommentCount = $allComments.TotalCount)
				#set ($totalPostCount = $appeal.AuthorUser.TotalPosts + $totalCommentCount)

				<li class="attribute-item posts">
					<span class="attribute-name">$core_v2_language.GetResource('Posts')</span>
					<span class="attribute-value">$totalPostCount</span>
				</li>

				#set ($contentWithFlags = $reportedAbusiveContent.TotalCount + $appealedAbusiveContent.TotalCount + $expiredAbusiveContent.TotalCount + $confirmedAbusiveContent.TotalCount + $appealResponsedAbusiveContent.TotalCount)
				<li class="attribute-item reported">
					<span class="attribute-name">$core_v2_language.GetResource('PostsWithFlags')</span>
					<span class="attribute-value">$contentWithFlags</span>
				</li>

				#set ($contentHidden = $initiatedAppeals.TotalCount + $rejectedAppeals.TotalCount + $expiredAppeals.TotalCount + $authorResponseAppeals.TotalCount)
				<li class="attribute-item hidden">
					<span class="attribute-name">$core_v2_language.GetResource('PostsHidden')</span>
					<span class="attribute-value">$contentHidden</span>
				</li>

				#set ($appealCount = $acceptedAppeals.TotalCount + $rejectedAppeals.TotalCount + $expiredAppeals.TotalCount + $authorResponseAppeals.TotalCount)
				<li class="attribute-item appeals">
					<span class="attribute-name">$core_v2_language.GetResource('Appeals')</span>
					<span class="attribute-value">$appealCount</span>
				</li>

				#set ($respondedAppealCount = $authorResponseAppeals.TotalCount)
				<li class="attribute-item responded">
					<span class="attribute-name">$core_v2_language.GetResource('AppealsOpen')</span>
					<span class="attribute-value">$respondedAppealCount</span>
				</li>

				#set ($acceptedAppealCount = $acceptedAppeals.TotalCount)
				<li class="attribute-item accepted">
					<span class="attribute-name">$core_v2_language.GetResource('AppealsAccepted')</span>
					<span class="attribute-value">$acceptedAppealCount</span>
				</li>
			#end
		#end
		</ul>
		<div class="attribute-list-footer"></div>


		## content attributes
		<div class="attribute-list-header"></div>
		<ul class="attribute-list content">
			<li class="attribute-item hidden-date">
				<span class="attribute-name">$core_v2_language.GetResource('ContentHidden')</span>
				<span class="attribute-value">$core_v2_language.FormatDateAndTime($appeal.AppealInitiatedDate)</span>
			</li>
			<li class="attribute-item created-date">
				<span class="attribute-name">$core_v2_language.GetResource('ContentPosted')</span>
				<span class="attribute-value">$core_v2_language.FormatDateAndTime($appeal.Content.CreatedDate)</span>
			</li>

			#if($shouldExpand)

				#set ($reports = $core_v2_abuseReport.List("%{ AbusiveContentId = $appeal.ContentId, Appealid = $appeal.AppealId, PageIndex = 0, PageSize = 20 }"))
				#foreach ($report in $reports)
				#beforeall
					<li class="attribute-item reported">
						<span class="attribute-name">$core_v2_language.GetResource('ContentFlaggedBy')</span>
						<span class="attribute-value">

							<div class="navigation-list-header"></div>
							<ul class="navigation-list">
				#each
							<li class="navigation-item">
								<span class="date">$core_v2_language.FormatDate($report.CreatedDate)</span>
								#if ($report.CreatedUser)
									<span class="user-name">
										#if($report.CreatedUser.ProfileUrl)
											<a href="$core_v2_encoding.HtmlAttributeEncode($report.CreatedUser.ProfileUrl)" class="internal-link view-user-profile">
												<span></span>$report.CreatedUser.DisplayName
											</a>
										#else
											<span></span>$report.CreatedUser.DisplayName
										#end
									</span>
								#end
							</li>
				#afterall
							<li class="navigation-item">
								#if ($reports.TotalCount > 20)
									#set ($difference = $reports.TotalCount - 20)
									$core_v2_language.FormatString($core_v2_language.GetResource('And_More'), difference)
								#end
							</li>
							</ul>
							<div class="navigation-list-footer"></div>
						</span>
						</li>
				#end
				#if ($appeal.Content.Application.Container)
					<li class="attribute-item container">
						<span class="attribute-name">$core_v2_language.GetResource('ContentInGroup')</span>
						<span class="attribute-value">$!appeal.Content.Application.Container.HtmlName('Web')</span>
					</li>
				#end

			#end
		</ul>
		<div class="attribute-list-footer"></div>

		## appeal content
		<div class="post-content appeal">
			#if ($appeal.AppealState == 'Initiated')
				<span class="message">
					$core_v2_language.FormatString($core_v2_language.GetResource('InitiatedMessage'), $core_v2_language.FormatDateAndTime($appeal.AppealExpires))
				</span>
			#elseif($appeal.AppealState == 'Expired')
				<span class="message">
					$core_v2_language.GetResource('ExpiredMessage')
				</span>
			#elseif($appeal.AppealState == 'Accepted')
				<span class="message">
					$core_v2_language.GetResource('AcceptedMessage')
				</span>
			#elseif($appeal.AppealState == 'Rejected')
				<span class="message">
					$core_v2_language.GetResource('RejectedMessage')
				</span>
			#end
			#if ($appeal.AppealState != 'Initiated')
				<span class="response heading">
					$core_v2_language.FormatString($core_v2_language.GetResource('ResponseHeading'), $appeal.AuthorUser.DisplayName)
				</span>
				$appeal.AuthorResponse
			#end
		</div>


		#if($shouldExpand)

			<h4 class="reported-content">$core_v2_language.GetResource('ReportedContent')</h4>
			<div class="post-content content">
				#set ($theaterUrl = $core_v2_widget.GetExecutedFileUrl('hiddencontent.vm'))
				#set ($theaterUrl = $core_v2_page.AdjustQueryString($theaterUrl, "AppealId=$appeal.AppealId"))
				<a href="#" class="ui-theater" data-theaterurl="$core_v2_encoding.HtmlAttributeEncode($theaterUrl)">$core_v2_language.GetResource('ViewPost')</a>
				<p>$!appeal.BoardResponse</p>
			</div>

			## resolved
			#if ($appeal.AppealState == 'Accepted' || $appeal.AppealState == 'Rejected' || $appeal.AppealState == 'Expired')
				<div class="attribute-list-header"></div>
				<ul class="attribute-list response">
					#if ($appeal.ModifiedByUser)
						<li class="attribute-item responder">
							<span class="attribute-name">$core_v2_language.GetResource('Responder')</span>
							<span class="attribute-value">
								<span class="avatar">
									#if ($appeal.ModifiedByUser.ProfileUrl)
										<a href="$core_v2_encoding.HtmlAttributeEncode($appeal.ModifiedByUser.ProfileUrl)">
											$core_v2_ui.GetResizedImageHtml($appeal.ModifiedByUser.AvatarUrl,32,32,"%{alt=$appeal.ModifiedByUser.DisplayName,style='border-width:0;'}")
										</a>
									#else
										$core_v2_ui.GetResizedImageHtml($appeal.ModifiedByUser.AvatarUrl,32,32,"%{alt=$appeal.ModifiedByUser.DisplayName,style='border-width:0;'}")
									#end
								</span>
								<span class="user-name">
									#if($appeal.ModifiedByUser.ProfileUrl)
										<a href="$core_v2_encoding.HtmlAttributeEncode($appeal.ModifiedByUser.ProfileUrl)" class="internal-link view-user-profile">
											<span></span>$appeal.ModifiedByUser.DisplayName
										</a>
									#else
										<span></span>$appeal.ModifiedByUser.DisplayName
									#end
								</span>
							</span>
						</li>
					#end
					<li class="attribute-item response">
						#if ($appeal.AppealState == 'Accepted')
							<span class="attribute-name">$core_v2_language.GetResource('Accepted')</span>
						#elseif ($appeal.AppealState =='Rejected')
							<span class="attribute-name">$core_v2_language.GetResource('Rejected')</span>
						#end
						#if ($appeal.ResolutionDate)
							<span class="attribute-value">$core_v2_language.FormatDateAndTime($appeal.ResolutionDate)</span>
						#end
					</li>
				</ul>
				<div class="attribute-list-footer"></div>
			## still manipulatable
			#elseif(!$appeal.RequiresManualActionToCorrectContent)
				#if ($appeal.Content.HtmlName('Web') || $appeal.Content.HtmlDescription('Web'))
					<div class="post-content field-list" style="display:none">
						<div class="field-list-header"></div>
						<fieldset class="field-list">
							<legend class="field-list-description"></legend>
							<ul class="field-list">
								<li class="field-item membership-type">
									<label for="$core_v2_widget.UniqueId('BoardResponse')" class="field-item-header">$core_v2_language.GetResource('BoardResponseLabel')</label>
									<span class="field-item-input">
										<textarea id="$core_v2_widget.UniqueId('BoardResponse')"></textarea>
									</span>
								</li>
							</ul>
						</fieldset>
						<div class="field-list-footer"></div>
					</div>
					<div class="post-actions begin">
						<div class="navigation-list-header"></div>
						<ul class="navigation-list">
							<li class="navigation-item">
								<a href="#" class="begin-accept"><span></span>$core_v2_language.GetResource('Accept')</a>
							</li>
							<li class="navigation-item">
								<a href="#" class="begin-reject"><span></span>$core_v2_language.GetResource('Reject')</a>
							</li>
						</ul>
						<div class="navigation-list-footer"></div>
					</div>
					<div class="post-actions accept" style="display:none">
						<div class="navigation-list-header"></div>
						<ul class="navigation-list">
							<li class="navigation-item">
								<a href="#" class="back"><span></span>$core_v2_language.GetResource('Cancel')</a>
							</li>
							<li class="navigation-item">
								<a href="#" class="accept"><span></span>$core_v2_language.GetResource('Accept')</a>
							</li>
						</ul>
						<div class="navigation-list-footer"></div>
					</div>
					<div class="post-actions reject" style="display:none">
						<div class="navigation-list-header"></div>
						<ul class="navigation-list">
							<li class="navigation-item">
								<a href="#" class="back"><span></span>$core_v2_language.GetResource('Cancel')</a>
							</li>
							<li class="navigation-item">
								<a href="#" class="reject"><span></span>$core_v2_language.GetResource('Reject')</a>
							</li>
						</ul>
						<div class="navigation-list-footer"></div>
					</div>
				#end
			#end

		#end

	</div>

	<div class="abbreviated-post-footer"></div>
</li>
