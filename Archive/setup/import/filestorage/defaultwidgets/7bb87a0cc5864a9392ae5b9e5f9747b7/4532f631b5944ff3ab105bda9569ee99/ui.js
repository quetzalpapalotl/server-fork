(function($)
{
	if (typeof $.telligent === 'undefined')
	        $.telligent = {};

	if (typeof $.telligent.evolution === 'undefined')
	    $.telligent.evolution = {};

	if (typeof $.telligent.evolution.widgets === 'undefined')
	    $.telligent.evolution.widgets = {};

	var searchClick = function(context)
		{
			window.location = context.searchUrlTemplate.replace(/\{SearchQuery\}/g, encodeURIComponent(context.searchInput.val()));
		};

	$.telligent.evolution.widgets.message = 
	{
		register: function(context) 
		{
			// context = { wrapperId, searchButton, searchInput, searchInputText, searchUrlTemplate }
			context.searchButton.evolutionValidation(
			{
				validateOnLoad: false,
				onValidated: function(isValid, buttonClicked, c) 
				{ 
					if (isValid) 
						context.searchButton.removeClass('disabled'); 
					else
						context.searchButton.addClass('disabled');
				},
				onSuccessfulClick: function(e) 
				{
					context.searchButton.addClass('disabled');
					searchClick(context);
				}
			});

			var f = context.searchButton.evolutionValidation('addCustomValidation', 'searchtext', function() 
			{
				return context.searchInput.val().length > 0 && context.searchInput.val() != context.searchInputText; 
			}, context.searchInputRequiredText, '#' + context.wrapperId + ' .field-item.search .field-item-validation', null);
			
			context.searchInput.keypress(function(e)
			{
				if ((e.keyCode || e.which) == 13)
				{
					if (context.searchButton.evolutionValidation('validate'))
						searchClick(context);
					
					return false;
				}
			});
			
			context.searchInput.focus(function()
			{
				if (context.searchInput.val() == context.searchInputText)
					context.searchInput.val('');
			});
			
			context.searchInput.blur(function()
			{
				if (context.searchInput.val().length == 0)
					context.searchInput.val(context.searchInputText);
			});
		}
	};
})(jQuery);
