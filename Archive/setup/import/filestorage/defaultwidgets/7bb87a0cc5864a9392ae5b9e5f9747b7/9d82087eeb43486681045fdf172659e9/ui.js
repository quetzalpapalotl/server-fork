(function($)
{
    if (typeof $.telligent === 'undefined') $.telligent = {};
    if (typeof $.telligent.evolution === 'undefined') $.telligent.evolution = {};
    if (typeof $.telligent.evolution.widgets === 'undefined') $.telligent.evolution.widgets = {};


    var AttachmentPreview = null,
	loadStatusForm = function(context, delayLoad)
    {
        var wrapper = $(context.wrapperSelector);
        var messageForm = $('.message-form', wrapper);
        var statusTextbox = $('textarea', messageForm);
        var fileUploadContainer = $('.field-item-input.file-upload', messageForm);
        $('.post-form', wrapper).hide();
        $('.application-message').hide();
        var fileUploadToggle = $('.field-item-input.file a', messageForm);

        if (delayLoad)
            messageForm.slideDown();
        else
            messageForm.show();

		AttachmentPreview = AttachmentPreview || (function(){
			var previewContainer = $('.post-thumbnail.status-attachment-preview', context.wrapperSelector);
			var previewTimeout,
	            attachedFileName = null,
				previewContent = null;
	            loadAttachmentPreview = function(fileName) {
					if(fileName)  {
						attachedFileName = fileName;
					}
	                clearTimeout(previewTimeout);
	                previewTimeout = setTimeout(function(){
	                    var data = {
	                        w_messageBody: statusTextbox.evolutionComposer('val'),
	                        w_groupId: context.groupId
	                    };
	                    if(attachedFileName) {
	                        data.w_attachedFileName = attachedFileName;
	                        data.w_attachedFileContext = context.statusUploadContextId;
	                    }
	                    $.telligent.evolution.post({
	                        url: context.previewAttachmentUrl,
	                        data: data,
	                        success: function(response) {
	                            response = $.trim(response);
								if(response && response.length > 0 && response !== previewContent) {
									previewContent = response;
									previewContainer.html(previewContent);
	                                previewContainer.slideDown(100);
								}
	                        }
	                    });
	                }, 150);
	            };
			return {
				loadPreview: function(fileName) {
					loadAttachmentPreview(fileName || null);
				},
				clearPreview: function() {
	                previewContainer.slideUp(100);
					clearTimeout(previewTimeout);
					attachedFileName = null;
					previewContent = null;
					previewContainer.empty();
				}
			}
		})();

        statusTextbox.val(context.statusMessagePrompt);
        if (statusTextbox.data('loaded') !== true)
        {
            statusTextbox.one('focus', function(){
                statusTextbox.evolutionComposer({
                    plugins: ['mentions','hashtags','urlhighlight']
                })
                .evolutionComposer('oninput', function(e){
                    AttachmentPreview.loadPreview();
                })
                .evolutionComposer('onkeydown', function(e){
                    if (e.which === 13)
                    {
                        e.preventDefault();
                        if (!e.shiftKey) {
	                        var fileUpload = fileUploadContainer.children('div');
	                        addStatusMessage(context, statusTextbox.evolutionComposer('val'),
	                            fileUploadToggle.hasClass('active') && fileUpload !== null && fileUpload.length > 0 && fileUpload.glowUpload('val') !== null ? fileUpload.glowUpload('val').name : null);
	                        $(e.target).blur();
	                    }
                        return false;
                    }
                    return true;
                })
                .keyup(function(e)
                {
                    if (e.keyCode != 13 || e.shiftKey)
                        validateMessageSubmit(context);
                })
                .focus(function()
                {
                    if (statusTextbox.evolutionComposer('val') == context.statusMessagePrompt)
                        statusTextbox.evolutionComposer('val','').trigger('keydown').trigger('keyup');
                }).blur(function()
                {
                    validateMessageSubmit(context);
                    if (statusTextbox.evolutionComposer('val') == "")
                        statusTextbox.evolutionComposer('val',context.statusMessagePrompt).trigger('keydown').trigger('keyup');
                });
            });

            fileUploadToggle.click(function() {
                if (fileUploadContainer.children('div').length == 0) {
                    var fileUploadInput = fileUploadToggle.closest('.field-item-input.file');
					fileUploadContainer.show();

                    var fileUpload = $('<div></div>');
                    fileUploadContainer.html(fileUpload);

                    fileUpload.glowUpload({
                        fileFilter: null,
                        width: '350px',
                        uploadUrl: context.statusUploadFileUrl
                    })
                    .bind('glowUploadBegun', function(e) {
                        messageForm.data('uploading', true);
                        validateMessageSubmit(context);
                    })
                    .bind('glowUploadComplete', function(e, file) {
                        AttachmentPreview.loadPreview(file.name);
                        messageForm.data('uploading', false);
                        validateMessageSubmit(context);
                    });

                    $(this).addClass('active');
                } else if (fileUploadContainer.css('display') == 'none') {
                    fileUploadContainer.show();
                    $(this).addClass('active');
                } else {
                    fileUploadContainer.hide();
                    $(this).removeClass('active');
                }

                return false;
            });

            $('.internal-link.add-post', messageForm).click(function(e)
            {
                var fileUpload = fileUploadContainer.children('div');
                addStatusMessage(context, statusTextbox.evolutionComposer('val'),
                    fileUploadToggle.hasClass('active') && fileUpload !== null && fileUpload.length > 0 && fileUpload.glowUpload('val') !== null ? fileUpload.glowUpload('val').name : null);
                e.preventDefault();
            });

            statusTextbox.data('loaded', true);
        }
    },
    validateMessageSubmit = function(context)
    {
        var wrapper = $(context.wrapperSelector);
        var messageForm = $('.message-form', wrapper);
        var statusTextbox = $('textarea', messageForm);
        var submit = $('.internal-link.add-post', messageForm);
        if ($.trim(statusTextbox.val()) == context.statusMessagePrompt || $.trim(statusTextbox.val()) == "" || messageForm.data('uploading') === true)
        {
            submit.addClass("disabled");
            return false;
        }

        submit.removeClass("disabled");
        return true;
    },
    addStatusMessage = function(context, value, attachedFileName)
    {
        if (!validateMessageSubmit(context))
            return;

        var wrapper = $(context.wrapperSelector);
        var messageForm = $('.message-form', wrapper);
        var statusTextbox = $('textarea', messageForm);
        var fileUploadContainer = $('.field-item-input.file-upload', messageForm);
        var fileUploadToggle = $('.field-item-input.file a', messageForm);
        statusTextbox.after('<span class="add-message-loading"></span>');
        $('.internal-link.add-post', messageForm)
            .unbind("click")
            .addClass("add-post add-post__disabled add-post__disabled__internal-link disabled disabled__internal-link")
            .html('<span></span>' + context.publishingText);

        var statusMessagePost = {
            message:value, groupId:context.groupId,
            contextId: context.statusFileUploadContextId
        };
        if(attachedFileName) {
            statusMessagePost.fileName = attachedFileName;
        }

        $.telligent.evolution.post({
            url: context.addStatusUrl,
            data: statusMessagePost,
            success: function(response)
            {
                $(document).trigger('telligent_messaging_activitymessageupdated', [response.body]);
                $(document).trigger('telligent_core_usersstatusupdated', [response.body]);
                $('.attribute-item.view-user-status .attribute-value', wrapper).html(response.body);
                $('.attribute-item.view-user-status-date .attribute-value', wrapper).html(context.statusDateSecondsAgo);
                $('.add-message-loading').remove();
                statusTextbox.evolutionComposer('val',context.statusMessagePrompt).trigger('keydown').trigger('keyup');
                $('.internal-link.add-post', messageForm)
                    .html('<span></span>' + context.postText)
                    .removeClass("add-post__disabled add-post__disabled__internal-link disabled disabled__internal-link")
                    .click(function(e) {
                        var fileUpload = fileUploadContainer.children('div');
                        addStatusMessage(context, statusTextbox.evolutionComposer('val'),
                            fileUploadToggle.hasClass('active') && fileUpload !== null && fileUpload.length > 0 && fileUpload.glowUpload('val') !== null ? fileUpload.glowUpload('val').name : null);
                        e.preventDefault();
                    });
                $('.field-item-input.file-upload', messageForm).hide().html('');
                $('.field-item-input.file a', messageForm).removeClass('active');
                AttachmentPreview.clearPreview();
                validateMessageSubmit(context);
            },
            error: function(xhr, desc, ex)
            {
                $.telligent.evolution.notifications.show(desc,{type:'error'});
                $('.add-message-loading').remove();
                $('.internal-link.add-post', messageForm)
                    .html('<span></span>' + context.postText)
                    .removeClass("add-post__disabled add-post__disabled__internal-link disabled disabled__internal-link")
                    .click(function(e) {
                        var fileUpload = fileUploadContainer.children('div');
                        addStatusMessage(context, statusTextbox.evolutionComposer('val'),
                            fileUploadToggle.hasClass('active') && fileUpload !== null && fileUpload.length > 0 && fileUpload.glowUpload('val') !== null ? fileUpload.glowUpload('val').name : null);
                        e.preventDefault();
                    });

                validateMessageSubmit(context);
            }
        });
    },
    loadWeblogForm = function(context)
    {
        var wrapper = $(context.wrapperSelector);
        $('.message-form', wrapper).hide();
        var postForm = $('.post-form', wrapper);
        postForm.html(context.loadingAppsHtml.replace(/{AppType}/g, context.weblogTitle)).show();

        $.telligent.evolution.post({
            url: context.weblogsTabUrl,
            data: {},
            dataType: 'html',
            success: function(response)
            {
                postForm.fadeOut(200, function()
                {
                    postForm.html(response).slideDown();

                    attachPostFormInputHandlers(postForm);
                    var weblogId = $('span', $('.post-to', postForm)).attr('data-weblog-id');
					var bodyComposer = $('.post-body textarea', postForm).one('focus', function() { bodyComposer.evolutionComposer({ plugins: ['hashtags','mentions'] }); });
                    var initialBodyHelp = bodyComposer.val();
					var titleComposer = $('.post-title textarea', postForm).one('focus', function(){ titleComposer.evolutionComposer({ plugins: ['hashtags'] }); });
                    var initialTitleHelp = titleComposer.val();

                    $('.internal-link.advanced-post', postForm).click(function()
                    {
                        if (!weblogId)
                            weblogId = $('select', $('.post-to', postForm)).val();

                        if (weblogId)
                        {
                            var title = titleComposer.evolutionComposer('val');
                            var body = bodyComposer.evolutionComposer('val');
                            if (title == initialTitleHelp)
                                title = "";
                            if (body == initialBodyHelp)
                                body = "";

                            advancedWeblogSubmit(context, weblogId, title, body);
                        }
                    });

                    $('.internal-link.add-post', postForm).click(function()
                    {
                        if ($(this).hasClass('add-post__disabled'))
                            return false;

                        if (!weblogId)
                            weblogId = $('select', $('.post-to', postForm)).val();

                        if (weblogId)
                            submitWeblogPost(context, weblogId, titleComposer.evolutionComposer('val'), bodyComposer.evolutionComposer('val'));
                    });
                });
            }
        });
    },
    advancedWeblogSubmit = function(context, weblogId, title, body)
    {
        $.telligent.evolution.post({
            url: context.weblogsAdvancedPostUrl,
            data: {weblogId:weblogId, title:title, body:body},
            success: function(response)
            {
                window.location = response.url;
            }
        });
    },
    submitWeblogPost = function(context, weblogId, title, body)
    {
        if ($('.post-form .internal-link.add-post', $(context.wrapperSelector)).hasClass('disabled'))
            return;

        $('.post-form .internal-link.add-post', $(context.wrapperSelector))
            .addClass("add-post add-post__disabled add-post__disabled__internal-link disabled disabled__internal-link")
            .html('<span></span>' + context.publishingText);

        $.telligent.evolution.post({
            url: context.weblogsPostUrl,
            data: {weblogId:weblogId, title:title, body:body},
            dataType: 'html',
            success: function(response)
            {
                $(document).trigger('telligent_messaging_activitymessageupdated', []);
                var wrapper = $(context.wrapperSelector);
                var messageContainer = $('.message-container', wrapper);
                messageContainer.html(response).show();
                $('.message', messageContainer).show();
                loadWeblogForm(context);

                $('.internal-link.close-message', messageContainer).click(function()
                {
                    messageContainer.fadeOut().slideUp();
                });
            },
            error: function(xhr, desc, ex)
            {
                $.telligent.evolution.notifications.show(desc,{type:'error'});
                $('.post-form .internal-link.add-post', $(context.wrapperSelector))
                    .html('<span></span>' + context.postText)
                    .removeClass("add-post__disabled add-post__disabled__internal-link disabled disabled__internal-link");
            }
        });
    },
    loadForumForm = function(context)
    {
        var wrapper = $(context.wrapperSelector);
        $('.message-form', wrapper).hide();
        var postForm = $('.post-form', wrapper);
        postForm.html(context.loadingAppsHtml.replace(/{AppType}/g, context.forumTitle)).show();

        $.telligent.evolution.post({
            url: context.forumsTabUrl,
            data: {},
            dataType: 'html',
            success: function(response)
            {
                postForm.fadeOut(200, function()
                {
                    postForm.html(response).slideDown();

                    attachPostFormInputHandlers(postForm);
                    var forumId = $('span', $('.post-to', postForm)).attr('data-forum-id');
					var bodyComposer = $('.post-body textarea', postForm).one('focus', function() { bodyComposer.evolutionComposer({ plugins: ['hashtags','mentions'] }); });
                    var initialBodyHelp = bodyComposer.val();
					var titleComposer = $('.post-title textarea', postForm).one('focus', function(){ titleComposer.evolutionComposer({ plugins: ['hashtags'] }); });
                    var initialTitleHelp = titleComposer.val();

                    $('.internal-link.advanced-post', postForm).click(function()
                    {
                        if (!forumId)
                            forumId = $('select', $('.post-to', postForm)).val();

                        if (forumId)
                        {
                            var title = titleComposer.evolutionComposer('val');
                            var body = bodyComposer.evolutionComposer('val');
                            if (title == initialTitleHelp)
                                title = "";
                            if (body == initialBodyHelp)
                                body = "";

                            advancedForumSubmit(context, forumId, title, body);
                        }
                    });

                    $('.internal-link.add-post', postForm).click(function()
                    {
                        if ($(this).hasClass('add-post__disabled'))
                            return false;

                        if (!forumId)
                            forumId = $('select', $('.post-to', postForm)).val();

                        if (forumId)
                            submitForumPost(context, forumId, titleComposer.evolutionComposer('val'), bodyComposer.evolutionComposer('val'));
                    });
                });
            }
        });
    },
    advancedForumSubmit = function(context, forumId, title, body)
    {
        $.telligent.evolution.post({
            url: context.forumsAdvancedPostUrl,
            data: {forumId:forumId, title:title, body:body},
            success: function(response)
            {
                window.location = response.url;
            }
        });
    },
    submitForumPost = function(context, forumId, title, body)
    {
        if ($('.post-form .internal-link.add-post', $(context.wrapperSelector)).hasClass('disabled'))
            return;

		var button = $('.post-form .internal-link.add-post', $(context.wrapperSelector));
		var classes = button.attr('class');
		var html = button.html();
		button.addClass("add-post add-post__disabled add-post__disabled__internal-link disabled disabled__internal-link")
			.html('<span></span>' + context.publishingText);

        $.telligent.evolution.post({
            url: context.forumsPostUrl,
            data: {forumId:forumId, title:title, body:body},
            dataType: 'html',
            success: function(response)
            {
                $(document).trigger('telligent_messaging_activitymessageupdated', []);
                var wrapper = $(context.wrapperSelector);
                var messageContainer = $('.message-container', wrapper);
                messageContainer.html(response).show();
                $('.message', messageContainer).show();

                if ($('.message', messageContainer).attr('data-is-error') != 'true')
                {
                    loadForumForm(context);
                }
				else
				{
					button.attr('class', classes);
					button.html(html);
				}

                $('.internal-link.close-message', messageContainer).click(function()
                {
                    messageContainer.fadeOut().slideUp();
                });
            },
            error: function(xhr, desc, ex)
            {
                $.telligent.evolution.notifications.show(desc,{type:'error'});
                $('.post-form .internal-link.add-post', $(context.wrapperSelector))
                    .html('<span></span>' + context.postText)
                    .removeClass("add-post__disabled add-post__disabled__internal-link disabled disabled__internal-link");
            }
        });
    },
    loadMediaForm = function(context)
    {
        var wrapper = $(context.wrapperSelector);
        $('.message-form', wrapper).hide();
        var postForm = $('.post-form', wrapper);
        postForm.html(context.loadingAppsHtml.replace(/{AppType}/g, context.mediaTitle)).show();

        $.telligent.evolution.post({
            url: context.mediasTabUrl,
            data: {},
            dataType: 'html',
            success: function(response)
            {
                postForm.fadeOut(200, function()
                {
                    postForm.html(response).slideDown();

                    attachMediaFormInputHandlers(postForm);
                    var mediaId = $('span', $('.post-to', postForm)).attr('data-gallery-id');
					var bodyComposer = $('.post-body textarea', postForm).one('focus', function() { bodyComposer.evolutionComposer({ plugins: ['hashtags','mentions'] }); });
                    var initialBodyHelp = bodyComposer.val();
                    var initialFileHelp = $('input', $('.field-item.post-file', postForm)).val();
					var titleComposer = $('.post-title textarea', postForm).one('focus', function(){ titleComposer.evolutionComposer({ plugins: ['hashtags'] }); });
                    var initialTitleHelp = titleComposer.val();

                    $('.internal-link.add-attachment', postForm).click(function()
                    {
                        if (!mediaId)
                            mediaId = $('select', $('.post-to', postForm)).val();

                        if (mediaId)
                        {
                            attachFileHandler(context, postForm, mediaId, initialTitleHelp, initialFileHelp);
                        }
                    });

                    $('input', $('.post-file', postForm)).focus(function()
                    {
                        if (!mediaId)
                            mediaId = $('select', $('.post-to', postForm)).val();

                        if (mediaId)
                        {
                            attachFileHandler(context, postForm, mediaId, initialTitleHelp, initialFileHelp);
                        }
                    });

                    $('.internal-link.advanced-post', postForm).click(function()
                    {
                        if (!mediaId)
                            mediaId = $('select', $('.post-to', postForm)).val();

                        var attachItem = $(".field-item.post-file", postForm);
                        if (mediaId)
                        {
                            var title = titleComposer.evolutionComposer('val');
                            var body = bodyComposer.evolutionComposer('val');
                            var fileName = $('input', $('.field-item.post-file', postForm)).val();
                            if (title == initialTitleHelp)
                                title = "";
                            if (body == initialBodyHelp)
                                body = "";
                            if (fileName == initialFileHelp)
                                fileName = "";

                            advancedMediaSubmit(context, mediaId, title, body, fileName, attachItem.attr('data-attachment-id'), attachItem.attr('data-attachment-url'));
                        }
                    });

                    $('.internal-link.add-post', postForm).click(function()
                    {
                        if ($(this).hasClass('add-post__disabled'))
                            return false;

                        if (!mediaId)
                            mediaId = $('select', $('.post-to', postForm)).val();

                        var attachItem = $(".field-item.post-file", postForm);

                        if (mediaId)
                        {
                            var body = bodyComposer.evolutionComposer('val');
                            if (body == initialBodyHelp)
                                body = "";

                            submitMediaPost(context, mediaId, titleComposer.evolutionComposer('val'), body, $('input', attachItem).val(), attachItem.attr('data-attachment-id'), attachItem.attr('data-attachment-url'));
                        }
                    });
                });
            }
        });
    },
    attachFileHandler = function(context, postForm, mediaId, initialTitleHelp, initialFileHelp)
    {
        $.telligent.evolution.post({
            url: context.uploadAttachmentUrl,
            data: {mediaId:mediaId},
            success: function(response)
            {
                Telligent_Modal.Open(response.url, 640, 480, function(result)
                {
                    if (result != null && result.fileName && result.fileName.length > 0)
                    {
                        var attachItem = $(".field-item.post-file", postForm);
                        var removeLink = $(".internal-link.remove-attachment", attachItem);
                        var attachmentFileName = $("input", attachItem);
                        var attachLink = $(".internal-link.add-attachment", attachItem);

                        attachLink.html('<span></span>' + context.changeFileText);
                        attachLink.hide();
                        attachmentFileName.val(result.fileName).removeClass("empty");

                        if (result.contextId && result.contextId.length > 0)
                            attachItem.attr('data-attachment-id', result.contextId).show();
                        else if (result.url && result.url.length > 0)
                            attachItem.attr('data-attachment-url', result.url).show();

                        validateMediaForm(postForm, initialTitleHelp, initialFileHelp);

                        removeLink.show().click(function()
                        {
                            attachLink.html('<span></span>' + context.attachFileText);
                            attachLink.show();
                            removeLink.hide();
                            attachmentFileName.val(context.attachFileHelpText).addClass("empty");
                            attachItem.attr('data-attachment-id', '');
                            attachItem.attr('data-attachment-url', '');
                            $('.internal-link.add-post', postForm).addClass("add-post__disabled add-post__disabled__internal-link disabled disabled__internal-link");
                        });
                    }
                });
            }
        });
    },
    advancedMediaSubmit = function(context, mediaId, title, body, fileName, contextId, fileUrl)
    {
        $.telligent.evolution.post({
            url: context.mediasAdvancedPostUrl,
            data: {mediaId:mediaId, title:title, body:body, fileName:fileName, fileContextId:contextId, fileUrl:fileUrl},
            success: function(response)
            {
                window.location = response.url;
            }
        });
    },
    submitMediaPost = function(context, mediaId, title, body, fileName, contextId, fileUrl)
    {
        if ($('.post-form .internal-link.add-post', $(context.wrapperSelector)).hasClass('disabled'))
            return;

		var button = $('.post-form .internal-link.add-post', $(context.wrapperSelector));
		var classes = button.attr('class');
		var html = button.html();
		button.addClass("add-post add-post__disabled add-post__disabled__internal-link disabled disabled__internal-link")
			.html('<span></span>' + context.publishingText);

        $.telligent.evolution.post({
            url: context.mediasPostUrl,
            data: {mediaId:mediaId, title:title, body:body, fileName:fileName, fileContextId:contextId, fileUrl:fileUrl},
            dataType: 'html',
            success: function(response)
            {
                $(document).trigger('telligent_messaging_activitymessageupdated', []);
                var wrapper = $(context.wrapperSelector);
                var messageContainer = $('.message-container', wrapper);
                messageContainer.html(response).show();
                $('.message', messageContainer).show();
				if ($('.message', messageContainer).attr('data-is-error') != 'true')
				{
					loadMediaForm(context);
				}
				else
				{
					button.attr('class', classes);
					button.html(html);
				}

                $('.internal-link.close-message', messageContainer).click(function()
                {
                    messageContainer.fadeOut().slideUp();
                });
            },
            error: function(xhr, desc, ex)
            {
                $.telligent.evolution.notifications.show(desc,{type:'error'});
                $('.post-form .internal-link.add-post', $(context.wrapperSelector))
                    .html('<span></span>' + context.postText)
                    .removeClass("add-post__disabled add-post__disabled__internal-link disabled disabled__internal-link");
            }
        });
    },
    loadWikiForm = function(context)
    {
        var wrapper = $(context.wrapperSelector);
        $('.message-form', wrapper).hide();
        var postForm = $('.post-form', wrapper);
        postForm.html(context.loadingAppsHtml.replace(/{AppType}/g, context.wikiTitle)).show();

        $.telligent.evolution.post({
            url: context.wikisTabUrl,
            data: {},
            dataType: 'html',
            success: function(response)
            {
                postForm.fadeOut(200, function()
                {
                    postForm.html(response).slideDown();

                    attachPostFormInputHandlers(postForm);
                    var wikiId = $('span', $('.post-to', postForm)).attr('data-wiki-id');
					var bodyComposer = $('.post-body textarea', postForm).one('focus', function() { bodyComposer.evolutionComposer({ plugins: ['hashtags','mentions'] }); });
                    var initialBodyHelp = bodyComposer.val();
					var titleComposer = $('.post-title textarea', postForm).one('focus', function(){ titleComposer.evolutionComposer({ plugins: ['hashtags'] }); });
                    var initialTitleHelp = titleComposer.val();

                    $('.internal-link.advanced-post', postForm).click(function()
                    {
                        if (!wikiId)
                            wikiId = $('select', $('.post-to', postForm)).val();

                        if (wikiId)
                        {
                            var title = titleComposer.evolutionComposer('val');
                            var body = bodyComposer.evolutionComposer('val');
                            if (title == initialTitleHelp)
                                title = "";
                            if (body == initialBodyHelp)
                                body = "";

                            advancedWikiSubmit(context, wikiId, title, body);
                        }
                    });

                    $('.internal-link.add-post', postForm).click(function()
                    {
                        if ($(this).hasClass('add-post__disabled'))
                            return false;

                        if (!wikiId)
                            wikiId = $('select', $('.post-to', postForm)).val();

                        if (wikiId)
                            submitWikiPost(context, wikiId, titleComposer.evolutionComposer('val'), bodyComposer.evolutionComposer('val'));
                    });
                });
            }
        });
    },
    advancedWikiSubmit = function(context, wikiId, title, body)
    {
        $.telligent.evolution.post({
            url: context.wikisAdvancedPostUrl,
            data: {wikiId:wikiId, title:title, body:body},
            success: function(response)
            {
                window.location = response.url;
            }
        });
    },
    submitWikiPost = function(context, wikiId, title, body)
    {
        if ($('.post-form .internal-link.add-post', $(context.wrapperSelector)).hasClass('disabled'))
            return;

		var button = $('.post-form .internal-link.add-post', $(context.wrapperSelector));
		var classes = button.attr('class');
		var html = button.html();
		button.addClass("add-post add-post__disabled add-post__disabled__internal-link disabled disabled__internal-link")
			.html('<span></span>' + context.publishingText);

        $.telligent.evolution.post({
            url: context.wikisPostUrl,
            data: {wikiId:wikiId, title:title, body:body},
            dataType: 'html',
            success: function(response)
            {
                $(document).trigger('telligent_messaging_activitymessageupdated', []);
                var wrapper = $(context.wrapperSelector);
                var messageContainer = $('.message-container', wrapper);
                messageContainer.html(response).show();
                $('.message', messageContainer).show();
				if ($('.message', messageContainer).attr('data-is-error') != 'true')
				{
					loadWikiForm(context);
				}
				else
				{
					button.attr('class', classes);
					button.html(html);
				}

                $('.internal-link.close-message', messageContainer).click(function()
                {
                    messageContainer.fadeOut().slideUp();
                });
            },
            error: function(xhr, desc, ex)
            {
                $.telligent.evolution.notifications.show(desc,{type:'error'});
                $('.post-form .internal-link.add-post', $(context.wrapperSelector))
                    .html('<span></span>' + context.postText)
                    .removeClass("add-post__disabled add-post__disabled__internal-link disabled disabled__internal-link");
            }
        });
    },
    attachPostFormInputHandlers = function(postForm)
    {
        var initialTitleHelp = $('textarea', $('.field-item.post-title', postForm)).val();
        var initialBodyHelp = $('.post-body textarea', postForm).val();

        $('textarea', $('.field-item.post-title', postForm)).focus(function()
        {
            if ($(this).val() == initialTitleHelp)
            {
                $(this).removeClass('empty');
                $(this).val('');
            }
        }).blur(function()
        {
            if ($(this).val() == "")
            {
                $(this).addClass('empty');
                $(this).val(initialTitleHelp);
            }
        }).keyup(function()
        {
            validatePostForm(postForm, initialTitleHelp, initialBodyHelp);
        });

        $('.post-body textarea', postForm).focus(function()
        {
            if ($(this).val() == initialBodyHelp)
            {
                $(this).removeClass('empty');
                $(this).val('');
            }
            $(this).evolutionResize();
        }).blur(function()
        {
            if ($(this).val() == "")
            {
                $(this).addClass('empty');
                $(this).val(initialBodyHelp);
            }
        }).keyup(function()
        {
            validatePostForm(postForm, initialTitleHelp, initialBodyHelp);
        });
    },
    validatePostForm = function(postForm, initialTitleHelp, initialBodyHelp)
    {
        var submitButton = $('.internal-link.add-post', postForm);
        var currentTitleVal = $.trim($('textarea', $('.field-item.post-title', postForm)).val());
        var currentBodyVal = $.trim($('.post-body textarea', postForm).val());

        if (currentTitleVal == initialTitleHelp || currentTitleVal == "")
        {
            submitButton.addClass("add-post__disabled add-post__disabled__internal-link disabled disabled__internal-link");
            return false;
        }
        else if (currentBodyVal == initialBodyHelp || currentBodyVal == "")
        {
            submitButton.addClass("add-post__disabled add-post__disabled__internal-link disabled disabled__internal-link");
            return false;
        }

        submitButton.removeClass("add-post__disabled add-post__disabled__internal-link disabled disabled__internal-link");
        return true;
    },
    attachMediaFormInputHandlers = function(postForm)
    {
        initialTitleHelp = $('textarea', $('.field-item.post-title', postForm)).val();
        initialFileHelp = $('input', $('.field-item.post-file', postForm)).val();
        initialBodyHelp = $('.post-body textarea', postForm).val();

        $('textarea', $('.field-item.post-title', postForm)).focus(function()
        {
            if ($(this).val() == initialTitleHelp)
            {
                $(this).removeClass('empty');
                $(this).val('');
            }
        }).blur(function()
        {
            if ($(this).val() == "")
            {
                $(this).addClass('empty');
                $(this).val(initialTitleHelp);
            }
        }).keyup(function()
        {
            validateMediaForm(postForm, initialTitleHelp, initialFileHelp);
        });

        $('input', $('.field-item.post-file', postForm)).focus(function()
        {
            if ($(this).val() == initialFileHelp)
            {
                $(this).removeClass('empty');
                $(this).val('');
            }
            else
                validateMediaForm(postForm, initialTitleHelp, initialFileHelp);

        }).change(function()
        {
            if ($(this).val() == "")
            {
                $(this).addClass('empty');
                $(this).val(initialFileHelp);
            }
            else
                validateMediaForm(postForm, initialTitleHelp, initialFileHelp);
            $(this).evolutionResize();
        });

        $('.post-body textarea', postForm).focus(function()
        {
            if ($(this).val() == initialBodyHelp)
            {
                $(this).removeClass('empty');
                $(this).val('');
            }
        }).blur(function()
        {
            if ($(this).val() == "")
            {
                $(this).addClass('empty');
                $(this).val(initialBodyHelp);
            }
        }).keyup(function()
        {
            validateMediaForm(postForm, initialTitleHelp, initialFileHelp);
        });
    },
    validateMediaForm = function(postForm, initialTitleHelp, initialFileHelp)
    {
        var submitButton = $('.internal-link.add-post', postForm);
        var currentTitleVal = $.trim($('textarea', $('.field-item.post-title', postForm)).val());
        var currentFileVal = $.trim($('input', $('.field-item.post-file', postForm)).val());

        if (currentTitleVal == initialTitleHelp || currentTitleVal == "")
        {
            submitButton.addClass("add-post__disabled add-post__disabled__internal-link disabled disabled__internal-link");
            return false;
        }
        else if (currentFileVal == initialFileHelp || currentFileVal == "")
        {
            submitButton.addClass("add-post__disabled add-post__disabled__internal-link disabled disabled__internal-link");
            return false;
        }

        submitButton.removeClass("add-post__disabled add-post__disabled__internal-link disabled disabled__internal-link");
        return true;
    },
    reloadPage = function()
    {
        window.location = window.location;
    };

    $.telligent.evolution.widgets.quickPost =
    {
        register: function (context)
        {
            $('textarea').evolutionResize();

            var wrapper = $(context.wrapperSelector);

            var statusFilterButton = $('.filter-option.msg.selected', wrapper);
            if (context.isStatus || (statusFilterButton && statusFilterButton.length > 0))
                loadStatusForm(context, false);

            $('.filter-option.msg', wrapper).click(function()
            {
                if (!$(this).hasClass('selected'))
                {
                    $('.filter-option.selected', wrapper).removeClass('selected');
                    $(this).addClass('selected');
                    $('.message-container', wrapper).hide();
                    loadStatusForm(context, true);
                }
            });

            var weblogFilterButton = $('.filter-option.weblog.selected', wrapper);
            if (weblogFilterButton && weblogFilterButton.length > 0)
                loadWeblogForm(context);

            $('.filter-option.weblog', wrapper).click(function()
            {
                if (!$(this).hasClass('selected'))
                {
                    $('.filter-option.selected', wrapper).removeClass('selected');
                    $(this).addClass('selected');
                    $('.message-container', wrapper).hide();
                    loadWeblogForm(context);
                }
            });

            if (context.isWeblog)
            {
                $('.filter-option.selected', wrapper).removeClass('selected');
                $(this).addClass('selected');
                $('.message-container', wrapper).hide();
                loadWeblogForm(context);
            }

            var forumFilterButton = $('.filter-option.forum.selected', wrapper);
            if (forumFilterButton && forumFilterButton.length > 0)
                loadForumForm(context);

            $('.filter-option.forum', wrapper).click(function()
            {
                if (!$(this).hasClass('selected'))
                {
                    $('.filter-option.selected', wrapper).removeClass('selected');
                    $(this).addClass('selected');
                    $('.message-container', wrapper).hide();
                    loadForumForm(context);
                }
            });

            if (context.isForum)
            {
                $('.filter-option.selected', wrapper).removeClass('selected');
                $(this).addClass('selected');
                $('.message-container', wrapper).hide();
                loadForumForm(context);
            }

            var mediaFilterButton = $('.filter-option.media-gallery.selected', wrapper);
            if (mediaFilterButton && mediaFilterButton.length > 0)
                loadMediaForm(context);

            $('.filter-option.media-gallery', wrapper).click(function()
            {
                if (!$(this).hasClass('selected'))
                {
                    $('.filter-option.selected', wrapper).removeClass('selected');
                    $(this).addClass('selected');
                    $('.message-container', wrapper).hide();
                    loadMediaForm(context);
                }
            });

            if (context.isMedia)
            {
                $('.filter-option.selected', wrapper).removeClass('selected');
                $(this).addClass('selected');
                $('.message-container', wrapper).hide();
                loadMediaForm(context);
            }

            var wikiFilterButton = $('.filter-option.wiki.selected', wrapper);
            if (wikiFilterButton && wikiFilterButton.length > 0)
                loadWikiForm(context);

            $('.filter-option.wiki', wrapper).click(function()
            {
                if (!$(this).hasClass('selected'))
                {
                    $('.filter-option.selected', wrapper).removeClass('selected');
                    $(this).addClass('selected');
                    $('.message-container', wrapper).hide();
                    loadWikiForm(context);
                }
            });

            if (context.isWiki)
            {
                $('.filter-option.selected', wrapper).removeClass('selected');
                $(this).addClass('selected');
                $('.message-container', wrapper).hide();
                loadWikiForm(context);
            }
        }
    };
})(jQuery);
