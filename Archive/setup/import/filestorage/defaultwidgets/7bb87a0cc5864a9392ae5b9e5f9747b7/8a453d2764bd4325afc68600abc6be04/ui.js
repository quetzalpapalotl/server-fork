(function($)
{
	if (typeof $.telligent === 'undefined')
		$.telligent = {};

	if (typeof $.telligent.evolution === 'undefined')
		$.telligent.evolution = {};

	if (typeof $.telligent.evolution.widgets === 'undefined')
		$.telligent.evolution.widgets = {};

	var load = function (context, pageIndex) {
		var data = { BaseUrl: context.baseUrl };
		if (pageIndex)
			data.PageIndex = pageIndex - 1;
		$.telligent.evolution.get({
			url: context.loadUrl,
			data: data,
			success: function(response)
			{
				context.wrapper.fadeOut('fast', function() {
					$(this).html($.trim(response) == '' ? context.noSearchItemsHtml : response).fadeIn('fast');
					attachHandlers(context);
				});
			}
		});
	};

	var attachHandlers = function (context) {
		$('.pager a', context.wrapper).click(function() {
			var match = RegExp('[?&]' + context.pageIndexQueryKey + '=([^&]*)').exec(this.href);
			if (match && match[1]) {
				load(context, parseInt(match[1]));
				return false;
			}
		});
	};
	
	$.telligent.evolution.widgets.trackTermSearch = 
	{
		register: function(context) 
		{
			load(context);
		}
	};
})(jQuery);
