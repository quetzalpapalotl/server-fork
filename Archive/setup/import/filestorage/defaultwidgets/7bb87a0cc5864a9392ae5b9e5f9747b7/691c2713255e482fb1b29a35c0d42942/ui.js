(function($) {
    var regenerateKey = function(context, keyId) {
            $.telligent.evolution.post({
                url: context.regenerateUrl,
                data: {
                    KeyId: keyId
                },
                success: function(response) {
                    window.location.reload(true);
                }
            });
        },
        deleteKey = function(context, keyId) {
            $.telligent.evolution.post({
                url: context.deleteUrl,
                data: {
                    KeyId: keyId
                },
                success: function(response) {
                    window.location.reload(true);
                }
            });
        },
        getKeyId = function(target) {
            return parseInt($(target).closest('tr').attr('data-apiid'), 10);
        };
    var api = {
        register: function(context) {
            var init = function() {
                context.wrapper = $(context.wrapper);
                $('a.regenerate', context.wrapper).bind('click', function(e,data){
                    if(confirm(context.regenerateConfirmMessage)){
                        var keyId = getKeyId(e.target);
                        regenerateKey(context, keyId);
                    }
                    return false;
                });
                $('a.delete', context.wrapper).bind('click', function(e,data){
                    if(confirm(context.deleteConfirmMessage)){
                        var keyId = getKeyId(e.target);
                        deleteKey(context, keyId);
                    }
                    return false;
                });
                $('a.edit', context.wrapper).bind('click', function(e,data){
                    var url = $(this).attr('href');
                    Telligent_Modal.Open(url,500,200,null);
                    return false;
                });
            };
            $.telligent.evolution.messaging.subscribe(context.pagedMessage, init);
            init();
        }
    };

    if (typeof $.telligent === 'undefined') { $.telligent = {}; }
    if (typeof $.telligent.evolution === 'undefined') { $.telligent.evolution = {}; }
    if (typeof $.telligent.evolution.widgets === 'undefined') { $.telligent.evolution.widgets = {}; }
    $.telligent.evolution.widgets.apiKeyList = api;

}(jQuery));
