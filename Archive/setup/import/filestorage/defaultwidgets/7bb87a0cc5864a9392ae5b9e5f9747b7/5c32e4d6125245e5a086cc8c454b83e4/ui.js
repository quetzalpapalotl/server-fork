(function($){
	var spinner = '<div style="text-align: center;"><img src="' + $.telligent.evolution.site.getBaseUrl() + 'utility/spinner.gif" /></div>',
		searchGroups = function(context, textbox, searchText) {
			if(searchText && searchText.length >= 2) {

				textbox.glowLookUpTextBox('updateSuggestions', [
					textbox.glowLookUpTextBox('createLookUp', '', spinner, spinner, false)
				]);

				$.telligent.evolution.get({
					url: $.telligent.evolution.site.getBaseUrl() + 'api.ashx/v2/groups.json',
					data: {
						GroupNameFilter: searchText,
						Permission: 'Group_CreateGroup'
					},
					success: function(response) {
						if (response && response.Groups.length >= 1) {
							textbox.glowLookUpTextBox('updateSuggestions',
								$.map(response.Groups, function(group, i) {
									return textbox.glowLookUpTextBox('createLookUp', group.Id, group.Name, group.Name, true);
								}));
						} else {
							textbox.glowLookUpTextBox('updateSuggestions', [
								textbox.glowLookUpTextBox('createLookUp', '', context.noGroupsMatchText, context.noGroupsMatchText, false)
							]);
						}
					}
				});
			}
		},
		createApplications = function(context, group) {
			var createMethods = [],
				succeededSubCreateCount = 0,
				// tracks that an asynchronous sub-app creation succeeded
				registerSuccess = function() {
					succeededSubCreateCount++;
					// if all registered sub-apps were created, redirect to new group url
					if(succeededSubCreateCount > createMethods.length) {
						window.location.href = group.Url;
					}
				},
				handleResponse = function(response, errorMessage) {
					if(response.Errors && response.Errors.length > 0) {
						alert(errorMessage);
					}
					registerSuccess();
				};

			// register a set of asynchronous sub-app creation methods
			if(context.createBlogInput.is(':checked')) {
				createMethods.push(function(){
					$.telligent.evolution.post({
						url: $.telligent.evolution.site.getBaseUrl() + 'api.ashx/v2/blogs.json',
						data: {
							GroupId: group.Id,
							Name: group.Name + ' - ' + context.singularNameBlog
						},
						success: function(response) {
							handleResponse(response, context.createBlogErrorMessage);
						},
						defaultErrorMessage: context.createBlogErrorMessage
					});
				});
			}
			if(context.createGalleryInput.is(':checked')) {
				createMethods.push(function(){
					$.telligent.evolution.post({
						url: $.telligent.evolution.site.getBaseUrl() + 'api.ashx/v2/galleries.json',
						data: {
							GroupId: group.Id,
							Name: group.Name + ' - ' + context.singularNameGallery
						},
						success: function(response) {
							handleResponse(response, context.createGalleryErrorMessage);
						},
						defaultErrorMessage: context.createGalleryErrorMessage
					});
				});
			}
			if(context.createWikiInput.is(':checked')) {
				createMethods.push(function(){
					$.telligent.evolution.post({
						url: $.telligent.evolution.site.getBaseUrl() + 'api.ashx/v2/wikis.json',
						data: {
							GroupId: group.Id,
							Name: group.Name + ' - ' + context.singularNameWiki
						},
						success: function(response) {
							handleResponse(response, context.createWikiErrorMessage);
						},
						defaultErrorMessage: context.createWikiErrorMessage
					});
				});
			}
			if(context.createForumInput.is(':checked')) {
				createMethods.push(function(){
					var forumCreateData = {
						GroupId: group.Id,
						Name: group.Name + ' - ' + context.singularNameForum
					};
					// if there was a mailing list associated with this forum create request, create it now
					if($(context.forumMailingInput).is(':checked')) {
						$.extend(forumCreateData, {
							EnableMailingList: true,
							MailingListName: forumCreateData.Name,
							MailingListAddress: $(context.forumAddressInput).val()
						});
					}
					$.telligent.evolution.post({
						url: $.telligent.evolution.site.getBaseUrl() + 'api.ashx/v2/forums.json',
						data: forumCreateData,
						success: function(response) {
							handleResponse(response, context.createForumErrorMessage);
						},
						defaultErrorMessage: context.createForumErrorMessage
					});
				});
			}
			// run all registered create methods, if any
			$.each(createMethods, function(i, method) { method(); });
			// register an initial success call, which will fail if there were registered sub-apps, and
			// will properly redirect to group url if there were not.
			registerSuccess();
		};
		createGroup = function(context) {
			var createData = {
				Name: context.groupNameInput.val(),
				Description: context.groupDescriptionInput.val(),
				EnableGroupMessages: context.enableMessagesInput.is(':checked'),
				GroupType: $('input:radio[name='+context.groupTypeName+']:checked').val(),
				AutoCreateApplications: false // want to specify which applications to create
			};
			if(context.parentGroupInput.val() !== null && context.parentGroupInput.val().length > 0) {
				createData.ParentGroupId = Number(context.parentGroupInput.val());
			}
			$.telligent.evolution.post({
				url: $.telligent.evolution.site.getBaseUrl() + 'api.ashx/v2/groups.json',
				data: createData,
				success: function(response) {
					createApplications(context, response.Group);
				}
			});
		},
		updateMailingListVisiblity = function(context) {
			if(context.createForumInput.is(':checked')) {
				context.enableEmailFieldItem.show();
				context.emailAddressFieldItem.show();
			} else {
				context.forumMailingInput.removeAttr('checked');
				context.enableEmailFieldItem.hide();
				context.emailAddressFieldItem.hide();
			}
			if(context.forumMailingInput.is(':checked')) {
				context.emailAddressFieldItem.show();
			} else {
				context.emailAddressFieldItem.hide();
			}
		};
	var api = {
		register: function(context) {
			context.parentGroupInput
				.glowLookUpTextBox({
					delimiter: ',',
					allowDuplicates: true,
					maxValues: 1,
					onGetLookUps: function(tb, searchText) {
						searchGroups(context, tb, searchText);
					},
					emptyHtml: '',
					selectedLookUpsHtml: [],
					deleteImageUrl: ''});

			context.createGroupInput
				.evolutionValidation({
					onValidated: function(isValid, buttonClicked, c) {
						if (isValid) {
							context.createGroupInput.removeClass('disabled');
						} else {
							context.createGroupInput.addClass('disabled');
						}
					},
					onSuccessfulClick: function(e) {
						e.preventDefault();
						context.createGroupInput.parent().addClass('processing');
						context.createGroupInput.addClass('disabled');
						// submit the form
						createGroup(context);
					}})
				.evolutionValidation('addField',
					context.groupNameInput,
					{
						required: true,
						groupnameexists: {
							getParentId: function(){
								var parentGroupInput = Number(context.parentGroupInput.val());
								if(!parentGroupInput) {
									parentGroupInput = context.rootGroupId;
								}
								return parentGroupInput;
							}
						},
						messages: {
							required: context.requiredNameValidationMessage,
							groupnameexists: context.uniqueNameValidationMessage
						}
					},
					context.groupNameInput.closest('.field-item').find('.field-item-validation'), null);

			if(context.forumMailingInput.length > 0) {

				$.validator.addMethod("forummailinglist", function(value, element) {
					var isChecked = context.createForumInput.is(':checked') && context.forumMailingInput.is(':checked');
					if (!isChecked) {
						return true;
					} else {
						return $.trim(context.forumAddressInput.val()).length > 0;
					}
				}, context.mailingListRequiredValidationMessage);

				context.createGroupInput
					.evolutionValidation('addField',
						context.forumAddressInput,
						{
							forummailinglist: true,
							mailinglistnameexists: true
						},
						context.forumAddressInput.closest('.field-item').find('.field-item-validation'), null);

				$(context.createForumInput.add(context.forumMailingInput)).bind('click', function(){
					$(context.forumAddressInput).valid();
				});
			}

			context.parentGroupInput.bind('glowLookUpTextBoxChange', function(){
				context.createGroupInput
					.evolutionValidation('validateField', context.groupNameInput);
			});

			context.forumMailingInput.bind('click', function(){
				updateMailingListVisiblity(context);
			});
			// uncheck forum mailing checkbox if create forum checkbox unchecked
			context.createForumInput.bind('click', function(){
				updateMailingListVisiblity(context);
			});
			updateMailingListVisiblity(context);

			if(context.defaultParentGroupName && context.defaultParentGroupId > 0) {
				var initialLookupValue = context.parentGroupInput.glowLookUpTextBox('createLookUp',
					context.defaultParentGroupId,
					context.defaultParentGroupName,
					context.defaultParentGroupName,
					true);
				context.parentGroupInput.glowLookUpTextBox('add', initialLookupValue);
			}
		}
	};

	if (typeof $.telligent === 'undefined') { $.telligent = {}; }
	if (typeof $.telligent.evolution === 'undefined') { $.telligent.evolution = {}; }
	if (typeof $.telligent.evolution.widgets === 'undefined') { $.telligent.evolution.widgets = {}; }
	$.telligent.evolution.widgets.createGroup = api;

}(jQuery));
