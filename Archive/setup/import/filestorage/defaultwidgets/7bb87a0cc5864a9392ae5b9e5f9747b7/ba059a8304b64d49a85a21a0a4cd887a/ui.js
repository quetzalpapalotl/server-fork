(function($, global) {
	if (typeof $.telligent === 'undefined') { $.telligent = {}; }
	if (typeof $.telligent.evolution === 'undefined') { $.telligent.evolution = {}; }		
	if (typeof $.telligent.evolution.widgets === 'undefined') { $.telligent.evolution.widgets = {}; }

	$.telligent.evolution.widgets.forgottenPassword = {
		register: function(context) {

			context.recover.evolutionValidation({
				onValidated: function(isValid, buttonClicked, c) {
				},
				onSuccessfulClick: function(e) {
					e.preventDefault();
					context.recover.attr('disabled',true);
					context.message.hide();
					
					$.telligent.evolution.post({
						url: context.recoverUrl,
						data: { emailAddress: $(context.emailAddressSelector).val() },
						success: function(response)
						{
							if (response.message)
							{
								if (response.success === 'true')
								{
									context.message.html(response.message).attr('class','message success').show();
									$('.message.directions, .field-list-header, .field-list, .field-list-footer', $('#' + context.wrapperId)).hide();
								}
								else
								{
									context.message.html(response.message).attr('class','message error').show();
								}
							}							

							context.recover.attr('disabled',false);
						},
						defaultErrorMessage: context.errorText,
						error: function(xhr, desc, ex)
						{
							context.message.html(desc).attr('class','message error').show();
							context.recover.attr('disabled',false);
						}
					});

					return false;
				}
			});

			context.recover.evolutionValidation('addField', context.emailAddressSelector, {
					required: true,
					email: true,
					messages: { required: '*', email: '*' }
				},
				'#' + context.wrapperId + ' .field-item.email-address .field-item-validation',
				null
			);

		}
	};
}(jQuery, window));
