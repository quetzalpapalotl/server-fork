(function($) {

    var refresh = function(context, link) {

        var update = function() {
            var data = {};
            data[context.pageKey] = context.pageIndex;
            // modify the url instead of passing as data, as the url might have this in the querystring already
            var url = $.telligent.evolution.url.modify({ url: context.pagedContentUrl, query: data });
            $.telligent.evolution.get({
                url: url,
            	cache: false,
            	success: function(response) {
                    var response = $(response);
                    if(response.find('li.content-item').length === 0 && !context.isLoaded && !context.isPreview) {
                        // no content on initial rendering, so hide widget
                        $('#' + context.wrapperId).hide();
                    } else {
                    	context.isLoaded = true;
                        $(context.pagedContentId).html(response);
                        addEventHandlers(context);
                    }
            	}
            });
        }

        if (link) {
            link.closest('li.content-item').slideUp('fast', update);
        } else {
            update();
        }
    },
    addEventHandlers = function(context) {
        // ignore
        $('#' + context.wrapperId + ' a.ignore-suggestion').on('click', function() {
            var link = $(this);
            $.telligent.evolution.post({
            	url: $.telligent.evolution.site.getBaseUrl() + 'api.ashx/v2/content/recommendation/ignore/{ContentId}.json',
            	data: {
            		ContentId: link.data('contentid')
            	},
            	success: function(response) {
                    refresh(context, link);
                }
            });

            return false;
        }).hide();

        $('#' + context.wrapperId + ' li.content-item').on('mouseover swiperight', function(e) {
            var elm = $(this);
            $('a.ignore-suggestion', elm).show();

            if (e.type == 'swiperight') {
                $(document).on('click', function() {
                    $('a.ignore-suggestion', elm).hide();
                });
            }
        }).on('mouseout', function() {
            $('a.ignore-suggestion', $(this)).hide();
        });
    };

	var api = {
		register: function(context) {
			context.isLoaded = false;
			
            $(context.pagerId).evolutionPager({
                currentPage: context.pageIndex,
                pageSize: context.pageSize,
                pageKey: context.pageKey,
                hash: context.pageKey,
                baseUrl: context.pagedContentUrl,
                showPrevious: true,
		        showNext: true,
		        showFirst: false,
		        showLast: false,
		        showIndividualPages: false,
                pagedContentContainer: $(context.pagedContentId),
                onPage: function(pageIndex, complete) {
                    context.pageIndex = pageIndex;
                    var data = {};
                    data[context.pageKey] = pageIndex;
                    // modify the url instead of passing as data, as the url might have this in the querystring already
                    var url = $.telligent.evolution.url.modify({ url: context.pagedContentUrl, query: data });
                    $.telligent.evolution.get({
                    	url: url,
                    	cache: false,
                    	success: function(response) {
                    		complete(response);
                            addEventHandlers(context);
                    	}
                    });
                }
            });

            refresh(context);
			addEventHandlers(context);
		}
	};



	if (typeof $.telligent === 'undefined') { $.telligent = {}; }
	if (typeof $.telligent.evolution === 'undefined') { $.telligent.evolution = {}; }
	if (typeof $.telligent.evolution.widgets === 'undefined') { $.telligent.evolution.widgets = {}; }
	$.telligent.evolution.widgets.recommendedContent = api;

}(jQuery));