(function($, global) {

    // ensures an action only occurs once across all tabs and windows
    //  doOnce(uniqueKey, function() { }, options)
    //  options: (optional)
    //    expireAfter: 5000
    //    maxWait: 500
    var doOnce = (function(){

        var actionsStorageKeyNameSpace = '_unique_actions_',
            get = function(key) {
                return JSON.parse(localStorage.getItem(key));
            },
            set = function(key, value) {
                localStorage.setItem(key, JSON.stringify(value));
            },
            remove = function(key) {
                localStorage.removeItem(key);
            };

        var api = function(key, task, options) {
            var settings = $.extend({
                expireAfter: 5000,
                maxWait: 500
            }, options || {});

            // wait for a small random amount of time to avoid race conditions across tabs
            setTimeout(function(){
                // if no localstorage record exists of this action being taken,
                // record it now and invoke the action
                var actionRecord = get(actionsStorageKeyNameSpace + key);
                if(!actionRecord) {
                    set(actionsStorageKeyNameSpace + key, {
                        expireAfter: (new Date()).getTime() + settings.expireAfter
                    });
                    task();
                }
            }, Math.floor(Math.random() * settings.maxWait));
        };

        // garbage-collect action records that happened far enough in the past not to care
        setInterval(function(){
            var currentTime = new Date().getTime();
            for (var i = 0; i < localStorage.length; i++){
                var actionKey = localStorage.key(i);
                if(actionKey.indexOf(actionsStorageKeyNameSpace) === 0) {
                    var actionRecord = get(actionKey);
                    if(actionRecord && actionRecord.expireAfter <= currentTime) {
                        remove(actionKey);
                    }
                }
            }
        }, 1000);

        return api;
    })();

    // var notifier = nativeNotifier();
    // notifier.notify({  });
    //    title: null,
    //    body: null,
    //    iconUrl: null,
    //    tag: null,
    //    onclick: null,
    //    onclose: null,
    //    onshow: null
    var nativeNotifier = (function() {

        var enable = function() {
                if (global.Notification && global.Notification.permissionLevel && global.Notification.permissionLevel() === "default") {
                    global.Notification.requestPermission(function () {
                        isEnabled = true;
                    });
                } else if (global.webkitNotifications && global.webkitNotifications.checkPermission()) {
                    global.webkitNotifications.requestPermission(function () {
                        isEnabled = true;
                    });
                }
            },
            notify = function(options) {
                settings = $.extend({}, {
                    key: null,
                    title: null,
                    body: null,
                    iconUrl: null,
                    tag: null,
                    onclick: null,
                    onclose: null,
                    onshow: null,
                    hideAfter: 2000
                }, options);

                var renderNotification = function() {
                    if (global.Notification && global.Notification.permissionLevel && global.Notification.permissionLevel() === "granted") {
                        var note = new global.Notification(settings.title, {
                            iconUrl: settings.iconUrl || "",
                            body: settings.body || "",
                            tag: settings.tag
                        });
                        if(settings.onclick) {
                            note.onclick = function() {
                                global.focus();
                                this.cancel();
                                settings.onclick();
                            };
                        }
                        if(settings.onclose) { note.onclose = settings.onclose; }
                        if(settings.onshow) { note.onshow = settings.onshow; }
                        note.show();
                    } else if (global.webkitNotifications && !global.webkitNotifications.checkPermission()) {
                        var note = global.webkitNotifications.createNotification(settings.iconUrl || "", settings.title, settings.body || "");
                        if(settings.onclick) {
                            note.onclick = function() {
                                global.focus();
                                this.cancel();
                                settings.onclick();
                            };
                        }
                        if(settings.onclose) { note.onclose = settings.onclose; }
                        if(settings.onshow) { note.onshow = settings.onshow; }
                        note.show();
                    }
                    if(note) {
                        setTimeout(function(){
                            note.close();
                        }, settings.hideAfter);
                    }
                };

                // if there's a unique key, only raise once per all tabs
                if(settings.key) {
                    doOnce(settings.key, renderNotification);
                } else {
                    renderNotification();
                }
            };

        return function() {
            // attempt to request permission on user gesture if not already granted
            $(function(){
                $('body').one('click', enable)
                    .one('keydown', enable);
            });
            return {
                notify: notify
            };
        }

    })();

    var notifier = (function() {
        var defaults = {
                parent: 'body',
                margin: 5,
                animationDuration: 250,
                opacity: 0.95,
                limit: 10,
                native: false,
                template: ''
            };
        function add(context, alert) {
            if(context.native) {
                context.nativeNotifier.notify({
                    key: (alert.id + alert.contentUrl + alert.message),
                    title: $.telligent.evolution.html.decode(alert.message),
                    iconUrl: alert.avatarUrl,
                    tag: 'notification-' + alert.id,
                    hideAfter: alert.hideAfter,
                    onclick: function() {
                        window.location.href = alert.contentUrl;
                    }
                });
            }
            var existingAlert = $.grep(context.alerts, function(a) { return a.id === alert.id });
            // notification already exists
            if(existingAlert !== null && existingAlert.length === 1) {
                existingAlert = existingAlert[0];
                conceal(context, existingAlert).then(function(){
                    existingAlert.view.remove();
                    existingAlert.message = alert.message;
                    existingAlert.cssClass = alert.cssClass;
                    existingAlert.hideAfter = alert.hideAfter;
                    buildView(context, existingAlert);
                    existingAlert.suppressPositionAnimation = true;
                    repositionAlerts(context);
                    reveal(context, existingAlert);
                    handleEvents(context, existingAlert);
                });
                return existingAlert;
            // add new notification
            } else {
                // if this will exceed limit, remove oldest
                if(context.alerts.length === context.limit) {
                    remove(context, context.alerts[context.alerts.length-1]);
                }
                context.alerts.unshift(alert);
                buildView(context, alert);
                repositionAlerts(context);
                reveal(context, alert);
                handleEvents(context, alert);
                return alert;
            }
        }

        function repositionAlerts(context) {
            var offset = context.margin;
            $.each(context.alerts, function(i, alert) {
                if(alert.suppressPositionAnimation) {
                    alert.view.css({ bottom: offset + 'px', opacity: 0 });
                    alert.suppressPositionAnimation = false;
                } else{
                    alert.view.animate({ bottom: offset + 'px' }, context.animationDuration );
                }
                if(alert.measuredHeight > 0) {
                    offset += (alert.measuredHeight + context.margin)
                }
            });
        }

        function conceal(context, alert) {
            return $.Deferred(function(dfd){
                alert.view.fadeOut(context.animationDuration, dfd.resolve);
            }).promise();
        }

        function reveal(context, alert) {
            return $.Deferred(function(dfd){
                alert.view.fadeTo(context.animationDuration, context.opacity, dfd.resolve);
            }).promise();
        }

        function remove(context, alert) {
            alert.removing = true;
            alert.measuredHeight = 0;
            repositionAlerts(context);
            conceal(context, alert).then(function(){
                alert.view.remove();
                context.alerts = $.grep(context.alerts, function(a) { return a !== alert; });
            });
        }

        function scheduleRemoval(context, alert) {
            global.clearTimeout(alert.removeTimeout);
            global.clearInterval(alert.hideSupressedWaitInterval);
            global.setTimeout(function(){
                if(!context.removalSuppressed) {
                    remove(context, alert);
                } else {
                    alert.hideSupressedWaitInterval = global.setInterval(function() {
                        if(!context.removalSuppressed) {
                            global.clearInterval(alert.hideSupressedWaitInterval);
                            remove(context, alert);
                        }
                    }, 1000);
                }
            }, alert.hideAfter);
        }

        function buildView(context, alert) {
            alert.active = !!alert.onClick;
            alert.view = $(context.template(alert))
                .css({
                    position: 'fixed',
                    left: context.margin + 'px',
                    bottom: context.margin + 'px',
                    left: context.margin + 'px',
                    zIndex: 10000
                })
                .hide()
                .appendTo(context.parent);
            alert.measuredHeight = alert.view.outerHeight();
        }

        function handleEvents(context, alert) {
            alert.view.find('a.close').on('click', function(e){
                e.preventDefault();
                e.stopPropagation();
                if(alert.onClose()) {
                    alert.onClose().then(function(){
                        remove(context, alert);
                    });
                } else {
                    remove(context, alert);
                }
            });
            alert.view.on('click', function () {
                if(alert.onClick) {
                    alert.onClick();
                }
            });
            alert.view.on('mouseenter', function () {
                context.entered = true;
                context.removalSuppressed = true;

                // if in process of removing, re-show
                if(alert.removing) {
                    alert.view.stop();
                    alert.removing = false;
                    reveal(context, alert);
                    repositionAlerts(context);
                    scheduleRemoval(context, alert);
                }
            });
            alert.view.on('mouseleave', function () {
                context.entered = false;
                global.setTimeout(function() {
                    if(!context.entered) {
                        context.removalSuppressed = false;
                    }
                }, 500)
            });
        }

        return function(options) {
            var context = $.extend({}, defaults, options || {}, {
                removalSuppressed: false,
                alerts: []
            });
            context.template = $.telligent.evolution.template.compile(context.template);

            if(context.native) {
                context.nativeNotifier = nativeNotifier();
            }

            function notify(options) {
                var alert = $.extend({}, notify.defaults, options || {});
                scheduleRemoval(context, add(context, alert));
            }
            notify.defaults = {
                id: '',
                message: '',
                cssClass: null,
                onClick: null,
                onClose: null,
                hideAfter: 5000
            };
            return {
                notify: notify
            };
        };
    })();

    var api = {
        register: function(options) {
            var n = notifier({
                template: options.template,
                limit: options.limit,
                native: options.native,
                opacity: 0.95
            });

            $.telligent.evolution.messaging.subscribe('notification.raised', function(data) {
                n.notify({
                    id: data.id,
                    message: data.message,
                    hideAfter: options.duration,
                    avatarUrl: data.avatarUrl,
                    contentUrl: data.contentUrl,
                    onClose: function() {
                        return $.telligent.evolution.put({
                            url: $.telligent.evolution.site.getBaseUrl() + 'api.ashx/v2/notification/{NotificationId}.json',
                            data: {
                                NotificationId: data.id,
                                MarkAsRead: true
                            }
                        });
                    },
                    onClick: function() {
                        return $.telligent.evolution.put({
                            url: $.telligent.evolution.site.getBaseUrl() + 'api.ashx/v2/notification/{NotificationId}.json',
                            data: {
                                NotificationId: data.id,
                                MarkAsRead: true
                            }
                        }).then(function(){
                            // redirect
                            window.location.href = data.contentUrl;
                        });
                    }
                });
            });

        }
    };

    $.telligent = $.telligent || {};
    $.telligent.evolution = $.telligent.evolution || {};
    $.telligent.evolution.widgets = $.telligent.evolution.widgets || {};
    $.telligent.evolution.widgets.userNotifications = api;

})(jQuery, window);
