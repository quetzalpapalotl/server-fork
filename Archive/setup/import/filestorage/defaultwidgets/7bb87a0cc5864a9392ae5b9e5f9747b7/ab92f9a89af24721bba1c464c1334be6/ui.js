(function (j, global)
{
	if (typeof j.telligent === 'undefined')
		j.telligent = {};

	if (typeof j.telligent.evolution === 'undefined')
		j.telligent.evolution = {};

	if (typeof j.telligent.evolution.widgets === 'undefined')
		j.telligent.evolution.widgets = {};

	var leaveGroup = function (context)
	{
		j.telligent.evolution.del({
			url: j.telligent.evolution.site.getBaseUrl() + 'api.ashx/v2/groups/' + context.groupId + '/members/users/' + context.userId + '.json',
			data: { UserId:context.userId, GroupId: context.groupId },
			success: function (response)
			{
				if(context.isGroupPrivate)
					global.location.href=$.telligent.evolution.site.getBaseUrl()
				else
					global.location.reload();
			},
			defaultErrorMessage: context.errorText
		});
	},
	joinGroup = function (context)
	{
		j.telligent.evolution.post({
			url: j.telligent.evolution.site.getBaseUrl() + 'api.ashx/v2/groups/' + context.groupId + '/members/users.json',
			data: { UserId: context.userId, GroupId: context.groupId },
			success: function (response)
			{
				global.location.reload();
			},
			defaultErrorMessage: context.errorText
		});
	},
	requestJoinGroup = function (context)
	{
		Telligent_Modal.Open(context.requestJoinUrl, 550, 300, function() { global.location.reload(); });
	},
	toggleMembership = function (context)
	{
		if (context.membershipType != "None")
			leaveGroup(context);
		else if (context.canJoinGroup)
			joinGroup(context);
		else
			requestJoinGroup(context);
	};

	j.telligent.evolution.widgets.groupBanner =
	{
		register: function (context) {
			if (context.forceRequestMembership) {
				if (context.canJoinGroup) {
					joinGroup(context);
				} else {
					requestJoinGroup(context);
				}
			}

			if (context.groupMembershipButton && context.groupMembershipButton.length > 0)
				context.groupMembershipButton.click(function (e) { toggleMembership(context); e.preventDefault(); });
		}
	};
})(jQuery, window);