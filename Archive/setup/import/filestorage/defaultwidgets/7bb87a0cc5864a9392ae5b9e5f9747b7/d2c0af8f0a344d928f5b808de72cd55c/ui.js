;(function($, global){

	var WikiService = (function(){
		return {
			unpublish: function(wikiId, pageId) {
				return $.telligent.evolution.put({
					url: $.telligent.evolution.site.getBaseUrl() + 'api.ashx/v2/wikis/{WikiId}/pages/{PageId}.json',
					dataType: 'json',
					data: {
						WikiId: wikiId,
						PageId: pageId,
						IsPublished: false
					}
				});
			},
			listChildren: function(wikiId, parentPageId) {
				return $.telligent.evolution.get({
					url: $.telligent.evolution.site.getBaseUrl() + 'api.ashx/v2/wikis/{WikiId}/pages/{ParentPageId}/pages.json',
					dataType: 'json',
					data: {
						WikiId: wikiId,
						ParentPageId: parentPageId
					}
				}).pipe(function(response) {
					return response.WikiPages;
				});
			},
			updateParent: function(wikiId, pageId, newParentPageId) {
				return $.telligent.evolution.put({
					url: $.telligent.evolution.site.getBaseUrl() + 'api.ashx/v2/wikis/{WikiId}/pages/{PageId}.json',
					dataType: 'json',
					data: {
						WikiId: wikiId,
						PageId: pageId,
						ParentPageId: newParentPageId
					}
				});
			}
		};
	})();

	var AppealService = (function(){
		var updateAppeal = function(appealId, reason, state) {
			return $.telligent.evolution.put({
				url: $.telligent.evolution.site.getBaseUrl() + 'api.ashx/v2/abuseappeals/{AppealId}.json',
				dataType: 'json',
				data: {
					AppealId: appealId,
					BoardResponse: reason,
					AppealState: state
				}
			})
		};
		return {
			accept: function(appealId, reason) {
				return updateAppeal(appealId, reason, 'Accepted');
			},
			reject: function(appealId, reason) {
				return updateAppeal(appealId, reason, 'Rejected');
			}
		}
	})();

	var animationDuration = 150,
		flashMessageAndReload = function(message, url) {
			$.telligent.evolution.notifications.show(message, {
				type: 'success',
				callback: function() {
					if(url) {
						window.location = url;
					} else {
						window.location.reload(true);
					}
				}
			});
		},
		restore = function(options) {
			AppealService.accept(options.appealId, $(options.responseInput).val()).done(function(r){
				$(options.responseForm).slideUp(animationDuration);
				flashMessageAndReload(options.acceptedMessage);
			});
		},
		confirmAbusiveAndDelete = function(options) {
			// get all children
			WikiService.listChildren(options.wikiId, options.wikiPageId).done(function(pages) {
				// for each child, either delete child page or update it to have a different parent
				pageRequests = $.map(pages, function(p) {
					if(options.children) {
						return WikiService.unpublish(options.wikiId, p.Id);
					} else {
						return WikiService.updateParent(options.wikiId, p.Id, options.parentWikiPageId);
					}
				});
				// after all children processed, reject appeal and unpublish page
				$.when.apply($, pageRequests).done(function(){
					// reject page's appeal
					AppealService.reject(options.appealId, $(options.responseInput).val()).done(function(r){
						$(options.responseForm).slideUp(animationDuration);
						// unpublish page
						WikiService.unpublish(options.wikiId, options.wikiPageId).done(function(r){
							// render message and refresh page
							flashMessageAndReload(options.rejectedMessge, options.groupUrl);
						});
					});
				});
			});
		},
		handleRestores = function(options) {
			$(options.restoreLink).on('click', function(e){
				e.preventDefault();
				$(options.responseForm)
					.insertAfter(e.target)
					.slideDown(animationDuration);
				$(options.responseInput).removeAttr('disabled').val('').focus();
				options.onSubmit = function() {
					restore(options);
				};
			});
		},
		handleAbusiveConfirmations = function(options) {
			$(options.confirmLink).on('click', function(e){
				e.preventDefault();
				$(options.responseForm)
					.insertAfter(e.target)
					.slideDown(animationDuration);
				$(options.responseInput).removeAttr('disabled').val('').focus();
				options.onSubmit = function() {
					$.glowModal(options.confirmUrl, {
	                    width: 400,
	                    height: 200,
						onClose: function(response) {
							if(response && response.delete) {
								confirmAbusiveAndDelete($.extend(options, response));
							} else {
								$(options.responseForm).slideUp(animationDuration);
							}
						}
					});
				};
			});
		},
		handleCancel = function(options) {
			$(options.cancelLink).on('click', function(e){
				e.preventDefault();
				$(options.responseForm).slideUp(animationDuration);
			});
		},
		handleSubmit = function(options) {
            $(options.submitLink)
            	.addClass('disabled')
                .evolutionValidation({
                    onValidated: function(isValid, buttonClicked, c) {
                        if (isValid) {
                            $(options.submitLink).removeClass('disabled');
                        } else {
                            $(options.submitLink).addClass('disabled');
                        }
                    },
                    onSuccessfulClick: function(e) {
                        e.preventDefault();
                        $(options.responseInput).attr('disabled','disabled');
                        $(options.submitLink).addClass('disabled');
                        options.onSubmit();
                    }
                })
                .evolutionValidation('addField', $(options.responseInput), {
                    required: true,
                }, options.responseForm + ' .field-item.response .field-item-validation', null);
		};

	var api = {
		register: function(options) {
			handleRestores(options);
			handleAbusiveConfirmations(options);
			handleCancel(options);
			handleSubmit(options);
		}
	};

	$.telligent = $.telligent || {};
	$.telligent.evolution = $.telligent.evolution || {};
	$.telligent.evolution.widgets = $.telligent.evolution.widgets || {};
	$.telligent.evolution.widgets.wikiResolveAppeal = $.telligent.evolution.widgets.wikiResolveAppeal || api;

}(jQuery, window));