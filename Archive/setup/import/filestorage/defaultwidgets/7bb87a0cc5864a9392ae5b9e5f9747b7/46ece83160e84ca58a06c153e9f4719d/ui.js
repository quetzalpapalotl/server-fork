(function($){
	var win = $(window),
		history = [ window.location.href ],
		backButtonIndex = 0;

	$.event.special.directedHashChange = {
		setup: function(data) {
			var comparer = data.customComparer;
			win.bind('hashchange', function(){
				var isReverse, comparerResult = 0;
				if(!!comparer) {
					comparerResult = comparer(history[history.length-1], window.location.href);
					backButtonIndex = history.length;
					history.push(window.location.href);
				}
				if(comparerResult !== 0) {
					isReverse = comparerResult < 0;
				} else {
					isReverse = (history.length >= 2 && history[backButtonIndex - 1] === window.location.href);
				}
				if(comparerResult !== 0) {
					if(isReverse) {
						backButtonIndex--;
					} else {
						backButtonIndex++;
						if(backButtonIndex === history.length) {
							history.push(window.location.href);
						}
					}
				}
				win.trigger('directedHashChange', {
					direction: isReverse ? 'backward' : 'forward',
					hash: window.location.href
				});
			});
		}
	};

}(jQuery));


/*
given markup like:

<div class="search-filter group-filters" data-filtername="group">
	<div class="navigation-list-header">
		<span>Top Groups</span>
	</div>
	<ul class="navigation-list">
		<li class="navigation-item no-group-filter" data-filtervalue="">
			<a href="#">
				<span class="navigation-item-title">All Groups</span>
			</a>
		</li>
		<li class="navigation-item group-filter" data-filtervalue="a">
			<a href="#">
				<span class="navigation-item-title">Group A</span>
				<span class="navigation-item-count">(50)</span>
			</a>
		</li>
		[...]
	</ul>
	<div class="navigation-list-footer"></div>
</div>

usage:

$('div.search-filter')
	.filterSelector()
	.live('filterAdd', function(e, data){
		console.log('filter added:');
		console.log(data);
		})
	.live('filterRemove', function(e, data){
		console.log('filter removed:');
		console.log(data);
		});

// data is of format { name: FILTERNAME, value: FILTERVALUE }

$('div.search-filter').filterSelector('val');
$('div.search-filter').filterSelector('val','wiki');

Handled all via delegation internally so as to survive ajax-based content replacements

*/
(function($){
	var isNullOrEmpty = function(value) { return value === null || value === ""; },
		_contextKey = '_filterSelctor_inited',
		_getCurrentSelection = function(filter, settings) {
			filter = $(filter);
			return {
				name: filter.attr(settings.nameAttribute),
				value: filter.find('li.navigation-item.selected').attr(settings.valueAttribute),
				visible: (filter.find('li.navigation-item:visible').length - 1)
			};
		},
		_uniquelySelect = function(filter, value, settings) {
			filter = $(filter);

			var filterName = filter.attr(settings.nameAttribute);
			var previouslySelected = filter.find('li.navigation-item.selected');
			var previouslySelectedValue = previouslySelected.attr(settings.valueAttribute);
			var newlySelected = filter.find("["+settings.valueAttribute+"='"+value+"']");
			var triggeredAddOrRemove = false;

			if(previouslySelected.length > 0 && previouslySelectedValue !== value) {
				previouslySelected.removeClass(settings.selectedClass);
				if(!isNullOrEmpty(previouslySelectedValue)) {
					filter.trigger(settings.removeFilterEventName, {
						name: filterName,
						value: previouslySelectedValue
					});
					triggeredAddOrRemove = true;
				}
			}

			newlySelected.addClass(settings.selectedClass);
			if(!isNullOrEmpty(value) && value !== previouslySelectedValue) {
				filter.trigger(settings.addFilterEventName, {
					name: filterName,
					value: value,
					displayName: newlySelected.find('span.navigation-item-title').html()
				});
				triggeredAddOrRemove = true;
			}

			if(triggeredAddOrRemove) {
				filter.trigger(settings.changeFilterEventName);
			}
		},
		_init = function(options) {
			if(!this.data(_contextKey)) {
				this.data(_contextKey, true);
				var settings = $.fn.filterSelector.defaults;
				//return this.each(function() {
				var selector = this.selector;
				$(selector + ' li.navigation-item a')
					.live('click', function(e){
						var target = $(e.target);
						_uniquelySelect(
							target.closest('div.search-filter'),
							target.closest('li').attr(settings.valueAttribute),
							settings);
						return false;
					});
				$(selector + ' .navigation-list-footer a')
					.live('click', function(e){
						var hiddenFilters = $(e.target).closest('div.search-filter').find('li:hidden');
						hiddenFilters.slice(0,3).each(function(){
							$(this).slideDown(200);
						});
						var remainingHiddenCount = hiddenFilters.length - 3;
						if(remainingHiddenCount <= 0) {
							$(e.target).slideUp(200);
						} else {
							$(e.target).html(options.viewMoreText.replace(/\{0\}/gi, remainingHiddenCount));
						}
						return false;
					});
			}
			return this;
		};
	var api = {
		val: function(value) {
			var settings = $.fn.filterSelector.defaults;
			if(typeof value !== 'undefined') {
				_uniquelySelect(this, value, settings);
			}
			return _getCurrentSelection(this, settings);
		}
	};
	$.fn.filterSelector = function(method) {
		if(method in api) {
			return api[method].apply(this, Array.prototype.slice.call(arguments, 1));
		} else if (typeof method === 'object' || !method) {
			return _init.apply(this, arguments);
		}
		return this;
	};
	$.extend($.fn.filterSelector, {
		defaults: {
			selectedClass: 'selected',
			addFilterEventName: 'filterAdd',
			removeFilterEventName: 'filterRemove',
			changeFilterEventName: 'filterChange',
			nameAttribute: 'data-filtername',
			valueAttribute: 'data-filtervalue'
		}
	});

}(jQuery));


/* tagList

<div class="breadcrumb-list-wrapper">
	<div class="breadcrumb-list-header">Filter By</div>
	<div class="breadcrumb-list">
		<span class="breadcrumb-item"><span>Blogs</span><a href="#">x</a></span>
		<span class="breadcrumb-item"><span>Group A Name</span><a href="#">x</a></span>
	</div>
	<div class="breadcrumb-list-footer"></div>
</div>


<span class="breadcrumb-item"><span>Blogs</span><a href="#">x</a></span>
<span class="separator"></span>
<span class="breadcrumb-item"><span>Group A Name</span><a href="#">x</a></span>

methods:
	addTag(name,value)
	removeTag(name)
events:
	tagRemove
		name

live-handles clicks internally

*/
(function($){
	var supportsAdvancedAnimation = $.browser.webkit || $.browser.mozilla;
	var removeTag = function(tag) {
			tag
				.find('a').remove().end()
				.css({
					overflow: 'hidden',
					whiteSpace:'nowrap'
				});
			if(supportsAdvancedAnimation) {
				tag.animate(
					{
						opacity: 0,
						width: 0,
						paddingLeft: 0,
						paddingRight: 0
					},
					300,
					function(){
						var parent = tag.parent();
						tag.remove();
						reOrder(parent.find('span.breadcrumb-item'), parent.parent().find('div.breadcrumb-list-header'));
					}
				);
			} else {
				var parent = tag.parent();
				tag.remove();
				reOrder(parent.find('span.breadcrumb-item'), parent.parent().find('div.breadcrumb-list-header'));
			}
		},
		addTag = function(settings, wrapper, tagData) {
			var newTag = $('<span class="breadcrumb-item" '+settings.nameAttribute+'="'+tagData.name+'" '+settings.valueAttribute+'="'+tagData.value+'"><span>'+tagData.displayName+'</span><a href="#">x</a></span>');
			newTag.css({ opacity: 0 }).appendTo(wrapper);
			reOrder(newTag.parent().find('span.breadcrumb-item'), wrapper.parent().find('div.breadcrumb-list-header'));
			if(supportsAdvancedAnimation) {
				newTag.animate({ opacity: 1 }, 300);
			} else {
				newTag.css({ opacity: 1 });
			}
		},
		init = function(options) {
			var settings = $.extend({}, $.fn.tagList.defaults, options || {});
			return $(this.selector + ' a').live('click', function(e, data){
				var target = $(e.target).closest('span'),
					name = target.attr(settings.nameAttribute);
				removeTag(target);
				$(document).trigger(settings.tagRemovedEvent, name);
				return false;
			});
		},
		reOrder = function(tags, header) {
			if(tags.length > 0) {
				header.show();
			} else {
				header.hide();
			}
			tags.each(function(index){
				$(this).css({
					position: 'relative',
					zIndex: (100 - index)
				});
			});
		};
	var api = {
		addTag: function(tagData) {
			addTag($.fn.tagList.defaults, this.find('div.breadcrumb-list'), tagData);
			return this;
		},
		removeTag: function(name) {
			this
				.find('span.breadcrumb-item['+$.fn.tagList.defaults.nameAttribute+'="'+name+'"]')
				.each(function(){
					removeTag($(this));
				});
			return this;
		}
	};
	$.fn.tagList = function(method) {
		if(method in api) {
			return api[method].apply(this, Array.prototype.slice.call(arguments, 1));
		} else if (typeof method === 'object' || !method) {
			return init.apply(this, arguments);
		}
		return this;
	};
	$.fn.tagList.defaults = {
		tagRemovedEvent: 'tagRemove',
		nameAttribute: 'data-filtername',
		valueAttribute: 'data-filtervalue'
	};
}(jQuery));



(function($){
	var supportsAdvancedAnimation = $.browser.webkit || $.browser.mozilla;
	var disableForm = function(context) {
			context.contentWrapper.css({ position: 'relative' });
			context.overlay = $('<div></div>')
				.css({
					backgroundColor: '#fff',
					position: 'absolute',
					width: context.contentWrapper.width() + 'px',
					height: context.contentWrapper.height() + 'px',
					top: '0px',
					zIndex: 1000,
					opacity: 0,
					cursor: 'default'
				})
				.appendTo(context.contentWrapper);
			if(supportsAdvancedAnimation) {
				context.overlay.animate({ opacity: 0.3 }, 200);
			} else {
				context.overlay.css({ opacity: 0.3 });
			}
		},
		enableForm = function(context) {
			if(!!context.overlay) {
				if(supportsAdvancedAnimation) {
					context.overlay
						.animate({ opacity: 0 }, {
							duration: 200,
							complete: function(){
								context.overlay.hide();
							}
						});
				} else {
					context.overlay.css({ opacity: 0 }).hide();
				}
			}
		},
		// performs a search query
		performQuery = function(context, query, transition) {
			// workaround for IE issue to re-set the sort selection after query
			var sort = query.sort || "";
			var ajaxQuery = {};
			$.each(query, function(key, value){
				if(key.indexOf('pi') === 0) {
					ajaxQuery[key] = value;
				} else {
					ajaxQuery['w_' + key] = value;
				}
			});
			$.telligent.evolution.get({
				url: context.searchUrl,
				data: ajaxQuery,
				success: function(response) {
					enableForm(context);
					revealResults(context, response, transition);
					bindAskFormHandlers(context);
					updateRssLinks(context, query);
					// workaround for IE issue to re-set the sort selection after query
					$(context.searchSortSelect).val(sort);
				}
			});
		},
		blendResultItemToExisting = function(context, selector, transition) {
			var existingItem = context
				.contentWrapper
				.find(selector);
			var newItem = context
				.resultHolder
				.find(selector);
			// only perform transition if the new content is actually different
			if(existingItem.html() !== newItem.html()) {
				newItem
					.hide()
					.insertAfter(existingItem);
				if(!!transition) {
					existingItem
						.glowTransition(newItem, transition);
				} else {
					existingItem.remove();
					newItem.show();
				}
			}
		},
		excludeNonFacetedTagFilters = function(tagFilters, searchResponse) {
			var fullResponse = $(searchResponse);
			$('.breadcrumb-item', tagFilters).each(function(){
				var filter = $(this);
				if($('div.search-filter[data-filtername="'+filter.attr('data-filtername')+'"] li', fullResponse).length === 0) {
					filter.remove();
				}
			});
			return tagFilters;
		},
		containSameTags = function(currentFilters, responseFilters, response) {
			tagFiltersB = excludeNonFacetedTagFilters(responseFilters, response);
			var fullResponse = $(response);
			var itemsA = {}, itemsB = {}, matches = true;
			$('.breadcrumb-item', currentFilters).each(function(){
				var item = $(this);
				itemsA[item.attr('data-filtername')] = item.attr('data-filtervalue');
			});
			$('.breadcrumb-item', tagFiltersB).each(function(){
				var item = $(this);
				itemsB[item.attr('data-filtername')] = item.attr('data-filtervalue');
			});
			$.each(itemsA, function(key, value){
			   if((typeof itemsB[key] === 'undefined') || (itemsA[key] !== itemsB[key])) {
				   matches = false;
				   return false;
			   }
			});
			if(!matches) {
				return matches;
			}
			$.each(itemsB, function(key, value){
			   if((typeof itemsA[key] === 'undefined') || (itemsB[key] !== itemsA[key])) {
				   matches = false;
				   return false;
			   }
			});
			return matches;
		},
		revealResults = function(context, response, transition) {
			// transition.animate = false;
			if(!transition.animate) {
				context.contentWrapper.html(response);
				if (context.contentWrapper.find('div.search-facet-filters .navigation-list').length == 0) {
					context.contentWrapper.find('div.filter-heading-wrapper').slideUp(0);
				}
			} else {
				var currentTagsFilters = $(context.tags);
				var responseTagFilters = $(response).find('.breadcrumb-list-wrapper');
				var identical = containSameTags(currentTagsFilters, responseTagFilters, response);
				if(!identical) {
					currentTagsFilters.replaceWith(responseTagFilters);
				}

				context.resultHolder.html(response);
				blendResultItemToExisting(context, 'div.search-facet-filters');
				blendResultItemToExisting(context, 'div.search-results',
					supportsAdvancedAnimation ? { duration: 300, type: transition.direction, resize: false } : null);
				blendResultItemToExisting(context, 'div.filter-data',
					{ duration: 200, type: 'slideUp', resize: false });

				if (context.contentWrapper.find('div.search-facet-filters .navigation-list').length == 0) {
					context.contentWrapper.find('div.filter-heading-wrapper').slideUp(200);
				} else {
					context.contentWrapper.find('div.filter-heading-wrapper').slideDown(200);
				}
			}
		},
		constructQueryFromFilters = function(context) {
			var query = {
				q: $(context.searchTextInput).val(),
				sort: $(context.searchSortSelect).val(),
				defaultAskForumId: context.defaultAskForumId
			};
			query[context.pageIndexQueryStringKey] = 1;
			$(context.filters).each(function(){
				var filter = $(this).filterSelector('val');
				if(!!filter.value) {
					query[filter.name] = filter.value;
					query[filter.name + 'Visible'] = filter.visible;
				}
			});
			return query;
		},
		constructRssFilterFromQuery = function(context, query) {
			var pattern = "";
			var dateStrings = {
				current: context.currentDate,
				weekAgo: context.weekAgoDate,
				monthAgo: context.monthAgoDate,
				yearAgo: context.yearAgoDate
			};
			var append = function(key, value) {
				if(pattern.length > 0) {
					pattern += "&";
				}
				pattern += (key + "=" + $.telligent.evolution.url.encode(value));
			};
			$.each(query, function(key, value){
				if(key === "q") {
					append('q', value);
				} else if(key === "category") {
					append('category', value);
				} else if(key === "group") {
					append('groups', value);
				} else if(key === "author") {
					append('users', value);
				} else if(key === "tag") {
					append('tags', value);
				} else if(key === "date") {
					if(value === 'past_week') {
						append('sdate', dateStrings.weekAgo);
						append('edate', dateStrings.current);
					} else if(value === 'past_month') {
						append('sdate', dateStrings.monthAgo);
						append('edate', dateStrings.current);
					} else if(value === 'past_year') {
						append('sdate', dateStrings.yearAgo);
						append('edate', dateStrings.current);
					}
				} else if(key === "sort") {
					append('sort', value);
				}
			});
			return pattern;
		},
		updateRssLinks = function(context, searchQuery) {
			var rssQuery = constructRssFilterFromQuery(context, searchQuery);
			var rssUrl = context.searchRssUrl.replace(/QUERY/, rssQuery);
			$(context.rssLink).attr('href', rssUrl);
			$('head')
				.find('link[type="application/rss+xml"]')
				.remove()
				.end()
				.append('<link rel="alternate" type="application/rss+xml" title="' + context.searchRssTitle + '" href="' + rssUrl + '">');
		},
		constructAndExecuteQuery = function(context) {
			// build query
			var query = constructQueryFromFilters(context);
			// push query to hash
			$.telligent.evolution.url.hashData(query, true);
		},
		submitQuestion = function(context) {
			$.telligent.evolution.post({
				url: $.telligent.evolution.site.getBaseUrl() + 'api.ashx/v2/forums/{ForumId}/threads.json',
				data: {
					ForumId: $(context.askForum).val(),
					Subject: $(context.askTitle).evolutionComposer('val'),
					Body: $(context.askBody).evolutionComposer('val'),
					IsQuestion: true
				},
				success: function(response) {
					window.location = response.Thread.Url;
				}
			});
		},
		bindAskFormHandlers = function(context) {
			$(context.askBody).one('focus', function(){
				$(this).evolutionComposer({
					plugins: ['mentions','hashtags']
				});
			});
			$(context.askTitle).one('focus', function(){
				$(this).evolutionComposer({
					plugins: ['hashtags']
				});
			});
			$(context.askSubmit)
				.evolutionValidation({
					onValidated: function(isValid, buttonClicked, c) {
						if (isValid) {
							$(context.askSubmit).removeClass('disabled');
						} else {
							$(context.askSubmit).addClass('disabled');
						}
					},
					onSuccessfulClick: function(e) {
						e.preventDefault();
						$(e.target).addClass('disabled').attr('disabled','disabled');
						submitQuestion(context);
					}
				})
				.evolutionValidation('addField',
					context.askBody, { required: true },
					$(context.askBody).closest('.field-item').find('.field-item-validation'), null)
				.evolutionValidation('addField',
					context.askTitle, { required: true },
					$(context.askTitle).closest('.field-item').find('.field-item-validation'), null);

		},
		setupAskForm = function(context) {
			// handle display of ask form when link clicked
			$(context.askFormLink).live('click', function(e, data) {
				// render an ask form, passing the form data about
				// the current filter results so it build an associated forum list
				var askLink = $(this);
				$.telligent.evolution.get({
					url: context.askFormUrl,
					data: {
						w_query: askLink.attr('data-querytext'),
						w_fieldFilters: askLink.attr('data-fieldfilter'),
						w_dateRangeFilters: askLink.attr('data-datefilter'),
						w_defaultAskForumId: context.defaultAskForumId
					},
					success: function(response) {
						// hide the link.
						$(context.askFormLink).slideUp(200);
						// show the newly returned form
						$(context.askFormWrapper)
							.hide()
							.html(response)
							.css({width:500})
							.slideDown(200);
						// activate the form
						bindAskFormHandlers(context);
					}
				});
				return false;
			});
		},
		// gets the current query, performs necessary legacy querystring support transforms
		getCurrentQuery = function(){
			var query = $.telligent.evolution.url.hashData();
			if(typeof query.ctypes !== 'undefined') {
				if (query.ctypes.indexOf(',') < 0) {
					query.category = query.ctypes;
				}
				delete query.ctypes;
			}
			if (typeof query.users !== 'undefined' && !query.q) {
				query.q = 'user:' + query.users;
				delete query.users;
			}
			return query;
		};
	var api = {
		register: function(context) {
			context.contentWrapper = $(context.contentWrapper);
			context.resultHolder = $(context.resultHolder);
			// perform initial query from current url
			performQuery(context, getCurrentQuery(), { animate: false });
			// set up rich client-side items:
			// tag list
			$(context.tags).tagList();
			// filters
			$(context.filters).filterSelector({ viewMoreText: context.viewMoreText });

			$(window).bind(
				'directedHashChange',
				{
					customComparer: function(prev, curr)
					{
						prev = $.telligent.evolution.url.parseQuery(prev);
						curr = $.telligent.evolution.url.parseQuery(curr);
						if(typeof prev[context.pageIndexQueryStringKey] !== 'undefined' && typeof curr[context.pageIndexQueryStringKey] !== 'undefined') {
							if(Number(prev[context.pageIndexQueryStringKey]) < Number(curr[context.pageIndexQueryStringKey])) {
								return 1;
							} else if(Number(prev[context.pageIndexQueryStringKey]) > Number(curr[context.pageIndexQueryStringKey])) {
								return -1;
							} else {
								return 0;
							}
						}
						return 0;
					}
				},
				function(e, data)
				{
					var data = getCurrentQuery();
					if(typeof data["q"] !== 'undefined') {
						performQuery(context, data, {
							animate: supportsAdvancedAnimation,
							direction: (data.direction === 'backward' ? 'slideRight' : 'slideLeft')
							});
					}
				}
			);
			// handle filter/tag events, and notify related filters/tags as well as modify the current query
			$(document)
				.bind('filterChange', function(e, data){
					constructAndExecuteQuery(context);
				})
				.bind('filterAdd', function(e, data){
					$(context.tags).tagList('addTag', data);
				})
				.bind('filterRemove', function(e, data){
					$(context.tags).tagList('removeTag', data.name);
				})
				.bind('tagRemove', function(e, data){
					$(context.filters+'[data-filtername="'+data+'"]').filterSelector('val','');
				});
			// handle enter presses in search box
			$(context.searchTextInput).live('keypress', function(e, data) {
				if(e.which === 13) {
					constructAndExecuteQuery(context);
					e.preventDefault();
					e.stopPropagation();
					return false;
				}
			});
			// handle search button clicks
			$(context.searchSubmitInput).live('click', function(e, data){
				constructAndExecuteQuery(context);
				return false;
			});
			// handle result sort changes
			$(context.searchSortSelect).live('change', function(e, data){
				constructAndExecuteQuery(context);
			});
			// handle pagination link clicks (inercept and convert them to client-side query-modifying events)
			$('#' + context.wrapperId + ' div.search-results div.content-list-footer a').live('click', function(e, data) {
				var page = Number($(this).attr('href').split('=')[1].split('#')[0]);
				var paginationData = {};
				paginationData[context.pageIndexQueryStringKey] = page;
				$.telligent.evolution.url.hashData(paginationData);
				return false;
			});
			setupAskForm(context);
		}
	};

	if (typeof $.telligent === 'undefined') { $.telligent = {}; }
	if (typeof $.telligent.evolution === 'undefined') { $.telligent.evolution = {}; }
	if (typeof $.telligent.evolution.widgets === 'undefined') { $.telligent.evolution.widgets = {}; }
	$.telligent.evolution.widgets.searchResultsList = api;

}(jQuery));