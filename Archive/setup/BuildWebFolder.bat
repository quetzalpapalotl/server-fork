cd..
DEL Web\*.* /F /S /Q

set build=Builds\TC-7.5.0.32466
xcopy %build%\Web\*.* Web  /S /E /Y /I
xcopy %build%\SQL\*.* sql\CSInstall  /S /E /Y /I
xcopy %build%\Web\bin\*.* lib /S /E /Y /I

xcopy setup\import\filestorage\*.* web\filestorage /S /E /Y /I

C:\WINDOWS\Microsoft.NET\Framework\v4.0.30319\MSBUILD.EXE Omnicell75.sln /t:rebuild  /p:Configuration=DEBUG /verbosity:m /p:Platform="Any CPU" 