@echo off

set site=omnizimbra.dev
set ip=127.0.0.1
set db=.\SQL2014EXPRESS
set search=solr-omni
set sandbox=L:\dev\omni-zimbra\sandbox\web

set curdir=%cd%
set devmode=%1
set hostsfile=%windir%\System32\drivers\etc\hosts
SET appcmd=CALL %WINDIR%\system32\inetsrv\appcmd
SET NEWLINE=^& echo.


echo %devmode% and %curdir% x

If "%devmode%"=="attach" goto :attach
If "%devmode%"=="detach" goto :detach
If "%devmode%"=="" goto :help

goto :enddev

:attach
echo Attaching %site% to development environment...

FIND /c "%site%" %hostsfile%
IF %ERRORLEVEL% NEQ 0 (
	ECHO %NEWLINE%^%ip% %site%>>%hostsfile%
)

%appcmd% list site /name:"%site%"
IF %ERRORLEVEL% NEQ 0 (
	%appcmd% add apppool /name:%site% /managedRuntimeVersion:v4.0
	%appcmd% add site /name:"%site%" /physicalPath:"%sandbox%" /bindings:http/%ip%:80:%site%
	%appcmd% set app "%site%/" /applicationPool:"%site%"
)

sqlcmd -E -S %db% -v devmode="'%devmode%'" -v datapath="'%curdir%\..\data\'" -i %curdir%\..\data\dev.sql

sc stop Tomcat7
timeout /t 5
xcopy %curdir%\..\data\search\%search% c:\solr\%search%\ /e/y/q
xcopy %curdir%\..\data\search\%search%.xml C:\"Program Files"\"Apache Software Foundation"\"Tomcat 7.0"\conf\Catalina\localhost /q
sc start Tomcat7

net start | findstr "%site%"
IF %ERRORLEVEL% NEQ 0 (
	C:\Windows\Microsoft.NET\Framework\v4.0.30319\InstallUtil /name="%site%" "%curdir%\..\sandbox\job\Telligent.Jobs.Server.exe"
	sc start "%site%"
)

goto :enddev

:detach
echo Removing %site from development environment...

FIND /c "%site%" %hostsfile%
IF %ERRORLEVEL% NEQ 1 (
	find /v "%ip% %site%" < %hostsfile% >%hostsfile%tmp 
	copy /a /y %hostsfile%tmp %hostsfile%
)
%appcmd% list site /name:"%site%"
IF %ERRORLEVEL% EQU 0 (
	%appcmd% delete site "%site%"
	%appcmd% delete apppool "%site%"
)

echo "test"
net start | findstr "%site%"
IF %ERRORLEVEL% NEQ 1 (
echo "test1"
	sc stop "%site%"
	C:\Windows\Microsoft.NET\Framework\v4.0.30319\InstallUtil /name="%site%" "%curdir%\..\sandbox\job\Telligent.Jobs.Server.exe" /u
)

sqlcmd -E -S %db% -v devmode="'%devmode%'" -v datapath="'%curdir%\..\data\'" -i %curdir%\..\data\dev.sql

sc stop Tomcat7
timeout /t 5
rmdir c:\solr\%search% /s/q 
del C:\"Program Files"\"Apache Software Foundation"\"Tomcat 7.0"\conf\Catalina\localhost\%search%.xml /q
sc start Tomcat7


goto :enddev

:help
	echo Usage: 
	echo dev.bat attach (configures %site% project.) 
	echo dev.bat detach (removes %site% configuration.) 

goto :enddev


:enddev
