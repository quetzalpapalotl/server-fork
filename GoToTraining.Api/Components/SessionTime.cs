﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoToTraining.Api.Components
{
    public class SessionTime
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}
