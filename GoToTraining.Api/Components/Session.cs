﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoToTraining.Api.Components
{
    public class Session
    {
        public Session()
        {
            Organizers = new List<Organizer>();
        }
        public int Duration { get; set; }
        public string SessionKey { get; set; }
        public int Build { get; set; }
        public DateTime SessionStartTime { get; set; }
        public DateTime SessionEndTime { get; set; }
        public string TrainingKey { get; set; }
        public string TrainingName { get; set; }
        public int AttendanceCount { get; set; }
        public List<Organizer> Organizers { get; set; }
    }
}
