﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoToTraining.Api.Components
{
    public class Organizer
    {
        //public int OrganizerKey { get; set; }
        public string OrganizerKey { get; set; }
        public string Surname { get; set; }
        public string GivenName { get; set; }
        public string Email { get; set; }
    }
}
