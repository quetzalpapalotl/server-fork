﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoToTraining.Api.Components
{
    public class Registrant
    {
        public string Key { get; set; }
        public string Surname { get; set; }
        public string GivenName { get; set; }
        public string Status { get; set; }
        public string Email { get; set; }
        public DateTime RegistrationDate { get; set; }
        public string JoinUrl { get; set; }
        public string ConfirmationUrl { get; set; }
    }
}
