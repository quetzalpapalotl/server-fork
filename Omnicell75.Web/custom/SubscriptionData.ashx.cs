﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections.Specialized;
using System.Text;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Xml.Serialization;
using System.Reflection;

using Omnicell.Custom;
using Telligent.Evolution;
using Telligent.Evolution.Components;
using TEntities = Telligent.Evolution.Extensibility.Api.Entities.Version1;
using TApi = Telligent.Evolution.Extensibility.Api.Version1.PublicApi;
using TExV1 = Telligent.Evolution.Extensibility.Api.Version1;
using Omnicell.Custom.Components;
using Omnicell.Data.Model;
using MoreLinq;
using Omnicell.Custom.Data;


namespace Omnicell75.Web.custom
{
    /// <summary>
    /// Summary description for SubscriptionData
    /// </summary>

    [Serializable]
    [XmlType("User")]
    public class UserSubscription
    {
        public string CSN { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        public string Last_Login { get; set; }
        public string Products { get; set; }
        public string Ticket_Number { get; set; }
        public string Ticket_CSN { get; set; }
        public string Ticket_Count { get; set; }
        public string Email_Count { get; set; }
        
       
        //public string LastName { get; set; }
        //public string FirstName { get; set; }
        //public string Position { get; set; }

    }

    [Serializable]
    public class ProfileInfo
    {
        [XmlAttribute("Key")]
        public string key { get; set; }
        [XmlAttribute]
        public string value { get; set; }
    }

    public class SubscriptionData : IHttpHandler
    {
        public string strIncludedColHeaders = "";
        protected List<TEntities.User> users = new List<TEntities.User>();
        private static object _runAsUserLock = new object();
        int parentGroupId = 5;

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            StringWriter outStream = new StringWriter();
            MemberInfoDataProvider mdp = new MemberInfoDataProvider();

            try
            {
                string apiKey = context.Request.QueryString["apikey"];

                if (apiKey != "gmjak34qvtveqvyg0ua")
                {
                    context.Response.Write("invalid api key");
                }
                else
                {
                    DateTime strStart = new DateTime();
                    if (context.Request.QueryString["startdate"] == null)
                    {

                        context.Response.Write("Please include a properly formatted startdate");
                    }
                    else
                    {
                        strStart = DateTime.Parse(context.Request.QueryString["startdate"]);

                        DateTime endDate = new DateTime();
                        if (context.Request.QueryString["enddate"] == null)
                        {

                            context.Response.Write("Please include a properly formatted enddate");
                        }
                        else
                        {
                            endDate = DateTime.Parse(context.Request.QueryString["enddate"]);

                            User admUser = Telligent.Evolution.Users.GetUser("admin");
                            RunAsUser(() => PopulateUserList(), new ContextService().GetExecutionContext(), admUser);
                            List<UserSubscription> lstSubscriptions = new List<UserSubscription>();

                            foreach (TEntities.User user in users)
                            {
                                UserSubscription sub = new UserSubscription();
                                sub = PopulateUserInfo(user, context, strStart, endDate);
                                //RunAsUser(() => sub.Products = GetUsersProducts(user, context, parentGroupId), new ContextService().GetExecutionContext(), admUser);
                                System.Convert.ToInt32(user.Id);
                                sub.Products = mdp.ListUserProducts(System.Convert.ToInt32(user.Id));
                                lstSubscriptions.Add(sub);
                            }

                            outStream = ConvertObjectToString(lstSubscriptions);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                StringBuilder sberror = new StringBuilder();
                sberror.AppendFormat("Message: {0}\n", ex.Message);
                if (ex.InnerException != null)
                    sberror.AppendFormat("InnerException: {0}\n", ex.InnerException);
                if (ex.StackTrace != null)
                    sberror.AppendFormat("StackTrace: {0}\n", ex.StackTrace);
                if (ex.Source != null)
                    sberror.AppendFormat("Source: {0}\n", ex.Source);

              //  context.Response.Write(sberror.ToString());
            }

            // finally send the item back
            context.Response.Write(outStream.ToString());
        }

        public void RunAsUser(Action a, IExecutionContext context, User u)
        {
            lock (_runAsUserLock)
            {
                var originalUser = context.User;
                try
                {
                    context.User = u;
                    a();
                }
                finally
                {
                    context.User = originalUser;
                }
            }
        }

        public void PopulateUserList()
        {
            var db = new OmnicellEntities();
            List<Omnicell_TicketSubscription> subscriptions = db.Omnicell_TicketSubscription.ToList();
            subscriptions = subscriptions.DistinctBy(x => x.UserId).ToList();


            foreach (var sub in subscriptions)
            {
                int userId = Convert.ToInt32(sub.UserId);
                User nuser = Telligent.Evolution.Users.GetUser(userId);
                if (nuser != null)
                {
                    foreach (TEntities.User user in TApi.Users.List(new TExV1.UsersListOptions { Usernames = nuser.Username }))
                        users.Add(user);
                }
            }
        }

        public UserSubscription PopulateUserInfo(TEntities.User user, HttpContext ctxt, DateTime startdate, DateTime enddate)
        {
            UserSubscription sub = new UserSubscription();
            List<Omnicell_TicketSubscription> sList = new List<Omnicell_TicketSubscription>();
            var db = new OmnicellEntities();
            try
            {
                sub.Email = (user.PrivateEmail != null) ? user.PrivateEmail.Replace(',', ';').ToString() : "";
                //sub.FirstName = user.ProfileFields["FirstName"].Value.Replace(',', ';').ToString();
                //sub.LastName = user.ProfileFields["LastName"].Value.Replace(',', ';').ToString();
                //sub.Position = user.ProfileFields["Title"].Value.Replace(',', ';').ToString();
                sub.Last_Login = user.LastLoginDate.ToString();
                sub.Username = user.Username;
                sub.CSN = user.ProfileFields["CSN"].Value.Replace(',', ';').ToString();
               // sub.Last_Login = user.ProfileFields["Lastlogin"].Value.ToString();
                sList = db.Omnicell_TicketSubscription.Where(x => x.UserId == user.Id).ToList();
                sub.Ticket_Number = GetTicketIds(sList, ctxt);
                sub.Email_Count = db.Omnicell_EmailCounter.Count(x => x.UserId == user.Id && x.Date > startdate && x.Date < enddate).ToString();
                sub.Ticket_Count = sList.Count.ToString();
                sub.Ticket_CSN = GetTicketCSNs(sList, ctxt);
                sub.CSN = sub.CSN.TrimEnd(';');

            }
            catch (Exception ex)
            {
                ctxt.Response.Write("ERROR: Message: " + ex.Message);
            }

            return sub;

        }

        public string GetTicketIds(List<Omnicell_TicketSubscription> subs, HttpContext ctxt)
        {
            string strReturnValue = "";
            var db = new OmnicellEntities();
            Omnicell_Ticket ticket = new Omnicell_Ticket();
            try
            {
                foreach (Omnicell_TicketSubscription sub in subs)
                {
                    ticket = db.Omnicell_Ticket.FirstOrDefault(x => x.Id == sub.TicketId);
                    if(ticket != null) 
                     strReturnValue += ticket.Number.ToString() + ";";
                }
            }
            catch (Exception ex)
            {
                ctxt.Response.Write("Error: Message: " + ex.Message);
            }

            return strReturnValue.TrimEnd(';');
        }

        public string GetTicketCSNs(List<Omnicell_TicketSubscription> subs, HttpContext ctxt)
        {
            string strReturnValue = "";
            var db = new OmnicellEntities();
            Omnicell_Ticket ticket = new Omnicell_Ticket();

            try
            {
                foreach (Omnicell_TicketSubscription sub in subs)
                {
                        strReturnValue += sub.CSN + ";";
                }
            }
            catch (Exception ex)
            {
               // ctxt.Response.Write("Error: Message: " + ex.Message);
            }

            return strReturnValue.TrimEnd(';');
        }

        public string GetUsersProducts(TEntities.User user, HttpContext ctxt, int iParentGroupId)
        {
            String strReturnValue = "";

            try
            {
                TExV1.GroupsListOptions grpListOptions = new TExV1.GroupsListOptions();
                grpListOptions.ParentGroupId = iParentGroupId;
                grpListOptions.IncludeAllSubGroups = true;
                List<TEntities.Group> lstGroups = new List<TEntities.Group>();
                List<int> lstUserGroups = new List<int>();
                List<string> lstProduct = new List<string>();

                foreach (TEntities.Group group in TApi.Groups.List(grpListOptions))
                {
                    lstGroups.Add(group);
                    TExV1.GroupUserMembersGetOptions grpuseroptions = new TExV1.GroupUserMembersGetOptions();
                    grpuseroptions.UserId = user.Id;
                    TEntities.GroupUser groupUser = new TEntities.GroupUser();
                    groupUser = TApi.GroupUserMembers.Get((int)group.Id, grpuseroptions);

                    if (groupUser != null)
                    {
                        bool? issmember = groupUser.IsDirectMember.Value;
                        if (!issmember.Equals(null))
                        {
                            lstUserGroups.Add((int)groupUser.Group.Id);
                            lstProduct.Add(group.Name.ToString());
                            strReturnValue += group.Name.ToString() + ";";
                        }
                    }

                }
            }

            catch (Exception ex)
            {
              //  ctxt.Response.Write("Error: Message: " + ex.Message);
            }

            return strReturnValue.TrimEnd(';');
        }

        public static IEnumerable<string> ToCsv<T>(string separator, IEnumerable<T> objectlist)
        {
            FieldInfo[] fields = typeof(T).GetFields();
            PropertyInfo[] properties = typeof(T).GetProperties();
            yield return String.Join(separator, fields.Select(f => f.Name).Union(properties.Select(p => p.Name)).ToArray());
            foreach (var o in objectlist)
            {
                yield return string.Join(separator, fields.Select(f => (f.GetValue(o) ?? "").ToString())
                    .Union(properties.Select(p => (p.GetValue(o, null) ?? "").ToString())).ToArray());
            }
        }

        public StringWriter ConvertObjectToString(List<UserSubscription> subs)
        {
            StringWriter sw = new StringWriter();

            Type type = typeof(UserSubscription);
            System.Reflection.PropertyInfo[] properties = type.GetProperties();
            var sb = new StringBuilder();
            foreach (System.Reflection.PropertyInfo prp in properties)
            {
                if (prp.CanRead)
                {
                    sb.Append(prp.Name).Append(',');
                }
            }

            sw.WriteLine(sb.ToString().TrimEnd(','));
            sb.Clear();

            foreach (UserSubscription usub in subs)
            {
                foreach (System.Reflection.PropertyInfo prp in properties)
                {
                    if (prp.CanRead)
                    {
                        sb.Append(prp.GetValue(usub, null)).Append(',');
                    }
                }

                sw.WriteLine(sb.ToString().TrimEnd(','));
                sb.Clear();
            }

            return sw;
        }

        private List<int> List<T1>()
        {
            throw new NotImplementedException();
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}