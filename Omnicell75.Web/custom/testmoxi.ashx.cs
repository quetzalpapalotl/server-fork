﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Omnicell.Custom;
using Omnicell.Custom.Components;

namespace Omnicell75.Web.custom
{
    /// <summary>
    /// Summary description for testmoxi
    /// </summary>
    public class testmoxi : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {

            // create a reference to the sharepoint class and call the delete method
            //MoxiService svMoxi = new MoxiService();
            //MoxiWikiService wMoxi = new MoxiWikiService();
            MoxiSyncService wMoxiSync = new MoxiSyncService();

            //wMoxiSync.BatchCreate();
            //wMoxiSync.BatchDelete();

            string metaData = "DxResourceAppName:SW|\r\nDxProductNameTaxHTField0:SW|\r\nTargetGroup:SW|ALERTS BULLETINS\r\nDxStatusTaxHTField0:SW|\r\nDxRegionTaxHTField0:SW|\r\nke94ae374613429c9b5e80b184b0c4f8:SW|\r\nDxKeywords:SW|\r\nDxSource:SW|\r\nDxProductModificationTaxHTField0:SW|\r\nDxMarketTaxHTField0:SW|\r\nvti_author:SR|INFODEVCMS\\\\spintegration\r\nDxAuthor:SW|\r\nDxAudienceJob:SW|\r\nDxId:SW|\r\nffa3be1a342a45aea665498f3654dc5c:SW|\r\nvti_modifiedby:SR|INFODEVCMS\\\\spintegration\r\nab1cd619c9d044e39a19c5a704856d57:SW|\r\nDxCopyRYear:SW|\r\nDxAudience:SW|\r\ne8d232c3374d465889080c8a565004ba:SW|\r\nContentTypeId:SW|0x010100FB57A36042E04DE397602180356DBAB8010300588B78021CEE4B4FAC527BE2E32DF0D5\r\nTaxCatchAll:SW|\r\nDxAudienceName:SW|\r\nDxProductBrandTaxHTField0:SW|\r\nDxProductFeatNumTaxHTField0:SW|\r\nDxProductPlatformTaxHTField0:SW|\r\np3e271209baa4abfa6279af65d28500b:SW|\r\nDxAudienceOtherType:SW|\r\nDxOtherMeta:SW|\r\nDxImportanceTaxHTField0:SW|\r\nvti_folderitemcount:IR|0\r\nDxBusinessProcessTaxHTField0:SW|\r\nDxResourceTaxHTField0:SW|\r\nDxCopyRHolder:SW|\r\nDxAudienceExperienceLevel:SW|\r\nDxProductComponentTaxHTField0:SW|\r\nDxProductProgramNumTaxHTField0:SW|\r\nDxXmlLangTaxHTField0:SW|\r\nDxTitle:SW|\r\nDxCritDatesCreated:SW|\r\nDxPublisherTaxHTField0:SW|\r\nDxBusinessUnitTaxHTField0:SW|\r\nDxPermissionsViewTaxHTField0:SW|\r\nDxAudienceTypeTaxHTField0:SW|\r\nDxProps:SW|\r\nDxRev:SW|\r\nkbb5ec9c61f6475fb5527da06ed29f71:SW|\r\nDxProductName:SW|\r\nvti_foldersubfolderitemcount:IR|0\r\nDxAudienceOtherJob:SW|\r\nvti_title:SW|\r\nDxCategory:SW|\r\nDxBase:SW|\r\nDxObjectType:SW|DxDefault\r\nDxCritDatesRevised:SW|\r\njc00c714c45440ea9184f479978ad9da:SW|\r\nDxProductSeriesTaxHTField0:SW|\r\nvti_parserversion:SR|14.0.0.6029\r\nDxResourceId:SW|\r\nDxProductVersionTaxHTField0:SW|\r\nDxProductReleaseTaxHTField0:SW|\r\n";
            var parsedMetaData = metaData.Split(new string[] { System.Environment.NewLine }, StringSplitOptions.None)
                .Select(x => x.Split('|'))
                .Where(x => x.Length > 1 && !String.IsNullOrEmpty(x.First().Trim()) && !String.IsNullOrEmpty(x.Last().Trim()))
                .ToDictionary(x => x.First().Split(':').First(), x => x.Last().Trim()); //

//            string s = "key1=val1|key2=val2|keyN=valN";

//            var dict = s.Split('|').Select(x => x.Split('='))
//            .Where(x => x.Length > 1 && !String.IsNullOrEmpty(x[0].Trim()) && !String.IsNullOrEmpty(x[1].Trim()))
//            .ToDictionary(x => x[0].Trim(), x => x[1].Trim());



            context.Response.ContentType = "text/plain";
            //context.Response.Write("Raw MetaData String <br />");
            //context.Response.Write(metaData);
            context.Response.Write("<br /><br />Parsed TargetGroup Value: ");
            if(parsedMetaData.ContainsKey("TargetGroup"))
                context.Response.Write(parsedMetaData["TargetGroup"]);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}