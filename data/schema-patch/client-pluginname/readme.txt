
naming convention for schema patch should start off with 
1.0.0 (major,minor, patch)

Copy client_pluginname_0.0.00.sql
Rename copy to client_pluginname_1.0.00.sql

Where client_pluginname is the name of the client and plugin name (ie. defakto_geoip)
Increment the appropriate version number (major.minor.path)

Open the following and configure the following:

Set @SchemaType = 'client_pluginname' -- Set The Name of the Client and Plugin Name (ie. defakto_geoip)
Set @ComponentId = '00000000-0000-0000-0000-000000000000'; -- GUID associated with plugin (optional)

-- Semantic Versioning
Set @Major = 1; -- Major Version Number - The major version is incremented for releases which are not backward-compatible
Set @Minor = 0; -- Minor Version Number - The minor version is incremented for releases which are backward-compatible
Set @Patch = 0; -- Patch Version Number - The patch version is incremented for minor changes and bug fixes which do not change the software's API


ADD YOUR SQL CODE BETWEEN HERE:
--## Schema Patch ##

--## END Schema Patch ##

Be sure to always include conditional logic to check if EXISTS then remove
This allows the ability for an admin to re-run a patch by deleting the version entry from the table and executing the script again.
