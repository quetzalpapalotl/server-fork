SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

	--## Configure Schema Version Schema
	IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='defakto_SchemaVersion')  
	BEGIN 
		CREATE TABLE [dbo].[defakto_SchemaVersion]
		(
		[SchemaType] [varchar](255) NOT NULL, 
		[Major] [int] NOT NULL,
		[Minor] [int] NOT NULL,
		[Patch] [int] NOT NULL,
		[InstallDate] [datetime] NOT NULL,
		[ComponentId] [uniqueidentifier] NOT NULL DEFAULT '00000000-0000-0000-0000-000000000000'
		)
	END
	--## END Init Schme Version Schema

DECLARE @Major int, @Minor int, @Patch int, @Installed DateTime, @Prereqs int, @SchemaType varchar(255), @ComponentId uniqueidentifier

Set @SchemaType = 'client_pluginname' -- Set The Name of the Client and Plugin Name (ie. defakto_geoip)
Set @ComponentId = '00000000-0000-0000-0000-000000000000'; -- GUID associated with plugin (optional)

-- Semantic Versioning
Set @Major = 1; -- Major Version Number - The major version is incremented for releases which are not backward-compatible
Set @Minor = 0; -- Minor Version Number - The minor version is incremented for releases which are backward-compatible
Set @Patch = 0; -- Patch Version Number - The patch version is incremented for minor changes and bug fixes which do not change the software's API

Select @Prereqs = isnull(Count(InstallDate),0)  from [defakto_SchemaVersion] where SchemaType=@SchemaType and Major=@Major and Minor=@Minor and ComponentId =  @ComponentId and Patch<@Patch
Select @Installed = InstallDate  from [defakto_SchemaVersion] where SchemaType = @SchemaType and Major=@Major and Minor=@Minor and ComponentId =  @ComponentId  and Patch=@Patch

If(@Installed is null AND @Prereqs = @Patch)
BEGIN
--## Schema Patch ##
/*
ADD YOUR SCHEMA UPDATES HERE - 
Be sure to always include conditional logic to check if EXISTS then remove
This allows the ability for an admin to re-run a patch by deleting the version entry from the table and executing the script again.

USE sp_ExecuteSQL to wrap all your drop / create object calls.  For Example:

-- DROP OBJECT
exec sp_ExecuteSQL N'
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id (N''[dbo].[fn_dfkt_whatever]'') ) 
	DROP function [dbo].[fn_dfkt_whatever]
'

-- ADD OBJECT
exec sp_ExecuteSQL N'
		Create function [dbo].[fn_dfkt_whatever]
		(
			@MyVar varchar(100)
		)
		RETURNS bigint
		as
		BEGIN
			RETURN (@MyVar + ''?! Whatever, I do what i want!'')
		END
'

*/

--## END Schema Patch ##

	Insert into [defakto_SchemaVersion](SchemaType, Major, Minor, Patch, InstallDate) values (@SchemaType, @Major, @Minor, @Patch, GetDate())
	Print 'Schema Patch v' + Convert(Varchar(2),@Major) + '.' + Convert(Varchar(2),@Minor) + '.' +  Convert(Varchar(3),@Patch) + ' for Component ' + @SchemaType + '(' + COALESCE(Convert(varchar(128),@ComponentId),'BASE') + ') was applied successfully '

END
ELSE IF(@Installed is not null)
BEGIN
	Print 'Schema Patch v' + Convert(Varchar(2),@Major) + '.' + Convert(Varchar(2),@Minor) + '.' +  Convert(Varchar(3),@Patch) + ' for Component ' + @SchemaType + '(' + COALESCE(Convert(varchar(128),@ComponentId),'BASE') + ') was already applied on ' + Convert(varchar(50), @Installed)  
END 
ELSE
BEGIN
	Print 'The patch could not be applied For Component ' + @SchemaType + '(' + COALESCE(Convert(varchar(128),@ComponentId),'BASE') +') because your current schema is missing previous updates (Schema Patch v' + Convert(Varchar(2),@Major) + '.' + Convert(Varchar(2),@Minor) + '.' +  Convert(Varchar(3),@Patch) + ')' 
END 

